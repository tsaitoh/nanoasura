#include	"config.h"
#include	"PlantGravityTree.h"

/* \file PlantGravityTree.c
 * \brief Build a tree structure for gravity calculation.
 */

#define TreeNsub (8)            //!< Number of subnodes.
#define  TreeMaxNodeLevel (20)  //!< Maximum level of the tree structure.
static double WidthFactor[TreeMaxNodeLevel]; //!< Node width factor for each tree level.
static double DiagGravity;                     //!< Distance between a tree node center and its child node center.

/*! @struct StructCachedData
 * Data cache for building the gravity tree structure.
 */
static struct StructCachedData{
    double Pos[3]; //!< Position
    double Mass;   //!< Mass
    double Eps;    //!< Softening length
} *CachedData;


static int *sublist; //<! Temporary array.

static void MakeGravityRoot(void);
static void BuildGravityTree(void);
static int NextGravityNode(const int NodeID);
static void GravityNodeDataImplant(const int CurrentNodeID);

static bool FirstFlag = true; //<! The flag which checks whether InitializeRootForGravity() is called or not.

/*!
  * This function initializes the root node of the tree structure which is used
  * in the gravity calculation.
  */
static void InitializeRootForGravity(void){

    int NloadGravity = ThisRun.NParticles;
    int NumberofAllocatedNodes = ThisRun.NParticles/8;

    /* GravtityRoot */
    GravityRoot.NumberofAllocatedLeaves = NloadGravity;
	GravityRoot.Leaves = malloc(sizeof(int)*NloadGravity+1);
    GravityRoot.NumberofAllocatedNodes = NumberofAllocatedNodes;
	GravityRoot.NumberofNodeCreationLimit = 8;
    GravityRoot.MaxLevel = TreeMaxNodeLevel;

    GravityRoot.NumberofLeavesInGroup = ThisRun.GroupSize;

    double dw = 0.5;
    for(int i=0;i<TreeMaxNodeLevel;i++)
        WidthFactor[i] = pow(dw,(double)i);
    /* GravtityRoot */

    /* GravityNode */
    GravityNode = malloc(sizeof(struct StructGravityNode)*GravityRoot.NumberofAllocatedNodes);

    struct StructGravityNode GravityNodeTemp = {0};
    GravityNodeTemp.Next = GravityNodeTemp.Parent = GravityNodeTemp.Children = 
    GravityNodeTemp.Sister = NONE;

    for(int i=0;i<GravityRoot.NumberofAllocatedNodes;i++)
        GravityNode[i] = GravityNodeTemp;
    /* GravityNode */

    /* GravityCache */
    GravityCache = malloc(sizeof(struct StructGravityCache)*NloadGravity+1);
    GravityAccPot = malloc(sizeof(struct StructGravityAccPot)*NloadGravity+1);
    /* GravityCache */
    
    sublist = malloc(sizeof(int)*NloadGravity);
    CachedData = malloc(sizeof(struct StructCachedData)*NloadGravity);

	return;
}

/*!
 * Copy particle positions, mass and softening length into the cache array.
 */
static void GravityTreePreprocessing(void){

    for(int i=0;i<ThisRun.NParticles;i++){
        CachedData[i].Pos[0] = SPH[i].Pos[0];
        CachedData[i].Pos[1] = SPH[i].Pos[1];
        CachedData[i].Pos[2] = SPH[i].Pos[2];
        CachedData[i].Mass   = SPH[i].Mass;
        CachedData[i].Eps    = SPH[i].Eps;
    }

    return ;
}

void PlantGravityTree(void){

    if(ThisRun.ShowLog){
        fprintf(stderr,"Plant gravity tree start... ");
        fflush(stderr);
    }

    if(FirstFlag){
        InitializeRootForGravity();
        FirstFlag = false;
    }

    GravityTreePreprocessing();

	MakeGravityRoot();
    BuildGravityTree();

    for(int i=1;i<GravityRoot.NumberofNodes;i++)
        GravityNode[i].Next = NextGravityNode(i);

    DiagGravity = DISTANCE(GravityNode[0].Pos,GravityNode[GravityNode[0].Children].Pos);

    GravityNodeDataImplant(0);

    if(ThisRun.ShowLog){
        fprintf(stderr,"finish.\n");
        fflush(stderr);
    }

	return;
}

/*!
 * Make the gravity root node.
 */
static void MakeGravityRoot(void){

	double min[3],max[3];
    int RootNodeID = 0;

    for(int k=0;k<3;k++){
        max[k] = CachedData[0].Pos[k];
        min[k] = CachedData[0].Pos[k];
    }
    for(int i=1;i<ThisRun.NParticles;i++){
        for(int k=0;k<3;k++){
            max[k] = fmax(CachedData[i].Pos[k],max[k]);
            min[k] = fmin(CachedData[i].Pos[k],min[k]);
        }
    }

    for(int k=0;k<3;k++){
        GravityRoot.PosMax[k] = max[k];
        GravityRoot.PosMin[k] = min[k];
    }

    double WidthMax = 0.e0;
    for(int k=0;k<3;k++){
        GravityNode[RootNodeID].Pos[k] = 0.5*(max[k] + min[k]);
        WidthMax = fmax(WidthMax,max[k]-min[k]);
    }
    GravityRoot.Width = WidthMax;

    GravityNode[RootNodeID].Next = NONE;
    GravityNode[RootNodeID].Parent = NONE;
    GravityNode[RootNodeID].Sister = NONE;
    GravityNode[RootNodeID].Children = NONE;

    GravityNode[RootNodeID].NumberofLeaves = ThisRun.NParticles;

    GravityNode[RootNodeID].Level = 0;
    GravityNode[RootNodeID].Leaves = 0;

    for(int i=0;i<ThisRun.NParticles;i++)
        GravityRoot.Leaves[i] = i;
    GravityRoot.NumberofLeaves = ThisRun.NParticles;

	return ;
}

/*!
 * This function finds the "next" tree node of the current tree node.  The
 * "next" node is the sister node if the tree node has the sister node.  If the
 * tree node do not have a sister node the sister node of the parent node. In
 * this case, if the sister node of the parent node does not exist, this process
 * is applied recursively to the upper level. 
 */
static int NextGravityNode(const int NodeID){

    int CurrentNodeID = NodeID;

    if(GravityNode[CurrentNodeID].Sister != NONE){
        CurrentNodeID = GravityNode[CurrentNodeID].Sister;
    } else {
        int NextNodeID = CurrentNodeID;
        while(1){
            if(GravityNode[GravityNode[NextNodeID].Parent].Sister != NONE){
                CurrentNodeID = GravityNode[GravityNode[NextNodeID].Parent].Sister;
                break;
            } else if(GravityNode[NextNodeID].Parent == 0){
                CurrentNodeID = 0;
                break;
            }
            NextNodeID = GravityNode[NextNodeID].Parent;
        }
    }
    return CurrentNodeID;
}

/*!
 * This function returns "true" if the number of particles contained in the node
 * is small enough, otherwise false.
 */
static inline bool __attribute__((always_inline)) GravityNodeSeparationCriterion(const int CurrentNodeID, const int CriticalNumber){

	if( (GravityNode[CurrentNodeID].NumberofLeaves <= CriticalNumber) || GravityNode[CurrentNodeID].Level+1>=TreeMaxNodeLevel){
        return true;
    } else {
        return false;
    }
}

/*!
 * Build the tree structure.
 */
static void BuildGravityTree(void){

    int NumberofNodes = 0; 
    int subhead[TreeNsub],subcurrent[TreeNsub],subnumber[TreeNsub]; 

    int NumberofNodeCreationLimit = GravityRoot.NumberofNodeCreationLimit;

    int CurrentMaxLevel = 0;
    int RootNodeID = 0; 
    int CurrentNodeID = RootNodeID;
    int ChildNodeID,BackwardNodeID,NextNodeID;

	while(1){

		if(GravityNodeSeparationCriterion(CurrentNodeID,NumberofNodeCreationLimit) && (CurrentNodeID != RootNodeID)){
			if(GravityNode[CurrentNodeID].Sister != NONE){
				CurrentNodeID = GravityNode[CurrentNodeID].Sister;
			}else{
				NextNodeID = CurrentNodeID;
				while(1){
                    if(GravityNode[GravityNode[NextNodeID].Parent].Sister != NONE){
                        CurrentNodeID = GravityNode[GravityNode[NextNodeID].Parent].Sister;
						break;
					} else if(GravityNode[NextNodeID].Parent == RootNodeID){
                        GravityRoot.CurrentMaxLevel = CurrentMaxLevel;

                        int NumberofLeaves = GravityNode[RootNodeID].NumberofLeaves;
                        for(int k=0;k<NumberofLeaves;k++){
                            int leaf =  GravityRoot.Leaves[k];
                            GravityCache[k].Pos[0] = CachedData[leaf].Pos[0];
                            GravityCache[k].Pos[1] = CachedData[leaf].Pos[1];
                            GravityCache[k].Pos[2] = CachedData[leaf].Pos[2];
                            GravityCache[k].Mass   = CachedData[leaf].Mass;
                            GravityCache[k].Eps    = CachedData[leaf].Eps;
                            GravityCache[k].Leaf = leaf;
                        }
                        GravityRoot.NumberofNodes = NumberofNodes + 1;

						return;
					}
                    NextNodeID = GravityNode[NextNodeID].Parent;
				}
			}
			continue;
		}

		for(int k=0;k<TreeNsub;k++){
			subnumber[k] = 0;
			subhead[k] = subcurrent[k] = NONE;
		}

        int NumberofLeaves = GravityNode[CurrentNodeID].NumberofLeaves;
		for(int i=0;i<NumberofLeaves;i++){
            int leaf = GravityRoot.Leaves[GravityNode[CurrentNodeID].Leaves+i];
			int subindex = 0;

            for(int k=0;k<3;k++)
                if(GravityNode[CurrentNodeID].Pos[k] <= CachedData[leaf].Pos[k])
                    subindex += 1 << k;

			if(subnumber[subindex] == 0){
				subhead[subindex] = subcurrent[subindex] = leaf;
			} else {
				sublist[subcurrent[subindex]] = leaf;
				subcurrent[subindex] = leaf;
			}
			subnumber[subindex] ++;
        }


        ChildNodeID = CurrentNodeID;
		for(int i=0;i<TreeNsub;i++){
			if(subnumber[i] != 0){
				BackwardNodeID = ChildNodeID; 
                NumberofNodes ++;
                ChildNodeID = NumberofNodes;
                if(NumberofNodes >= GravityRoot.NumberofAllocatedNodes){
                    int NumberofAllocatedNodes = (int)(1.2*NumberofNodes);
                    GravityNode = realloc(GravityNode,sizeof(struct StructGravityNode)*NumberofAllocatedNodes);
                    GravityRoot.NumberofAllocatedNodes = NumberofAllocatedNodes;
                }

                GravityNode[ChildNodeID].Next = NONE;
                GravityNode[ChildNodeID].Parent = NONE;
                GravityNode[ChildNodeID].Sister = NONE;
                GravityNode[ChildNodeID].Children = NONE;
                GravityNode[ChildNodeID].Parent = CurrentNodeID;

                if(BackwardNodeID == CurrentNodeID){
                    GravityNode[CurrentNodeID].Children = ChildNodeID;
					NextNodeID = ChildNodeID;
                    GravityNode[ChildNodeID].Leaves = GravityNode[CurrentNodeID].Leaves;
                    CurrentMaxLevel = MAX(CurrentMaxLevel,GravityNode[CurrentNodeID].Level+1);
                } else {
                    GravityNode[BackwardNodeID].Sister = ChildNodeID;
                    GravityNode[ChildNodeID].Leaves = 
                        GravityNode[BackwardNodeID].Leaves + GravityNode[BackwardNodeID].NumberofLeaves;
                }

                GravityRoot.Leaves[GravityNode[ChildNodeID].Leaves] = subhead[i];
                for(int k=1;k<subnumber[i];k++){
                    GravityRoot.Leaves[GravityNode[ChildNodeID].Leaves+k] = 
                        sublist[GravityRoot.Leaves[GravityNode[ChildNodeID].Leaves+k-1]];
                }
                GravityNode[ChildNodeID].NumberofLeaves = subnumber[i];
                GravityNode[ChildNodeID].Level = GravityNode[CurrentNodeID].Level+1;

				for(int k=0;k<3;k++)
                    GravityNode[ChildNodeID].Pos[k] = GravityNode[CurrentNodeID].Pos[k] +
						+ BitSign((i>>k)&1)*0.25*GravityRoot.Width*WidthFactor[GravityNode[CurrentNodeID].Level];
            }
		}
        CurrentNodeID = NextNodeID;

	}
}

/* !
 * This function implants data into tree nodes.
 */
static void GravityNodeDataImplant(const int CurrentNodeID){

    double DistanceMax = 0.e0;
    double Mass = 0.e0;
    double COM[3] = {0.e0,0.e0,0.e0};
    double Eps2 = 0.e0;
    double EpsMax = 0.e0,
           EpsMin = 0.e0;

    if(GravityNode[CurrentNodeID].Children == NONE){
        int Number_of_leaf = GravityNode[CurrentNodeID].NumberofLeaves;
        int header = GravityNode[CurrentNodeID].Leaves;
        EpsMax = EpsMin = GravityCache[header].Eps;
        for(int k=0;k<Number_of_leaf;k++){
            int leaf = header+k;
            double Distance = DISTANCE(GravityNode[CurrentNodeID].Pos,GravityCache[leaf].Pos);
            DistanceMax = fmax(Distance,DistanceMax);
            Mass += GravityCache[leaf].Mass;
            COM[0] += GravityCache[leaf].Mass*GravityCache[leaf].Pos[0];
            COM[1] += GravityCache[leaf].Mass*GravityCache[leaf].Pos[1];
            COM[2] += GravityCache[leaf].Mass*GravityCache[leaf].Pos[2];
            Eps2 += GravityCache[leaf].Mass*SQ(GravityCache[leaf].Eps);
            EpsMin = fmin(EpsMin,GravityCache[leaf].Eps);
            EpsMax = fmax(EpsMax,GravityCache[leaf].Eps);
        }
    } else {
        bool first = true;
        int ChildNodeID = GravityNode[CurrentNodeID].Children;
        do{
            GravityNodeDataImplant(ChildNodeID);

            DistanceMax = fmax(DistanceMax,GravityNode[ChildNodeID].DistanceMax);
            Mass += GravityNode[ChildNodeID].Mass;
            COM[0] += GravityNode[ChildNodeID].Mass*GravityNode[ChildNodeID].COM[0];
            COM[1] += GravityNode[ChildNodeID].Mass*GravityNode[ChildNodeID].COM[1];
            COM[2] += GravityNode[ChildNodeID].Mass*GravityNode[ChildNodeID].COM[2];
            Eps2 += GravityNode[ChildNodeID].Mass*GravityNode[ChildNodeID].Eps2;
            if(first == true){
                EpsMin = GravityNode[ChildNodeID].EpsMin;
                EpsMax = GravityNode[ChildNodeID].EpsMax;
                first = false;
            } else {
                EpsMin = fmin(EpsMin,GravityNode[ChildNodeID].EpsMin);
                EpsMax = fmax(EpsMax,GravityNode[ChildNodeID].EpsMax);
            }
            ChildNodeID = GravityNode[ChildNodeID].Sister;
        } while(ChildNodeID != NONE);
    }

    double InvMass = 1.e0/Mass;
    GravityNode[CurrentNodeID].COM[0] = COM[0]*InvMass;
    GravityNode[CurrentNodeID].COM[1] = COM[1]*InvMass;
    GravityNode[CurrentNodeID].COM[2] = COM[2]*InvMass;

    double Width = DiagGravity*WidthFactor[HydroNode[CurrentNodeID].Level];
    GravityNode[CurrentNodeID].DistanceMax = DistanceMax + Width;
    GravityNode[CurrentNodeID].Mass = Mass;
    GravityNode[CurrentNodeID].Eps2 = Eps2*InvMass;
    GravityNode[CurrentNodeID].EpsMax = EpsMax;
    GravityNode[CurrentNodeID].EpsMin = EpsMin;

    return ;
}



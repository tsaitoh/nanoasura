#include "config.h"
#include "ReadParameters.h"
#include "InitOpenMP.h"
#include "./setup/InitRun.h"
#include "Run.h"
#include "Exit.h"

/*! \file main.c
 * \brief The main routine of this simulation code.
 */

/*!
 * This is the main function of this program.
 */
int main(int argc, char **argv){

    // Read and set run parameters.
    if(argc > 1){
        ReadParameters(argv[1]);
    } else {
        ReadParameters("./param.toml");
    }

    // Show log on the screen at the end of the run.
    atexit(ASURA_Exit);

    // Inititalize the environment for OpenMP operations.
    CheckAndSetOpenMPEnv();

    // Set initial condition.
    InitRun();

    // Run simulation.
    Run();

    return EXIT_SUCCESS;
}

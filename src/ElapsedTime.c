#include "config.h"
#include <time.h>
#include <sys/time.h>

/*! \file ElapsedTime.c
 * \brief Get the current time.
 */

/*!
 * This function returns the current time in second.
 */
double GetElapsedTime(void){
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return (tp.tv_sec + tp.tv_usec*1e-6);
}

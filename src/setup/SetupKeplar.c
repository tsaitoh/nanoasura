#include "../config.h"

/*! \file SetupKeplar.c
 * \brief Initial condition the Keplar test.
 */

/*!
 * This function generates the initial condition of the Keplar disk used in
 * Hosono, Saitoh \& Makino (2016).
 * See https://ui.adsabs.harvard.edu/#abs/arXiv:1601.05903
 */
#define GM (1.0)
#define InitKeplarBase (6)
void InitKeplarTest(void){

    int p = InitKeplarBase;
    int Nring = ThisRun.NParticles;
    double r_in = 0.5;
    double r_out = 2;
    double dr = r_out/Nring;
    int i_min = (r_in/dr)+1;

    int count = 0;
    for(int i=i_min;i<Nring;i++){
        for(int k=0;k<i*p;k++){
            count ++;
        }
    }

    SPH = malloc(sizeof(struct StructSPH)*count);

    double Surface = M_PI*(2.0*2.0-0.5*0.5);
    double Rho = 1.0;
    double TotalMass = Surface*Rho;
    double Mass = TotalMass/((double)count);
    double Kernel = 10.0*sqrt(Surface)/count;

    double P = 1.e-6;
    double u = P/(ThisRun.Gm1*Rho);

    count = 0;
    for(int i=i_min;i<Nring;i++){
        double r = i*dr;
        double phi = 2*M_PI/(i*p);
        for(int k=0;k<i*p;k++){

            double x = r*cos(phi*k);
            double y = r*sin(phi*k);

            double r = sqrt(x*x+y*y);

            double Vc = sqrt(GM/r);

            SPH[count].Pos[0] = x;
            SPH[count].Pos[1] = y;
            SPH[count].Vel[0] = -Vc*(y/r);
            SPH[count].Vel[1] = +Vc*(x/r);

            SPH[count].Rho = Rho;
            SPH[count].u = u;
            SPH[count].Kernel = Kernel;
            SPH[count].Mass = Mass;
            if(ThisRun.UseDISPH == 1){
                SPH[count].q = SPH[count].Rho*SPH[count].u;
            } 

            SPH[count].du = 0.e0;
            SPH[count].Pos[2] = SPH[count].Vel[2] = 
            SPH[count].Acc[0] = SPH[count].Acc[1] = SPH[count].Acc[2] = 0.e0;
            SPH[count].Alpha = 0.1;
            count ++;
        }
    }

    ThisRun.NParticles = count;

    // SetInitialViscosityParameter(NONE,InitAlpha[0]);
    fprintf(stderr,"Finish initial setup.\n");
    return ;
}

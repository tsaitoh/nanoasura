#include "../config.h"

/*! \file SetupKH.c
 * \brief Initial condition for KH.
 */


/*!
 * This function generates the initial condition of Kelvin-Helmholtz instability
 * test.
 * @param mode If this parameter is 0, the initial condition used in Price
 * (2008) is used. If it is 1, that used in Read et al. (2010) is used.
 */
void InitKHTest(const int mode){ // mode 0 = Price, mode = 1 Read

    const double rho_h = 2.0;
    const double rho_l = 1.0;
    const double p_h = 2.5;
    const double p_l = 2.5;
    const double v_h = 0.5;
    const double v_l = -0.5;
    const double u_h = p_h/(ThisRun.Gm1*rho_h);
    const double u_l = p_l/(ThisRun.Gm1*rho_l);

    double cs_h = sqrt(ThisRun.GGm1*u_h);
    double cs_l = sqrt(ThisRun.GGm1*u_l);
    fprintf(stderr,"cs_h = %g, cs_l = %g\n",cs_h,cs_l);

    const int ngrid_h = ThisRun.NParticles;
    const int ngrid_l = sqrt(SQ(ngrid_h) *rho_l/rho_h);

    const int n_h = 0.5 * SQ(ngrid_h);
    const int n_l = 0.5 * SQ(ngrid_l);

    const double mgas = 0.5 * (rho_h + rho_l) * (ThisRun.LBox[0]) / ((double)(n_h + n_l));
    fprintf(stderr,"ngrid_h = %d, ngrid_l = %d\n",ngrid_h,ngrid_l);
    fprintf(stderr,"npart_h = %d, npart_l = %d\n",n_h,n_l);

    ThisRun.NParticles = 0;
    int half_grid = 0.5 * ngrid_h;
    for (int i = 0; i != ngrid_h; ++i) {
    for (int j = 0; j != half_grid; ++j) {
        ThisRun.NParticles ++;
    }
    }

    half_grid = 0.5 * ngrid_l;
    for (int i = 0; i != ngrid_l; ++i) {
    for (int j = 0; j != half_grid; ++j){
        ThisRun.NParticles ++;
    }
    }

    SPH = malloc(sizeof(struct StructSPH)*ThisRun.NParticles);

    double KH_Width,KH_Lambda,KH_Amp;
    if(mode == 0){
        KH_Width  = 0.025;
        KH_Lambda = 1.0/6.0;
        KH_Amp    = 0.025;
    } else if(mode == 1){
        KH_Width  =  0.025;
        KH_Lambda =  1.0/2.0;
        KH_Amp    =  1.0/8.0;
    }

    int counter = 0;
    half_grid = 0.5 * ngrid_h;
    for (int i = 0; i != ngrid_h; ++i) {
        for (int j = 0; j != half_grid; ++j) {
            double dx = sqrt(0.5 * SQ(ThisRun.LBox[0])/((double)n_h));
            double x = (0.5 + i) * dx;
            double y = 0.25 + (0.5 + j) * dx;
            SPH[counter].Kernel = 2*dx;
            SPH[counter].Pos[0] = x;
            SPH[counter].Pos[1] = y;
            SPH[counter].Pos[2] = 0.5;

            if(mode == 0){
                SPH[counter].Vel[0] = v_h;
                double vy = 0;
                if (fabs(y - 0.5 - 0.25) < KH_Width)
                    vy = KH_Amp * sin(-2 * M_PI * (x+0.5)/KH_Lambda);
                else if (fabs(y - 0.5 + 0.25) < KH_Width)
                    vy = KH_Amp * sin(2 * M_PI * (x+0.5)/KH_Lambda);
                SPH[counter].Vel[1] = vy;
                SPH[counter].Vel[2] = 0.e0;
            } else if(mode == 1){
                SPH[counter].Vel[0] = v_h;
                SPH[counter].Vel[1] = KH_Amp*
                    (sin(2.0*M_PI*(x+KH_Lambda/2.0)/KH_Lambda)*exp(-SQ(10*(y-0.75)))
                    -sin(2.0*M_PI*x/KH_Lambda)*exp(-SQ(10*(y-0.25))));
            }
            SPH[counter].Vel[2] = 0.e0;

            SPH[counter].Mass = mgas;
            SPH[counter].u = u_h;
            SPH[counter].Rho = rho_h;
            SPH[counter].q = rho_h*u_h;
            SPH[counter].Tag = 0;

            SPH[counter].du =
            SPH[counter].Acc[0] =
            SPH[counter].Acc[1] =
            SPH[counter].Acc[2] = 0.e0;
            SPH[counter].Alpha = 1.0;

            counter ++;
        }
    }

    half_grid = 0.5 * ngrid_l;
    for (int i = 0; i != ngrid_l; ++i) {
        for (int j = 0; j != half_grid; ++j){
            double dx = sqrt(0.5 * SQ(ThisRun.LBox[0])/((double)n_l));
            double x = (0.5 + i) * dx;
            double y = (1.0 + j) * dx;
            if (y > 0.25)
                y += 0.5;
            SPH[counter].Kernel = 2*dx;
            SPH[counter].Pos[0] = x;
            SPH[counter].Pos[1] = y;
            SPH[counter].Pos[2] = 0.5;

            if(mode == 0){
                SPH[counter].Vel[0] = v_l;
                double vy = 0;
                if (fabs(y - 0.5 - 0.25) < KH_Width)
                    vy = KH_Amp * sin(-2 * M_PI * (x+0.5)/KH_Lambda);
                else if (fabs(y - 0.5 + 0.25) < KH_Width)
                    vy = KH_Amp * sin(2 * M_PI * (x+0.5)/KH_Lambda);
                SPH[counter].Vel[1] = vy;
                SPH[counter].Vel[2] = 0.e0;
            } else if(mode ==1){
                SPH[counter].Vel[0] = v_l;
                SPH[counter].Vel[1] = KH_Amp*
                    (sin(2.0*M_PI*(x+KH_Lambda/2.0)/KH_Lambda)*exp(-SQ(10*(y-0.75)))
                    -sin(2.0*M_PI*x/KH_Lambda)*exp(-SQ(10*(y-0.25))));
            }
            SPH[counter].Vel[2] = 0.e0;

            SPH[counter].Mass = mgas;
            SPH[counter].u = u_l;
            SPH[counter].Rho = rho_l;
            SPH[counter].q = rho_l*u_l;
            SPH[counter].Tag = 1;

            SPH[counter].du =
            SPH[counter].Acc[0] =
            SPH[counter].Acc[1] =
            SPH[counter].Acc[2] = 0.e0;
            SPH[counter].Alpha = 1.0;

            counter ++;
        }
    }

    ThisRun.TNorm = ((rho_l+rho_h)*(KH_Lambda))/(sqrt(rho_l*rho_h)*(v_h-v_l));
    ThisRun.TEnd *= ThisRun.TNorm;
    fprintf(stderr,"KH time scale is %g, Tend = %g\n",ThisRun.TNorm,ThisRun.TEnd);
    ThisRun.dt_output = ThisRun.TEnd/(double)ThisRun.OutputFileNumber;

    fprintf(stderr,"Finish initial setup.\n");

    return ;
}


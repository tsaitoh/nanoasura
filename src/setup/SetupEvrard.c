#include "../config.h"
#include "../NeighborSearch.h"
#include "../KernelFunctions.h"
#include "../Run.h"

#define TINY                (1.e-30)

/*! \file SetupEvrard.c
 * \brief Initial condition for the Evrard test.
 */

void InitEvrardTest(void){

    int NGrid = ThisRun.NParticles;
    ThisRun.NParticles = CUBE(NGrid);

    // Count particles
	double dx = 2.e0/(double)(NGrid-1);
    int counter = 0;
	for(int i=0;i<NGrid;i++){
		for(int j=0;j<NGrid;j++){
			for(int k=0;k<NGrid;k++){
                double Pos[3];
				Pos[0] = -1.e0+i*dx;
				Pos[1] = -1.e0+j*dx;
				Pos[2] = -1.e0+k*dx;

				if(1.e0>NORM(Pos)){
					counter ++;
				}
			}
		}
	}
    ThisRun.NParticles = counter;

    SPH = malloc(sizeof(struct StructSPH)*ThisRun.NParticles);

    double mgas = 1.0/counter;
    double Eps = 0.1*(cbrt((double)1736/(double)counter));
    double Uinit = 0.05;
    counter = 0;
	for(int i=0;i<NGrid;i++){
		for(int j=0;j<NGrid;j++){
			for(int k=0;k<NGrid;k++){
                double Pos[3];
				Pos[0] = -1.e0+i*dx;
				Pos[1] = -1.e0+j*dx;
				Pos[2] = -1.e0+k*dx;

				double r = NORM(Pos);
				/*** make unit vector ***/
                if(r>TINY){
                    Pos[0] /= r;
                    Pos[1] /= r;
                    Pos[2] /= r;
				
                    /*** strech grid(r_new = r_old**3) ***/
                    Pos[0] *= (r)*sqrt(r);
                    Pos[1] *= (r)*sqrt(r);
                    Pos[2] *= (r)*sqrt(r);
                } else {
                    Pos[0] = 0.e0;
                    Pos[1] = 0.e0;
                    Pos[2] = 0.e0;
                }

				if(1.e0 < NORM(Pos)) continue;
				
                SPH[counter].Pos[0] = Pos[0];
                SPH[counter].Pos[1] = Pos[1];
                SPH[counter].Pos[2] = Pos[2];

                SPH[counter].Vel[0] = 
                SPH[counter].Vel[1] = 
                SPH[counter].Vel[2] = 0.e0;

                SPH[counter].Mass = mgas;
                SPH[counter].Rho = 1.0; // Dummy 
                SPH[counter].Kernel = dx;
                SPH[counter].u = Uinit;
                SPH[counter].q = SPH[counter].Rho*SPH[counter].u;

                SPH[counter].du =
                SPH[counter].Acc[0] =
                SPH[counter].Acc[1] =
                SPH[counter].Acc[2] = 0.e0;

                SPH[counter].Alpha = 1.0;
                SPH[counter].Eps = Eps;

                SPH[counter].Tag = 0;


                counter ++;
			}
		}
	}

    return ;
}

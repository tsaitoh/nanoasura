#include "../config.h"
#include "../NeighborSearch.h"
#include "../KernelFunctions.h"
#include "../Run.h"

#define MaxIteration        (200)

/*! \file SetupGalaxyModel.c
 * \brief Initial condition for the galaxy model.
 */

static double ReturnDistanceForExponent(const double F, const double Rd){

#define Rstart (0)
#define Rend   (5*Rd)
    double Diff;

    double rstart = Rstart;
    double rend = Rend;
    double fstart = 1-exp(-rstart/Rd)*(1+rstart/Rd) - F;
    //double fend = 1-exp(-rend/Rd)*(1+rend/Rd) - F;

    double r = 0.5*(rend+rstart);
    double f = 1-exp(-r/Rd)*(1+r/Rd) - F;

    int Iteration = 0;
    do{
        f = 1-exp(-r/Rd)*(1+r/Rd) - F;

        if(f*fstart < 0.0){
            // fend = f;
            rend = r;
            r = 0.5*(rstart+rend);
            Diff = fabs(rend-rstart);
        } else {
            // fstart = f;
            rstart = r;
            r = 0.5*(rstart+rend);
            Diff = fabs(rend-rstart);
        }
        if(Iteration > MaxIteration)
            exit(1);

        Iteration ++;
    } while ( Diff > 1.e-6*Rd );

#undef Rstart 
#undef Rend  
    return r;
}


static inline double __attribute__((always_inline)) fx(const double x){
        return (log(1.0+x) - x/(1.e0+x));
}

/*
 * This function returns acceleration from the static potentials of halo and
 * stellar disk/bulge/bar.  The model is based on that used in Saitoh et al.
 * (2008) and Pettitt et al. (2014).
 */
static double Omega_bar_log; // This value is used in WriteRC();
void AccelerationByMilkyWayPotentialHaloDisk(void){

    /* Parameters for NFW halo */
    // Parameters are obtained from Saitoh et al. 2018
    const double Cnfw = 12.e0;
    const double OmegaM = 0.3;
    const double delta_th = 340.e0;
    const double hubble = 0.7;
    const double Hubble = hubble*100*(0.001/*UnitLength/Mpc*/)*(1.e+5/3.086e+21)/(1.0/3.15576e+15);
    const double GravitationalConstant = (6.6725985e-8*(1.989e33*1.e10*SQ(3.15576e+15))/CUBE(3.086e+21));

    const double Mvir = 1.e+2;  // times 10^10 Msun
    const double RhoCrit = (3.e0*SQ(Hubble)/(8.e0*M_PI*GravitationalConstant));
    const double Rvir = cbrt(Mvir*3.e0/(4.e0*M_PI*RhoCrit*OmegaM*delta_th));
    const double rs = Rvir/Cnfw;
    /* Parameters for NFW halo */

    /* Parameters for Miyamoto-Nagai disk */
    // Parameters are obtained from Pettitt et al. 2014
    const double Mdisk = 8.56; // times 10^10 Msun
    const double R0 = 5.3; // kpc
    const double z0 = 0.25; // kpc
    /* Parameters for Miyamoto-Nagai disk */

    /* Parameters for bulge */
    // Parameters are obtained from Pettitt et al. 2014
    const double Mbulge = 1.4;  // times 10^10 Msun
    const double rbulge = 0.39; // kpc
    const double rbulge2 = SQ(rbulge); // kpc
    /* Parameters for bulge */

    /* Parameters for bar */
    // Parameters are obtained from Pettitt et al. 2014, which is based on Wada 1994.
    const double r_bar = 2.0; // kpc
    const double r_bar2 = SQ(r_bar); 
    const double r_bar4 = SQ(r_bar2); 
    const double V0_bar = 220.0*(1.e+5/3.086e+21)/(1.0/3.15576e+15); // km/s
    const double Phi_bar = SQ(V0_bar)*sqrt(27.0/4.0); 
    const double Omega_bar_km_per_s_per_kpc = 50; // km/s/kpc
    const double Omega_bar = Omega_bar_km_per_s_per_kpc*(1.e+5/3.086e+21)/(1.0/3.15576e+15); // km/s/kpc in simulation units.
    Omega_bar_log = Omega_bar_km_per_s_per_kpc;

    double epsilon_bar = 0.05*fmin(1,ThisRun.TCurrent/0.1); // amplitude control function
    /* Parameters for bar potential used in Wada (1994) */


    for(int i=0;i<ThisRun.NParticles;i++){
#if 1
        // NFW halo 
        double r = sqrt(NORM2(SPH[i].Pos)+SQ(0.001*rs));
        double x = r/rs;
        double Mass = Mvir*fx(x)/fx(Cnfw);
        double gravfact = GravitationalConstant*Mass/CUBE(r);

        SPH[i].Acc[0] -= gravfact*SPH[i].Pos[0];
        SPH[i].Acc[1] -= gravfact*SPH[i].Pos[1];
        SPH[i].Acc[2] -= gravfact*SPH[i].Pos[2];

        // Stellar disk potential (Miyamoto & Nagai)
        double Rdisk = sqrt(SQ(SPH[i].Pos[0])+SQ(SPH[i].Pos[1]));
        double Zdisk = SPH[i].Pos[2];
        double Adisk = R0+sqrt(SQ(Zdisk)+SQ(z0));
        double gravfact_disk = GravitationalConstant*Mdisk/sqrt(CUBE(SQ(Rdisk)+SQ(Adisk)));

        SPH[i].Acc[0] -= gravfact_disk*SPH[i].Pos[0];
        SPH[i].Acc[1] -= gravfact_disk*SPH[i].Pos[1];
        SPH[i].Acc[2] -= gravfact_disk*(R0/sqrt(SQ(Zdisk)+SQ(z0))+1.0)*SPH[i].Pos[2];

        // Bulge potential
        double Rbulge = sqrt(NORM2(SPH[i].Pos)+rbulge2);
        double gravfact_bulge = GravitationalConstant*Mbulge/CUBE(Rbulge);
        SPH[i].Acc[0] -= gravfact_bulge*SPH[i].Pos[0];
        SPH[i].Acc[1] -= gravfact_bulge*SPH[i].Pos[1];
        SPH[i].Acc[2] -= gravfact_bulge*SPH[i].Pos[2];
#else
        // Disk potential (Wada 1994)
        double Rdisk = sqrt(SQ(SPH[i].Pos[0])+SQ(SPH[i].Pos[1])+SQ(2.0));
        double gravfact_disk = Phi_bar/CUBE(Rdisk);
        SPH[i].Acc[0] -= gravfact_disk*SPH[i].Pos[0];
        SPH[i].Acc[1] -= gravfact_disk*SPH[i].Pos[1];
#endif

        // Bar potential (Wada 1994)
        double Rbar2 = SQ(SPH[i].Pos[0])+SQ(SPH[i].Pos[1]);
        double phi_bar = atan2(SPH[i].Pos[1],SPH[i].Pos[0]);
        double cos_bar = cos(2.0*phi_bar-2.0*Omega_bar*ThisRun.TCurrent);
        double Rbar_factor = Rbar2/r_bar2;
        double gravfact_bar = epsilon_bar*Phi_bar*cos_bar*(2.0/(r_bar2*SQ(1.0+Rbar_factor))-4.0*Rbar2/(r_bar4*CUBE(1.0+Rbar_factor)));
        SPH[i].Acc[0] -= gravfact_bar*SPH[i].Pos[0];
        SPH[i].Acc[1] -= gravfact_bar*SPH[i].Pos[1];

        // Here the value of du is set to zero to enforce gas in the isothermal phase.
        SPH[i].du = 0.e0;
    }

    return ;
}

/*
 * This function writes the rotation curve of the adopted potentials and the
 * rotation speed of that as a function of the galactocentric distance.
 */
static void WriteRC(void){
    
    int Ntarget = 101;

    // Keep first Ntarget particles' position
    double dx = 10.0/(double)Ntarget;
    double Pos[Ntarget][3];
    for(int i=0;i<Ntarget;i++){
        Pos[i][0] = SPH[i].Pos[0];
        Pos[i][1] = SPH[i].Pos[1];
        Pos[i][2] = SPH[i].Pos[2];

        SPH[i].Pos[0] = dx*(i+0.5); 
        SPH[i].Pos[1] = 0.e0; 
        SPH[i].Pos[2] = 0.e0; 
    }

    AccelerationByMilkyWayPotentialHaloDisk();

    FILE *fp;
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/rc.dat",ThisRun.OutDir);
    FileOpen(fp,fname,"w");
    for(int i=0;i<Ntarget-1;i++){
        double Acc = fabs(SPH[i].Acc[0]);
        double r = SPH[i].Pos[0];
        double Vel = sqrt(Acc*r)/((1.e+5/3.086e+21)/(1.0/3.15576e+15)); //in km/s
        double Omega = Vel/r; // in km/s/kpc

        double Vel1 = sqrt(fabs(SPH[i+1].Acc[0])*SPH[i+1].Pos[0])/((1.e+5/3.086e+21)/(1.0/3.15576e+15)); //in km/s
        double Omega1 = Vel1/SPH[i+1].Pos[0];
        double kappa = sqrt(r*(SQ(Omega1)-SQ(Omega))/dx+4.0*SQ(Omega));

        fprintf(fp,"%g %g %g %g %g %g\n",r,Vel,Omega,Omega+kappa/2.0,Omega-kappa/2.0,Omega_bar_log); 
    }
    fclose(fp);

    // Restore the fist 20 particles' positions
    for(int i=0;i<Ntarget;i++){
        SPH[i].Pos[0] = Pos[i][0];
        SPH[i].Pos[1] = Pos[i][1];
        SPH[i].Pos[2] = Pos[i][2];
    }

    // Flush Acc
    for(int i=0;i<ThisRun.NParticles;i++){
        SPH[i].Acc[0] = 
        SPH[i].Acc[1] = 
        SPH[i].Acc[2] = 0.e0;
    }

    return ;
}


/*
 * This function generates the initial condition of the milkyway-like disk
 * galaxy model.
 *
 * The simulation units for the length, mass, and time are 1 Kpc, 10^10 Msun,
 * and 10^8 yr, respectively.
 *
 * In cgs units, UnitLenght = 3.086e+21 [cm], 
 *               UnitMass   = 1.989e33 [g],
 *               UnitTime   = 3.15576e+15 [sec].
 *
 * The current simulation setup is for the two dimentional runs, but is easy to
 * extend to the three dimensiotnal runs.
 *
 * Note that for simplicity, this model does not solve the energy equation by
 * assuming that du/dt term is always zero, in order to mimic the isothermal
 * EoS. 
 */
void InitGalaxyModelTest(void){

    InitXorshift(1977);

    double Mgas = 0.8;  // times 10^10 Msun 
    double Rdisk = 5.3; // Kpc
    double DiskEdgeFactor = 1.5; 

    double fact = (1-exp(-DiskEdgeFactor)*(1+DiskEdgeFactor));
    double mass = Mgas/(double)ThisRun.NParticles;

    // Gravitational constant
    double GravitationalConstant = (6.6725985e-8*(1.989e33*1.e10*SQ(3.15576e+15))/CUBE(3.086e+21));

    vprint(GravitationalConstant);

    double InitialKernelSize = sqrt((SQ(DiskEdgeFactor*Rdisk))/(double)ThisRun.NParticles);
    fprintf(stderr,"Initial Kernel Size : %g kpc\n",InitialKernelSize);

    double InitialDensity = Mgas/sqrt((SQ(DiskEdgeFactor*Rdisk))/(double)ThisRun.NParticles);

    double ConvertTtoU = (3.0/2.0)*(1.38065812e-16/(0.59*1.672623110e-24))*(SQ(3.15576e+15/3.086e+21));
    double InitialSpecificInternalEnergy = 10000*ConvertTtoU;

    SPH = malloc(sizeof(struct StructSPH)*ThisRun.NParticles);

    double rend = 0;
    double Mass = 0;
    int Index = 0;
    for(int i=0;i<ThisRun.NParticles;i++){
        double M = mass*(Index+1);
        double F = fact*M/Mgas;

        // solve about r.
        double r = ReturnDistanceForExponent(F,2.0*Rdisk);
        double theta = 2.0*M_PI*XorshiftRand();

        SPH[i].Pos[0] = r*cos(theta);
        SPH[i].Pos[1] = r*sin(theta);
        SPH[i].Pos[2] = 0.e0;

        // Initialize velocities.
        SPH[i].Vel[0] = 0.e0;
        SPH[i].Vel[1] = 0.e0;
        SPH[i].Vel[2] = 0.e0;

        SPH[i].Rho = InitialDensity;
        SPH[i].u = InitialSpecificInternalEnergy;
        SPH[i].Kernel = InitialKernelSize;
        SPH[i].Mass = mass;
        if(ThisRun.UseDISPH == 1){
            SPH[i].q = SPH[i].Rho*SPH[i].u;
        } 

        SPH[i].du = 0.e0;
        SPH[i].Pos[2] = SPH[i].Vel[2] = 
        SPH[i].Acc[0] = SPH[i].Acc[1] = SPH[i].Acc[2] = 0.e0;
        SPH[i].Alpha = 0.1;

        Mass += mass;
        Index ++;
        rend = r;
    }

    WriteRC();

    AccelerationByMilkyWayPotentialHaloDisk();

    for(int i=0;i<ThisRun.NParticles;i++){
        double Acc = sqrt(SQ(SPH[i].Acc[0])+SQ(SPH[i].Acc[1]));
        double r = sqrt(SQ(SPH[i].Pos[0])+SQ(SPH[i].Pos[1]));
        if(r>1.e-30){
            SPH[i].Vel[0] = -sqrt(Acc/r)*SPH[i].Pos[1];
            SPH[i].Vel[1] = +sqrt(Acc/r)*SPH[i].Pos[0];
        } else {
            SPH[i].Vel[0] = 0.e0;
            SPH[i].Vel[1] = 0.e0;
        }
        SPH[i].Acc[0] = SPH[i].Acc[1] = SPH[i].Acc[2] = 0.e0;
    }

    fprintf(stderr,"Finish initial setup.\n");

    return ;
}

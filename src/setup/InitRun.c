#include "../config.h"
#include "../NeighborSearch.h"
#include "../KernelFunctions.h"
#include "../Run.h"
#include "SetupShocktube.h"
#include "SetupSurfaceTension.h"
#include "SetupKH.h"
#include "SetupRT.h"
#include "SetupSedov.h"
#include "SetupKeplar.h"
#include "SetupGalaxyModel.h"
#include "SetupEvrard.h"

/*! \file InitRun.c
 * \brief Initial conditions.
 */

/*!
 * Set parameters which require calculations of the input parameters.
 */
void SetParameters(void){

    ThisRun.Gm1 = ThisRun.Gamma-1.e0;
    ThisRun.GGm1 = ThisRun.Gamma*ThisRun.Gm1;
    ThisRun.NBmin = ThisRun.Ns-ThisRun.Nspm;
    ThisRun.NBmax = ThisRun.Ns+ThisRun.Nspm;
    ThisRun.dt_output = ThisRun.TEnd/(double)ThisRun.OutputFileNumber;
    ThisRun.TNorm = 1.0;

    return ;
}


/*!
 * The initial setup of the simulation is done in this function.
 */
void InitRun(void){

    SetParameters();
    InitKernelFunction();

    if(ThisRun.RunType == 0){
        fprintf(stderr,"Shocktube test.\n");
        InitSodShockTest();
    } else if(ThisRun.RunType == 1){
        fprintf(stderr,"Surface tension test.\n");
        InitSurfaceTensionTest(0,4);
    } else if(ThisRun.RunType == 2){
        fprintf(stderr,"Kelvin-Helmholtz instability test.\n");
        InitKHTest(1);
    } else if(ThisRun.RunType == 3){
        fprintf(stderr,"Rayleigh-Taylor instability test.\n");
        InitRTTest();
    } else if(ThisRun.RunType == 4){
        fprintf(stderr,"Sedov test.\n");
        //InitSedovTest();
        InitSedovGlassTest();
    } else if(ThisRun.RunType == 5){
        fprintf(stderr,"Keplar disk test.\n");
        InitKeplarTest();
    } else if(ThisRun.RunType == 6){
        fprintf(stderr,"Galaxy model test.\n");
        InitGalaxyModelTest();
    } else if(ThisRun.RunType == 7){
        fprintf(stderr,"Evrard test.\n");
        InitEvrardTest();
    } 

    // Impose the periodic boundary condition.
    PeriodicWrapping();

    return ;
}


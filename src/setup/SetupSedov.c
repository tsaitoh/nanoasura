#include "../config.h"
#include "../NeighborSearch.h"
#include "../KernelFunctions.h"
#include "../Run.h"

/*! \file SetupSedov.c
 * \brief Initial condition for the Sedov test.
 */

static bool SedovCheckNBList(double *KernelSize, const int Nlist, const int NSmooth, double *Left, double *Right){
    const int Nspm = 8;
    const int _SedovNBmin = NSmooth-Nspm;
    const int _SedovNBmax = NSmooth+Nspm;

    if((_SedovNBmin<=Nlist)&&(Nlist<=_SedovNBmax)){
        return true;
    } else if((Nlist<=(_SedovNBmax))&&(*Right>0.e0)&&(*Left>0.e0)){
        if((*Right)-(*Left) < 1.e-2*(*Left)){
            return true;
        }
    }

    if(Nlist < _SedovNBmin){
        *Left = fmax(*Left,*KernelSize);
    } else if(Nlist>_SedovNBmax){
        if(*Right >0.e0){
            *Right = fmin(*Right,*KernelSize);
        } else {
            *Right = *KernelSize;
        }
    }

#define KernelFactInc   (1.14) // 1.5 ^ (1.3)
#define KernelFactDec   (0.79) // 0.75 ^ (1.3)

    if((*Left>0.e0)&&(*Right>0.e0)){
        *KernelSize = cbrt(0.5*(CUBE(*Left)+CUBE(*Right)));
    } else {
        if(iszero(*Right)&&(*Left>0.e0)){
            *KernelSize *= KernelFactInc;
        } else if((*Right>0.e0)&&iszero(*Left)){
            *KernelSize *= KernelFactDec;
        }
    }
#undef KernelFactInc
#undef KernelFactDec

    return false;
}


static double SedovSmoothedRadius(const int index, const int NSmooth){

    double KernelSize = SPH[index].Kernel;
    gprint(KernelSize);
    double Left = 0.e0;
    double Right = 0.e0;
    int Nlist;
    do{
        Nlist = GetNeighborNumberDirect(SPH[index].Pos,2.0*KernelSize);
    } while(SedovCheckNBList(&KernelSize,Nlist,NSmooth,&Left,&Right) == false);

    return KernelSize;
}

static inline double SPHKernel_Sedov(const double r, const double Kernel){

    double InvKernel = 1.0/Kernel;
    double u = r*InvKernel;
    const double coef3d = M_1_PI;
    double coef = coef3d*CUBE(InvKernel);
    if(u<1.e0){
        return (coef*(1.e0-1.5*SQ(u)+0.75*CUBE(u)));
    } else if(u<2.e0){
        return (coef*(0.25*CUBE(2.e0-u)));
    } else {
        return 0.e0;
    }
}

void InitSedovTest(void){

    double rho = 1.0;
    int NGrid = ThisRun.NParticles;
    ThisRun.NParticles = CUBE(NGrid);
    double mgas = 1.0/ThisRun.NParticles;
    double u_h = 1.e0/mgas;
    double u_l = 1.e-6/mgas;

    SPH = malloc(sizeof(struct StructSPH)*ThisRun.NParticles);

    double dx = 1.0/(double)NGrid;
    int counter = 0;
    for(int i=0;i<NGrid;i++){
        for(int j=0;j<NGrid;j++){
            for(int k=0;k<NGrid;k++){
                SPH[counter].Pos[0] = dx*(i+0.5);
                SPH[counter].Pos[1] = dx*(j+0.5);
                SPH[counter].Pos[2] = dx*(k+0.5);

                SPH[counter].Vel[0] = SPH[counter].Vel[1] = SPH[counter].Vel[2] = 0.e0;

                SPH[counter].Mass = mgas;
                SPH[counter].Rho = rho;
                SPH[counter].Kernel = dx;
                SPH[counter].u = u_l;
                SPH[counter].Alpha = 1.0;

                counter ++; 
            }
        }
    }

    // Add energy at the center.
    int search_center_id = 0;
    double Center[3] = {0.5,0.5,0.5};
    double dist2_min = DISTANCE2(Center,SPH[0].Pos);
    for(int i=0;i<ThisRun.NParticles;i++){
        double d2 = DISTANCE2(Center,SPH[i].Pos);
        if(dist2_min > d2){
            dist2_min = d2;
            search_center_id = i;
        }
    }
    fprintf(stderr,"Center particle id : %d\n",search_center_id);
    fprintf(stderr,"Center particle pos : %g %g %g\n",
            SPH[search_center_id].Pos[0],SPH[search_center_id].Pos[1],SPH[search_center_id].Pos[2]);
    // SPH[search_center_id].u = u_h;
    
    int Neighbors[MaxNeighborSize];
    int nlist = GetNeighborsDirect(SPH[search_center_id].Pos,
            2.0*SPH[search_center_id].Kernel,Neighbors);
    dprint(nlist);

    double wt = 0.e0;
    for(int i=0;i<nlist;i++){
        int nbindex = Neighbors[i];
        double r = DISTANCE(SPH[search_center_id].Pos,SPH[nbindex].Pos); 
        wt += SPHKernel_Sedov(r,SPH[search_center_id].Kernel);
    }
    gprint(wt);

    for(int i=0;i<nlist;i++){
        int nbindex = Neighbors[i];
        double r = DISTANCE(SPH[search_center_id].Pos,SPH[nbindex].Pos); 
        SPH[nbindex].u += u_h*SPHKernel_Sedov(r,SPH[search_center_id].Kernel)/wt;
    }

    // exit(1);

    return ;
}

/*!
 * This function loads the initial condition of the Sedov test. The initial
 * condition is the same as that used in Saitoh & Makino (2013).
 */
void InitSedovGlassTest(void){


    // Read file
    FILE *fp;
    FileOpen(fp,ThisRun.ICFile,"r");
    int counter = 0;
    while(fscanf(fp,"%*d %*e %*e %*e %*e %*e %*e") != EOF){
        counter ++;
    }
    fclose(fp);

    ThisRun.NParticles = counter;
    SPH = malloc(sizeof(struct StructSPH)*ThisRun.NParticles);

    FileOpen(fp,ThisRun.ICFile,"r");
    for(int i=0;i<ThisRun.NParticles;i++){
        fscanf(fp,"%*d %le %le %le %le %le %le",
            &(SPH[i].Pos[0]),&(SPH[i].Pos[1]),&(SPH[i].Pos[2]),
            &(SPH[i].Kernel),&(SPH[i].u),&(SPH[i].Mass));
            SPH[i].Vel[0] = SPH[i].Vel[1] = SPH[i].Vel[2] = 0.e0;
            SPH[i].Rho = 1.e0;

        SPH[i].du =
        SPH[i].Acc[0] =
        SPH[i].Acc[1] =
        SPH[i].Acc[2] = 0.e0;
        SPH[i].Alpha = 3.0;
    }
    fclose(fp);

    // search the hottest particle.
    int id = 0; 
    for(int i=0;i<ThisRun.NParticles;i++){
        if(SPH[i].u > SPH[id].u){
            id = i;
        }
    }

    // center shift
    double center[3] = {SPH[id].Pos[0],SPH[id].Pos[1],SPH[id].Pos[2]};
    for(int i=0;i<ThisRun.NParticles;i++){
        SPH[i].Pos[0] -= center[0];
        SPH[i].Pos[1] -= center[1];
        SPH[i].Pos[2] -= center[2];
        SPH[i].Pos[0] += 0.5;
        SPH[i].Pos[1] += 0.5;
        SPH[i].Pos[2] += 0.5;
    }

    // search the lowest temperature.
    int id_low = 0; 
    for(int i=0;i<ThisRun.NParticles;i++){
        if(SPH[i].u < SPH[id_low].u){
            id_low = i;
        }
    }
    fprintf(stderr,"The ambient particle's temperature is %g\n",SPH[id_low].u);

    int NSmooth = 128;
    double SmoothedRadius = SedovSmoothedRadius(id,NSmooth);
    gprint(SmoothedRadius);

    int Neighbors[MaxNeighborSize];
    int nlist = GetNeighborsDirect(SPH[id].Pos,2.0*SmoothedRadius,Neighbors);

    dprint(nlist);

    double wt = 0.e0;
    for(int i=0;i<nlist;i++){
        int nbindex = Neighbors[i];
#ifdef PERIODIC
		double r2 = 0.e0;
        for(int k=0;k<DIMENSION;k++)
            r2 += SQ(PeriodicDistance(SPH[id].Pos[k],SPH[nbindex].Pos[k],k));
#else
		double r2 = DISTANCE2(SPH[id].Pos,SPH[nbindex].Pos);
#endif
        double r = sqrt(r2);
        wt += SPHKernel_Sedov(r,SmoothedRadius);
    }
    gprint(wt);


    double u_h = 1.e0/SPH[0].Mass;
    double u_l = 1.e-6/SPH[0].Mass;

    for(int i=0;i<ThisRun.NParticles;i++){
        SPH[i].u = u_l;
    }

    for(int i=0;i<nlist;i++){
        int nbindex = Neighbors[i];
#ifdef PERIODIC
		double r2 = 0.e0;
        for(int k=0;k<DIMENSION;k++)
            r2 += SQ(PeriodicDistance(SPH[id].Pos[k],SPH[nbindex].Pos[k],k));
#else
		double r2 = DISTANCE2(SPH[id].Pos,SPH[nbindex].Pos);
#endif
        double r = sqrt(r2);
        SPH[nbindex].u += u_h*SPHKernel_Sedov(r,SmoothedRadius)/wt;
    }

    fprintf(stderr,"Finish initial setup.\n");

    return ;
}

#include "../config.h"

/*! \file SetupSurfaceTension.c
 * \brief Initial condition for the surface tension test.
 */


/*!
 * This function generate the initial condition of the two-dimensional
 * hydrostatic test.  The reference of this test is Saitoh & Makino (2013).
 */
void InitSurfaceTensionTest(const int FlagEqualMass, const double DensityRatio){

    int Base = (int)(sqrt(DensityRatio)+0.5);

    double rho_h = SQ(Base);
    double rho_l = 1.0;
    double p_h = 2.5;
    double p_l = 2.5;
    double u_h = p_h/(ThisRun.Gm1*rho_h);
    double u_l = p_l/(ThisRun.Gm1*rho_l);

    int NGrid = ThisRun.NParticles;
    double dx = 1.0/(double)ThisRun.NParticles;
    int counter = 0;
    for(int i=0;i<NGrid;i++){
        for(int k=0;k<NGrid;k++){
            double x = dx*(i+0.5);
            double y = dx*(k+0.5);
            if((x>0.25)&&(x<0.75)&&(y>0.25)&&(y<0.75)){
                if(FlagEqualMass == 1){
                    counter += SQ(Base);
                } else {
                    counter ++;
                }
            }  else {
                counter ++;
            }
        }
    }
    ThisRun.NParticles = counter;
    
    double mgas = 1.0/(SQ(NGrid));

    SPH = malloc(sizeof(struct StructSPH)*ThisRun.NParticles);

    counter = 0;
    for(int i=0;i<NGrid;i++){
        for(int k=0;k<NGrid;k++){
            double x = dx*(i+0.5);
            double y = dx*(k+0.5);
            if((x>0.25)&&(x<0.75)&&(y>0.25)&&(y<0.75)){
                if(FlagEqualMass){
                    double dxx = dx/Base;
                    for(int ii=0;ii<Base;ii++){
                        for(int kk=0;kk<Base;kk++){
                            double xx = x + dxx*(ii-0.5);
                            double yy = y + dxx*(kk-0.5);

                            SPH[counter].Pos[0] = xx;
                            SPH[counter].Pos[1] = yy;
                            SPH[counter].Pos[2] = 0.0;
                            SPH[counter].Vel[0] = SPH[counter].Vel[1] = SPH[counter].Vel[2] = 0.0;

                            SPH[counter].Rho = rho_h;
                            SPH[counter].Kernel = 2*dxx;
                            SPH[counter].Mass = mgas;
                            SPH[counter].u = u_h;
                            SPH[counter].du = SPH[counter].Acc[0] = SPH[counter].Acc[1] = SPH[counter].Acc[2] = 0.e0;
                            SPH[counter].Alpha = 1.0;
                            SPH[counter].Tag = 1;

                            counter ++;
                        }
                    }
                } else {
                    SPH[counter].Pos[0] = x;
                    SPH[counter].Pos[1] = y;
                    SPH[counter].Pos[2] = 0.0;
                    SPH[counter].Vel[0] = SPH[counter].Vel[1] = SPH[counter].Vel[2] = 0.0;

                    SPH[counter].Rho = rho_h;
                    SPH[counter].Kernel = 2*dx;
                    SPH[counter].Mass = mgas*SQ(Base);
                    SPH[counter].u = u_h;
                    SPH[counter].du = SPH[counter].Acc[0] = SPH[counter].Acc[1] = SPH[counter].Acc[2] = 0.e0;
                    SPH[counter].Alpha = 1.0;
                    SPH[counter].Tag = 1;

                    counter ++;
                }
            }  else {
                SPH[counter].Pos[0] = x;
                SPH[counter].Pos[1] = y;
                SPH[counter].Pos[2] = 0.0;
                SPH[counter].Vel[0] = SPH[counter].Vel[1] = SPH[counter].Vel[2] = 0.0;

                SPH[counter].Rho = rho_l;
                SPH[counter].Kernel = 2*dx;
                SPH[counter].Mass = mgas;
                SPH[counter].u = u_l;
                SPH[counter].du = SPH[counter].Acc[0] = SPH[counter].Acc[1] = SPH[counter].Acc[2] = 0.e0;
                SPH[counter].Alpha = 1.0;
                SPH[counter].Tag = 0;

                counter ++;
            }
        }
    }

    fprintf(stderr,"Finish initial setup.\n");

    return ;
}


#include "../config.h"

/*! \file SetupRT.c
 * \brief Initial condition for RT.
 */


/*!
 * This function generates the initial condition of the Rayleigh-Taylor
 * instability test. The initial condition is the same as that used in Saitoh &
 * Makino (2013).
 */
void InitRTTest(void){

    double p_0 = 10.0/7.0;
    double g_0 = -0.5;
    double rho_h = 2.0;
    double rho_l = 1.0;
    double A_h = p_0/pow(rho_h,ThisRun.Gamma);
    double A_l = p_0/pow(rho_l,ThisRun.Gamma);

    int ngrid = ThisRun.NParticles;
    //ThisRun.NParticles = SQ(ngrid);
    ThisRun.NParticles = ngrid*ngrid/4;
    double dx = 1.0/ngrid;
    double ds = SQ(dx);

    SPH = malloc(sizeof(struct StructSPH)*ThisRun.NParticles);

#define RT_VEL  (0.1)

    int counter = 0;
    for(int i=0;i<ngrid/4;i++){
        for(int j=0;j<ngrid;j++){
            double x = (0.5 + i) * dx;
            double y = (0.5 + j) * dx;
            SPH[counter].Kernel = 2*dx;
            SPH[counter].Pos[0] = x;
            SPH[counter].Pos[1] = y;
            SPH[counter].Pos[2] = 0.5;

            SPH[counter].Vel[0] = 
            SPH[counter].Vel[1] = 
            SPH[counter].Vel[2] = 0.e0;
            if((SPH[counter].Pos[1]>0.3) && (SPH[counter].Pos[1]<0.7)){
                SPH[counter].Vel[1] = 0.25*RT_VEL*
                    (1.0+cos(8.0*M_PI*(SPH[counter].Pos[0]+0.25)))*
                    (1.0+cos((2.0/0.4)*M_PI*(SPH[counter].Pos[1]-0.5)));
            }
            if(SPH[counter].Pos[1] < 0.5){
                double rho = pow(pow(rho_l,ThisRun.Gm1)-ThisRun.Gm1*(g_0/(A_l*ThisRun.Gamma))*(0.5-y)
                            ,1.0/ThisRun.Gm1);
                SPH[counter].Rho = rho;
                SPH[counter].Mass = rho*ds;
                SPH[counter].u = (A_l/ThisRun.Gm1)*pow(rho,ThisRun.Gm1);
                SPH[counter].Tag = 1;
            } else {
                double rho = pow(pow(rho_h,ThisRun.Gm1)+ThisRun.Gm1*(g_0/(A_h*ThisRun.Gamma))*(y-0.5)
                                ,1.0/ThisRun.Gm1);
                SPH[counter].Rho = rho;
                SPH[counter].Mass = rho*ds;
                SPH[counter].u = (A_h/ThisRun.Gm1)*pow(rho,ThisRun.Gm1);
                SPH[counter].Tag = 2;
            }

            SPH[counter].du =
            SPH[counter].Acc[0] =
            SPH[counter].Acc[1] =
            SPH[counter].Acc[2] = 0.e0;
            SPH[counter].Alpha = 1.0;

            counter ++;
        }
    }


    // Put tag=0 on the boundary particles.
    for(int i=0;i<ThisRun.NParticles;i++){
        if((SPH[i].Pos[1] < 0.1)||(SPH[i].Pos[1]>0.9)){
            SPH[i].Tag = 0;
        }
    }

#undef RT_VEL
    fprintf(stderr,"Finish initial setup.\n");

    return ;
}


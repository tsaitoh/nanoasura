#include "../config.h"

/*! \file SetupShocktube.c
 * \brief Initial condition for the shocktube test.
 */

/*!
 * This function sets initial condition of the one-dimensional shock tube test.
 * The setup is the same as Hernquist & Katz (1989).
 */
void InitSodShockTest(void){

    int NBlock = ThisRun.NParticles/5;
    int NLeft = 4*NBlock;
    int NRight = NBlock;
    ThisRun.NParticles = NLeft + NRight;
    fprintf(stderr,"Total number of particles = %d\n",ThisRun.NParticles);
    fprintf(stderr," Left and right hand side have %d and %d particles.\n",NLeft,NRight);

    SPH = malloc(sizeof(struct StructSPH)*ThisRun.NParticles);

    for(int i=0;i<ThisRun.NParticles;i++){
        if(i < NLeft){
            SPH[i].Pos[0] = (1.0/(4.e0*NBlock))*i;
            SPH[i].Rho = 1.e0;
            SPH[i].u = 1.0/(ThisRun.Gm1*SPH[i].Rho);
            SPH[i].Tag = 0;
        } else {
            SPH[i].Pos[0] = -1.e0+1.e0/(NBlock)*(i-4*NBlock);
            SPH[i].Rho = 0.25;
            SPH[i].u = 0.1795/(ThisRun.Gm1*SPH[i].Rho);
            SPH[i].Tag = 1;
        }
        if(ThisRun.UseDISPH == 1){
            SPH[i].q = SPH[i].Rho*SPH[i].u;
        }
        SPH[i].Kernel = 1.e0/(NBlock);
        SPH[i].Mass = 1.25/((double)ThisRun.NParticles);
        SPH[i].Pos[1] = SPH[i].Pos[2] = 0.e0;
        SPH[i].Vel[0] = SPH[i].Vel[1] = SPH[i].Vel[2] = 0.e0;
        SPH[i].Acc[0] = SPH[i].Acc[1] = SPH[i].Acc[2] = 0.e0;
        SPH[i].Alpha = 1.0;
    }

    fprintf(stderr,"Finish initial setup.\n");
    
    return ;
}


#include "config.h"
/*! \file Vars.c
 * \brief Structures used in the simulation is defined.
 */

struct StructThisRun ThisRun;
struct StructSPH *SPH;
struct StructHydroRoot HydroRoot;
struct StructHydroNode *HydroNode;
struct StructNBCache *NBCache;
struct StructGravityRoot GravityRoot;
struct StructGravityNode *GravityNode;
struct StructGravityCache *GravityCache;
struct StructGravityAccPot *GravityAccPot;

#include "config.h"
#include "PlantGravityTree.h"

/* \file CalcGravity.c
 * \brief Calculate the acceleration and potential energy of the particles. 
 */

static void CalcGravityTree(void);
/*!
 * This function updates gravitational acceleration of all particles.
 */
void UpdateGravity(void){

    if(ThisRun.SolveSelfGravity != 1) return;

    PlantGravityTree();
    CalcGravityTree();

    return ;
}

#define  TreeMaxNodeLevel (20)  //!< Maximum level of the tree structure.
static double WidthFactor[TreeMaxNodeLevel]; //!< Node width factor for each tree level.
static bool first = true;
static void SetWidthFactor(void){
    if(first == false) return;

    double dw = 0.5;
    for(int i=0;i<TreeMaxNodeLevel;i++)
        WidthFactor[i] = pow(dw,(double)i);

    first = false;
    return ;
}

static struct StructGravityAccPot CalcGravityTree_i(double Pos[restrict], double Eps){

    struct StructGravityAccPot AccPot = {0};

    double Eps2 = SQ(Eps);
    double sqTheta = SQ(ThisRun.OpeningAngle);

    int RootNodeID = 0;
	int CurrentNodeID = GravityNode[RootNodeID].Children;
    while(CurrentNodeID != RootNodeID){
        double Dist[3];
        Dist[0] = Pos[0] - GravityNode[CurrentNodeID].Pos[0];
        Dist[1] = Pos[1] - GravityNode[CurrentNodeID].Pos[1];
        Dist[2] = Pos[2] - GravityNode[CurrentNodeID].Pos[2];
        double r2 = NORM2(Dist)+Eps2;
        double sqWidth = SQ(GravityRoot.Width*WidthFactor[GravityNode[CurrentNodeID].Level]);
        if(ThisRun.UseSymmetrizedPlummer){
            r2 += GravityNode[CurrentNodeID].Eps2;
            sqWidth = sqWidth + SQ(GravityNode[CurrentNodeID].EpsMax-GravityNode[CurrentNodeID].EpsMin);
        }

        if((sqWidth<sqTheta*r2)||(GravityNode[CurrentNodeID].Children == NONE)){
            if(GravityNode[CurrentNodeID].Children == NONE){

                int NumberofLeaves = GravityNode[CurrentNodeID].NumberofLeaves;
                int Header = GravityNode[CurrentNodeID].Leaves;
                for(int k=0;k<NumberofLeaves;k++){
                    int Leaf = Header+k;

                    Dist[0] = Pos[0] - GravityCache[Leaf].Pos[0];
                    Dist[1] = Pos[1] - GravityCache[Leaf].Pos[1];
                    Dist[2] = Pos[2] - GravityCache[Leaf].Pos[2];
                    double Distance2 = NORM2(Dist);
                    r2 = Distance2 + Eps2
                        +((ThisRun.UseSymmetrizedPlummer == 0)
                            ?0.e0
                            :SQ(GravityCache[Leaf].Eps));
                    double r = sqrt(r2);
                    double ir = 1.e0/r;
                    double ir3 = ir*ir*ir;
                    AccPot.Acc[0] -= GravityCache[Leaf].Mass*ir3*Dist[0];
                    AccPot.Acc[1] -= GravityCache[Leaf].Mass*ir3*Dist[1];
                    AccPot.Acc[2] -= GravityCache[Leaf].Mass*ir3*Dist[2];
                    AccPot.Pot -= GravityCache[Leaf].Mass*ir;
                    AccPot.InteractionList ++;
                }
            } else {
                r2 = DISTANCE2(Pos,GravityNode[CurrentNodeID].COM);
                double r = sqrt(r2+Eps2
                              +((ThisRun.UseSymmetrizedPlummer==0)
                                 ?0.e0
                                 :GravityNode[CurrentNodeID].Eps2));
                double ir = 1.e0/r;
                double ir3 = ir*ir*ir;
                AccPot.Acc[0] -= GravityNode[CurrentNodeID].Mass*ir3*Dist[0];
                AccPot.Acc[1] -= GravityNode[CurrentNodeID].Mass*ir3*Dist[1];
                AccPot.Acc[2] -= GravityNode[CurrentNodeID].Mass*ir3*Dist[2];
                AccPot.Pot -= GravityNode[CurrentNodeID].Mass*ir;
                AccPot.InteractionList ++;
            }
            CurrentNodeID = GravityNode[CurrentNodeID].Next;
        } else {
            CurrentNodeID = GravityNode[CurrentNodeID].Children;
        }
    }

    return AccPot;

}

static void CalcGravityTree(void){

    SetWidthFactor();

    const double factor = (ThisRun.UseSymmetrizedPlummer==0)?1.0:sqrt(2.0);
#pragma omp parallel for schedule(dynamic, 1)
    for(int i=0;i<ThisRun.NParticles;i++){
        struct StructGravityAccPot AccPot = CalcGravityTree_i(SPH[i].Pos,SPH[i].Eps);
        SPH[i].Acc[0] += ThisRun.GravitationalConstant*AccPot.Acc[0];
        SPH[i].Acc[1] += ThisRun.GravitationalConstant*AccPot.Acc[1];
        SPH[i].Acc[2] += ThisRun.GravitationalConstant*AccPot.Acc[2];
        SPH[i].Pot += ThisRun.GravitationalConstant*AccPot.Pot
                    +SPH[i].Mass/(factor*SPH[i].Eps);
    }

    return ;
}


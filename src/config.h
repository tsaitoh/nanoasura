#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>
#include <tgmath.h>
#include <assert.h>
#include <stdint.h>
#include "Macros.h"
#include "Constants.h"
#include "Utilities.h"
#include "Vars.h"
#include "Log.h"
#include "PeriodicWrapping.h"
#include "ElapsedTime.h"
#include "xorshift.h"
#ifdef USE_OPENMP
#include "omp.h"
#endif


#pragma once

/*! @struct StructThisRun
 * A structure which contains all of run status.
 */
extern struct StructThisRun{

    int NParticles; //!< Number of particles in the run. 
    int RunType;  //!< Select type of simulation. 0:1D shocktube, 1:2D hydrostatic test, 2:2D Kelvin-Helmholtz instability test, 3:2D Rayleigh-Taylor instability test.
    char OutDir[MaxCharactersInLine];   //!< Data output directory.
    char RunName[MaxCharactersInLine];  //!< Run name.
    char ICFile[MaxCharactersInLine];   //!< IC file name.

    int Dimension;        //!< Dimension.
    int PeriodicBoundary; //!< Periodic boundary condition flag. If this flag is 1, the periodic consition is imposed.
    double LBox[3];       //!< Simulation box size for the run with the periodic boundary condition.

    double dt;       //!< Timestep.
    double TCurrent; //!< Current time.
    double TEnd;     //!< End time.
    int NStep;       //!< Total steps.
    double TNorm;    //!< Normalization of the time.
    double dt_output; //!< Output timestep.
    double CFL;       //!< Coefficient for the CFL timestep.

    int SolveSelfGravity;     //!< Solve gravity flag. If this flag is 1, gravity is solved.
    int UseSymmetrizedPlummer;//!< Symmetrized Plummer model flag. If this flag is 1, the symmetrized Plummer model is adopted.
    double OpeningAngle;      //!< The opening angle for the gravity.
    int GroupSize;            //!< The group size which shares the same interaction list for the gravity calculation.
    double GravitationalConstant;      //!< The gravitational constant.

    int UseDISPH;              //!< This flag controls the type of SPH. If the value is 1, DISPH is adopted. If it is 0, Standard SPH is used.
    int SelectKernelType;      //!< The kernel type can be selected by this parameter. The values of 0, 1, 2, 3 corresponds to the cubic spline kernel, Wendland C2, C4, and C6 kernels, respectively.
    int KernelEvaluationType;  //!< This flag cotrols the type of the kernel size evaluation procedure. If the flag is 0, ASURA determines the kernel size in which \f$N_{\rm s} \pm N_{\rm spm}\f$ neighbor particles are included. If 1, ASURA uses \f$h = \eta (m/\rho)^{1/D}\f$.
    double KernelEta;          //!< This is a factor which is used in the evaluation of the kernel size if KernelEvaluationType == 1.
    int UseGradh;              //!< This flag controls the use of the grad-h term.
    int UseGradN;              //!< This flag controls the use of the grad-N term.
    int DerivativeOperatorType;//!< If 0, the code adopts the standard derivative operators. If 1, it uses the derivative operators shown in Hosono, Saitoh \& Makino (2016). 

    double Gamma;              //!< Specific heat ratio \f$\gamma\f$.
    double Gm1;                //!< \f$\gamma-1\f$.
    double GGm1;               //!< \f$\gamma(\gamma-1)\f$.

    int Ns;    //!< Number of neighbor partilces; \f$N_{\rm s}\f$.
    int Nspm;  //!< Tolerance of the neighbor particles; \f$N_{\rm s} \pm N_{\rm spm}\f$.
    int NBmin; //!< Minimum number of the neighbor particles; \f$N_{\rm s}-N_{\rm spm}\f$.
    int NBmax; //!< Maximum number of the neighbor particles; \f$N_{\rm s}+N_{\rm spm}\f$.

    int ViscType;              //!< Viscosity type.
    double ViscAlpha;          //!< Viscosity coefficient.
    int UseVariableAlpha;      //!< This flag controls the use of the variable alpha mode.
    double ViscAlphaMin;       //!< Min alpha used in the variable alpha mode.
    double ViscAlphaMax;       //!< Max alpha used in the variable alpha mode.
    int ViscBalsara;           //!< Balsara limiter flag. If this flag is 1, the run uses the Balsara limiter.
    double ViscSignalVelocityBeta; //!< A coefficient, \f$\beta\f$, for the signal velocity. The definition of the signal velocity between particles \f$i\f$ and \f$j\f$ is \f$v_{{\rm sig},ij} = c_{\rm i} +c_{\rm j} -\beta \boldmath r \cdot \boldmath v/|r|\f$, where \f$c\f$ is the sound speed, \f$r\f$ and \f$v\f$ are the distance and relative velocity between particles \f$i\f$ and \f$j\f$, respectively.

    int OutputFileNumber;  //!< Number of output file.
    int WriteEveryStep;    //!< If this flag is 1, the log files are written every timestep. "OutputFileNumber" is ignored.
    int ShowLog;           //!< Verbose mode flag. If this flag is 1, you can see logs during the simulation.

    double Tstart;         //!< The beginning time of this run.
    double Tfin;           //!< The finishing time of this run

    int ThreadNumber;   //!< Thread number for OpenMP parallelization.
    int DynamicChunk;   //!< Chunk size of OpenMP parallelization.
} ThisRun; 

/*! @struct StructSPH
 * A structure which contains all information of a SPH particle
 */
extern struct StructSPH{
    int Nlist;         //!< Number of neighbor particles
    double Pos[3];     //!< Position
    double Vel[3];     //!< Velocity
    double Velh[3];    //!< Velocity at the intermediate step
    double Acc[3];     //!< Acceleration
    double Pot;        //!< Potential
    double Mass;       //!< Mass
    double Eps;        //!< Gravitational softening length
    double Rho;        //!< Density evaluated by \f$\rho=\sum_j m_j W(r_{ij},h_i)\f$
    double Kernel;     //!< Kernel size
    double q;          //!< Energy Density used in the DISPH mode. The definition is \f$q=\sum_j U_j W(r_{ij},h_i)\f$
    double DivV;          //!< Divergence of velocity
    double GradV[3];      //!< Gradient of velocity
    double RotV[3];       //!< Rotation of velocity
    double F;             //!< Balsara limiter
    double Gradh;         //!< Grad-h term
    double NumberDensity; //!< Number density defined by \f$n=\sum_j W(r_{ij},h_i)\f$
    double GradN;         //!< Grad-N term
    double Vsig;          //!< Signal velocity
    double u;             //!< Specific internal energy
    double uh;            //!< Specific internal energy at the intermediate step
    double du;            //!< Time derivative of the specific internal energy
    double Alpha;         //!< Viscosity factor used in the variable alpha mode
    double B[3][3];
    int Tag;              //!< Tag
} *SPH;


/*! @struct StructHydroRoot
 * A structure which contains all information of the root node of the tree
 * structure
 */
extern struct StructHydroRoot{
    int NumberofLeaves;            //!< Number of particles contained in this tree.
    int NumberofAllocatedLeaves;   //!< Maximum allocated memory for *Leaves.
    int *Leaves;                   //!< The morton-ordered local particles index list.
    int NumberofNodes;             //!< Number of tree nodes.
    int NumberofAllocatedNodes;    //!< Number of allocated tree nodes.
    int NumberofNodeCreationLimit; //!< Maximum number of particles for the lowest node.

    short CurrentMaxLevel;         //!< Current maximum level of this tree. 
    short MaxLevel;                //!< Maximum level of the tree structure.

    double PosMax[3];              //!< Maximum corner of particle distribution. 
    double PosMin[3];              //!< Minimum corner of particle distribution. 
    double Width;                  //!< Width of the tree root node.
} HydroRoot;

/*! @struct StructHydroNode
 * A structure which contains all information of each node of the tree
 * structure
 */
extern struct StructHydroNode{
    int Next;                 //!< Next node index.
    int Parent;               //!< Parent node index.
    int Children;             //!< First child node index.
    int Sister;               //!< Sister node index.

    short Level;              //!< Level of this node.
    short NumberofChildren;   //!< Number of child-nodes.
    int Leaves;               //!< Index which points the head of the member list. 
    int NumberofLeaves;       //!< Number of particles containd in this node.

    double  Pos[3];           //!< Center of this node.
    double  KernelMax;        //!< The maximum length of kernels.
    double  DistanceMax;      //!< The maximum distance between particles and the center of this node.
} *HydroNode;

/*! @struct StructNBCache
 * A structure which contains all information of each node of the tree
 * structure
 */
extern struct StructNBCache{
    double Pos[3];            //!< Position of particle
    double Kernel;            //!< Kernel size of particle
    int  Leaf;                //!< ID to refer the original particle
} *NBCache;

/*! @struct StructGravityRoot
 * A structure which contains all information of the root node of the tree
 * structure
 */
extern struct StructGravityRoot{
    int NumberofLeaves;            //!< Number of particles contained in this tree.
    int NumberofAllocatedLeaves;   //!< Maximum allocated memory for *Leaves.
    int *Leaves;                   //!< The morton-ordered local particles index list.
    int NumberofNodes;             //!< Number of tree nodes.
    int NumberofAllocatedNodes;    //!< Number of allocated tree nodes.
    int NumberofNodeCreationLimit; //!< Maximum number of particles for the lowest node.
    int NumberofLeavesInGroup;     //!< Maximum number of leaves who shares the same interaction list.

    short CurrentMaxLevel;         //!< Current maximum level of this tree. 
    short MaxLevel;                //!< Maximum level of the tree structure.

    double PosMax[3];              //!< Maximum corner of particle distribution. 
    double PosMin[3];              //!< Minimum corner of particle distribution. 
    double Width;                  //!< Width of the tree root node.

} GravityRoot;

/*! @struct StructGravityNode
 * A structure which contains all information of each node of the tree
 * structure
 */
extern struct StructGravityNode{
    int Next;                 //!< Next node index.
    int Parent;               //!< Parent node index.
    int Children;             //!< First child node index.
    int Sister;               //!< Sister node index.

    short Level;              //!< Level of this node.
    short NumberofChildren;   //!< Number of child-nodes.
    int Leaves;               //!< Index which points the head of the member list. 
    int NumberofLeaves;       //!< Number of particles containd in this node.

    double  Pos[3];           //!< Center of this node.
    double  COM[3];           //!< Center of mass of this node.
    double  Mass;             //!< Mass of this node.
    double  Eps2;             //!< Mass weighted (softening length)^2.
    double  EpsMin;           //!< The minimum gravitational softening length.
    double  EpsMax;           //!< The maximum gravitational softening length.
    double  DistanceMax;      //!< The maximum distance between particles and the center of this node.
} *GravityNode;

/*! @struct StructGravityCache
 * A structure which contains all information of each node of the tree
 * structure
 */
extern struct StructGravityCache{
    double Pos[3];            //!< Position of particle
    double Mass;              //!< Mass of particle
    double Eps;               //!< Gravitational softening length
    int  Leaf;                //!< ID to refer the original particle
} *GravityCache;

extern struct StructGravityAccPot{
    double Acc[3];
    double Pot;
    int  InteractionList;
} *GravityAccPot;



#pragma once

#define MaxCharactersInLine (256)    //!< Maximum character number in a string.
#define MaxNeighborSize     (1024)   //!< Maximum neighbor number.

#define	SQ(x)		((x)*(x))        //!< Evaluate \f$x^2\f$.
#define	CUBE(x)		((x)*(x)*(x))    //!< Evaluate \f$x^3\f$.
#define	MAX(x,y)	(((x)>(y))?(x):(y)) //!< Select larger value.
#define	MIN(x,y)	(((x)<(y))?(x):(y)) //!< Select smaller value.
#define BitSign(x)  (((x)==1)?(+1):(-1)) //!< If x == 1, return 1. Otherwise return -1.

#define	NORM(x)		( sqrt( SQ((x)[0]) + SQ((x)[1]) + SQ((x)[2]) ) ) //!< \f$|\boldmath x|\f$
#define	NORM2(x)	( SQ((x)[0]) + SQ((x)[1]) + SQ((x)[2]) )         //!< \f$|\boldmath x|^2\f$
#define	DISTANCE(x,y)	( sqrt(SQ((x)[0]-(y)[0])+SQ((x)[1]-(y)[1])+SQ((x)[2]-(y)[2])) ) //!< Distance btw two vectors \f$\boldmath x\f$ and \f$\boldmath y\f$.
#define	DISTANCE2(x,y)	(SQ((x)[0]-(y)[0])+SQ((x)[1]-(y)[1])+SQ((x)[2]-(y)[2])) //!< Square of the distance btw two vectors \f$\boldmath x\f$ and \f$\boldmath y\f$.  
#define	DOT_PRODUCT(x,y)	(((x)[0]*(y)[0])+((x)[1]*(y)[1])+((x)[2]*(y)[2])) //!< Dot product of \f$\boldmath x\f$ and \f$\boldmath y\f$.
#ifndef iszero //{
#define iszero(x) (fpclassify (x) == FP_ZERO) //!< Check the value zero or not. 
#endif // iszero //}


#define dprint(a)   printf(" ### " #a " = %d:%s:line%d:%s()\n", a,__FILE__,__LINE__,__FUNCTION__)
#define lprint(a)   printf(" ### " #a " = %ld:%s:line%d:%s()\n", a,__FILE__,__LINE__,__FUNCTION__)
#define gprint(a)   printf(" ### " #a " = %g:%s:line%d:%s()\n", a,__FILE__,__LINE__,__FUNCTION__)
#define sprint(a)   printf(" ### " #a " %s:line%d:%s()\n", a, __FILE__,__LINE__,__FUNCTION__)
#define pprint(a)   printf(" ### " #a " = %p:%s:line%d:%s()\n", a,__FILE__,__LINE__,__FUNCTION__)

#define print_format(x) (_Generic( (x), \
    bool:          "*** "#x " = %d %s:%s:%d\n",\
    int:           "*** "#x " = %d %s:%s:%d\n",\
    unsigned int:  "*** "#x " = %u %s:%s:%d\n",\
    long:          "*** "#x " = %ld %s:%s:%d\n",\
    unsigned long: "*** "#x " = %ld %s:%s:%d\n",\
    float:         "*** "#x " = %g %s:%s:%d\n",\
    double:        "*** "#x " = %g %s:%s:%d\n",\
    char*:         "*** "#x " = %s %s:%s:%d\n",\
    default:       "*** "#x " = %x %s:%s:%d\n"))
#define vprint(x) fprintf(stderr,print_format(x), x,__FILE__,__FUNCTION__,__LINE__); 

#define ON      (1)  //!< On.
#define OFF     (0)  //!< Off.
#define NONE    (-1) //!< None.

#define PI (3.1415926535897932384626433832795)   //!< \f$\pi\f$
#define IPI (0.31830988618379067153776752674503) //!< \f$1/\pi\f$

/*!
 * A macro that uses when a file is opened. 
 */
#define FileOpen(fp, fname, mode)				\
	(fp) = fopen((fname),(mode));				\
	if ((fp) == NULL){					\
		fprintf(stderr,"Can't open file %s\n",(fname));	\
		exit(1);					\
	}

/*!
 * A macro that uses when a file is opened. 
 */
#define Snprintf(buf,expression, ...)                       \
    if( snprintf((buf),MaxCharactersInLine,expression, __VA_ARGS__ ) >= (MaxCharactersInLine) ){ \
        fprintf(stderr,"buffer overflow: %s:line%d:%s\n",       \
                __FILE__,__LINE__,__FUNCTION__);              \
        exit(1);                                            \
    }

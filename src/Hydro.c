#include "config.h"
#include "NeighborSearch.h"
#include "PlantHydroTree.h"
#include "KernelFunctions.h"
#include "ExtraForce.h"

/*! \file Hydro.c
 * \brief Evaluate the kernel size, density, energy density, div v, rot v, the
 * pressure gradient, the time derivative of the internal energy.
 *
 */

static void ClearHydroData(void);
static void CalcKernelSize(void);
static void CalcRho(void);
static void CalcHydroAcc(void);

extern double (*SPHKernel)(const double, const double);
extern double (*dSPHKernel)(const double, const double);


#define KernelFactInc   (1.26) // 2.0 ^ (0.3)
#define KernelFactDec   (0.79) // 0.5 ^ (0.3)

/*!
 * This function updates fluid infomation of all particles, such as kernel sizes,
 * densities, time derivatives of the specific internal enegy, and the pressure
 * gradients.
 */
void UpdateHydro(void){

    PlantHydroTree();
    ClearHydroData();
    CalcKernelSize();
    UpdateHydroTree();
    CalcRho();
    CalcHydroAcc();

    return ;
}

/*! 
 * Clears variables by zero.
 */
static void ClearHydroData(void){

    for(int i=0;i<ThisRun.NParticles;i++){
        if(ThisRun.KernelEvaluationType == 0){
            SPH[i].Rho = 0.e0;
            if(ThisRun.UseDISPH == 1)
                SPH[i].q = 0.e0;
        }
        SPH[i].Vsig = 
        SPH[i].DivV = 
        SPH[i].Gradh = 
        SPH[i].NumberDensity = 
        SPH[i].GradN = 
        SPH[i].GradV[0] = 
        SPH[i].GradV[1] = 
        SPH[i].GradV[2] = 
        SPH[i].RotV[0] = 
        SPH[i].RotV[1] = 
        SPH[i].RotV[2] = 
        SPH[i].Acc[0] = 
        SPH[i].Acc[1] = 
        SPH[i].Acc[2] = 
        SPH[i].du = 0.e0;

        SPH[i].B[0][0] = SPH[i].B[0][1] = SPH[i].B[0][2] = 
        SPH[i].B[1][0] = SPH[i].B[1][1] = SPH[i].B[1][2] = 
        SPH[i].B[2][0] = SPH[i].B[2][1] = SPH[i].B[2][2] = 0.e0;

        SPH[i].F = 1.0;

        if(ThisRun.SolveSelfGravity){
            SPH[i].Pot = 0.0;
        }
    }

    return ;
}

/*!
 * Check number of neighbor particles. If the number is between ThisRun.NBmin
 * and This Run.NBmax, this function retuns "true". Otherwise this function
 * returns "false".
 */
static bool CheckNBList(const int index, const int Nlist, double *Left, double *Right){

    if((ThisRun.NBmin<=Nlist)&&(Nlist<=ThisRun.NBmax)){
        return true;
    } else if((Nlist<=(ThisRun.NBmax))&&(*Right>0.e0)&&(*Left>0.e0)){
        if((*Right)-(*Left) < 1.e-6*(*Left)){
            return true;
        }
    }

    if(Nlist < ThisRun.NBmin){
        *Left = fmax(*Left,SPH[index].Kernel);
    } else if(Nlist>ThisRun.NBmax){
        if(*Right >0.e0){
            *Right = fmin(*Right,SPH[index].Kernel);
        } else {
            *Right = SPH[index].Kernel;
        }
    }
    if((*Left>0.e0)&&(*Right>0.e0)){
        SPH[index].Kernel = cbrt(0.5*(CUBE(*Left)+CUBE(*Right)));
    } else {
        if(iszero(*Right)&&(*Left>0.e0)){
            SPH[index].Kernel *= KernelFactInc;
        } else if((*Right>0.e0)&&iszero(*Left)){
            SPH[index].Kernel *= KernelFactDec;
        }
    }

    return false;
}

/*!
 * Find the kenrel size of particle "index" iteratively.
 */
static void CalcKernelSize_i(const int index){

    double Left = 0.e0;
    double Right = 0.e0;
    do{
        SPH[index].Nlist = GetNeighborNumber(SPH[index].Pos,2.0*SPH[index].Kernel);
    } while(CheckNBList(index,SPH[index].Nlist,&Left,&Right) == false);

    return ;
}

/*!
 * In this function, kernel sizes of particles are determined. Now, ASURA
 * supports two ways to determine the kernel size. One is the size which
 * includes \f$N_{\rm s} \pm N_{\rm spm}\f$. The other is that the kernel size
 * is evaluated as follows: \f$ h = \eta (m/\rho)^{1/D} \f$, where \f$h\f$,
 * \f$m\f$, \f$\rho\f$, and \f$D\f$ are the kernel size, particle mass, density,
 * and the dimension, respectively.  \f$\eta\f$ is a factor and is given by
 * ThisRun.KernelEta.
 */
static void CalcKernelSize(void){

    if(ThisRun.ShowLog){
        fprintf(stderr,"Kernel evaluation start... ");
        fflush(stderr);
    }

#pragma omp parallel for schedule(dynamic, 1)
    for(int i=0;i<ThisRun.NParticles;i++){
        if(ThisRun.KernelEvaluationType == 0){
            CalcKernelSize_i(i);
        } else {
            SPH[i].Kernel = ThisRun.KernelEta
                *pow((SPH[i].Mass/SPH[i].Rho),1.0/(double)ThisRun.Dimension);
            SPH[i].Rho = 0;
            if(ThisRun.UseDISPH == 1){
                SPH[i].q = 0.e0;
            }
        }
    }

    if(ThisRun.ShowLog){
        fprintf(stderr,"finish.\n");
        fflush(stderr);
    }
    return ;
}

static void CalcMoment(double B[3][3], const double dr[3], const double w, const double Volume){

    double Factor = w*Volume;

    B[0][0] += Factor*dr[0]*dr[0];
    B[0][1] += Factor*dr[0]*dr[1];
    B[0][2] += Factor*dr[0]*dr[2];

    B[1][0] += Factor*dr[1]*dr[0];
    B[1][1] += Factor*dr[1]*dr[1];
    B[1][2] += Factor*dr[1]*dr[2];

    B[2][0] += Factor*dr[2]*dr[0];
    B[2][1] += Factor*dr[2]*dr[1];
    B[2][2] += Factor*dr[2]*dr[2];

    return ;
}

static void CalcMomentInv(double B[3][3]){

    if(ThisRun.Dimension == 2){
        B[2][2] = 1.0;
        B[2][0] = B[2][1] = B[0][2] = B[1][2] = 0.e0;
    } else if(ThisRun.Dimension == 1){
        B[1][1] = B[2][2] = 1.0;
    }

    double det = B[0][0]*B[1][1]*B[2][2] + B[1][0]*B[2][1]*B[0][2] + B[2][0]*B[0][1]*B[1][2] 
                -B[0][0]*B[2][1]*B[1][2] - B[2][0]*B[1][1]*B[0][2] - B[1][0]*B[0][1]*B[2][2];
    det += 1.e-16;

    double C[3][3];
    for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
            C[i][j] = B[i][j];
        }
    }

    B[0][0] = (C[1][1]*C[2][2]-C[1][2]*C[2][1])/det;
    B[0][1] = (C[0][2]*C[2][1]-C[0][1]*C[2][2])/det;
    B[0][2] = (C[0][1]*C[1][2]-C[0][2]*C[1][1])/det;
    B[1][0] = (C[1][2]*C[2][0]-C[1][0]*C[2][2])/det;
    B[1][1] = (C[0][0]*C[2][2]-C[0][2]*C[2][0])/det;
    B[1][2] = (C[0][2]*C[1][0]-C[0][0]*C[1][2])/det;
    B[2][0] = (C[1][0]*C[2][1]-C[1][1]*C[2][0])/det;
    B[2][1] = (C[0][1]*C[2][0]-C[0][0]*C[2][1])/det; 
    B[2][2] = (C[0][0]*C[1][1]-C[0][1]*C[1][0])/det; 

    return ;
}

static void CalcMLS(const int index, const int Nlist, const int Neighbors[]){

    double div_v = 0.e0;
    double rot_v[3] = {0.e0,0.e0,0.e0};
    double grad_v[3] = {0.e0,0.e0,0.e0};
    for(int j=0;j<Nlist;j++){
        int nbindex = Neighbors[j];
        double xij[3], vij[3];
#ifdef PERIODIC
        xij[0] = PeriodicDistance(SPH[index].Pos[0],SPH[nbindex].Pos[0],0);
        xij[1] = PeriodicDistance(SPH[index].Pos[1],SPH[nbindex].Pos[1],1);
        xij[2] = PeriodicDistance(SPH[index].Pos[2],SPH[nbindex].Pos[2],2);
#else
        xij[0] = SPH[index].Pos[0] - SPH[nbindex].Pos[0];
        xij[1] = SPH[index].Pos[1] - SPH[nbindex].Pos[1];
        xij[2] = SPH[index].Pos[2] - SPH[nbindex].Pos[2];
#endif

        vij[0] = SPH[index].Vel[0] - SPH[nbindex].Vel[0];
        vij[1] = SPH[index].Vel[1] - SPH[nbindex].Vel[1];
        vij[2] = SPH[index].Vel[2] - SPH[nbindex].Vel[2];

        double r2 = NORM2(xij);
        double r = sqrt(r2);
        double w = SPHKernel(r,SPH[index].Kernel);

        double Volumej = 0.e0; 
        if(ThisRun.UseDISPH == 0){ // SSPH
            Volumej = SPH[nbindex].Mass/SPH[nbindex].Rho;
        } else if(ThisRun.UseDISPH == 1){ // DISPH
            Volumej = SPH[nbindex].Mass*SPH[nbindex].u/SPH[nbindex].q;
        }

        double psi_ji[3] = {
            SPH[index].B[0][0]*xij[0]+SPH[index].B[0][1]*xij[1]+SPH[index].B[0][2]*xij[2],
            SPH[index].B[1][0]*xij[0]+SPH[index].B[1][1]*xij[1]+SPH[index].B[1][2]*xij[2],
            SPH[index].B[2][0]*xij[0]+SPH[index].B[2][1]*xij[1]+SPH[index].B[2][2]*xij[2]};

        psi_ji[0] *= w*Volumej;
        psi_ji[1] *= w*Volumej;
        psi_ji[2] *= w*Volumej;

        div_v += DOT_PRODUCT(vij, psi_ji);

        rot_v[0] += vij[1]*psi_ji[2]-vij[2]*psi_ji[1];
        rot_v[1] += vij[2]*psi_ji[0]-vij[0]*psi_ji[2];
        rot_v[2] += vij[0]*psi_ji[1]-vij[1]*psi_ji[0];

        grad_v[0] += vij[0]*psi_ji[0];
        grad_v[1] += vij[1]*psi_ji[1];
        grad_v[2] += vij[2]*psi_ji[2];
    }

    SPH[index].DivV = div_v;
    SPH[index].RotV[0] = rot_v[0];
    SPH[index].RotV[1] = rot_v[1];
    SPH[index].RotV[2] = rot_v[2];
    SPH[index].GradV[0] = grad_v[0];
    SPH[index].GradV[1] = grad_v[1];
    SPH[index].GradV[2] = grad_v[2];

    return ;
}

/*!
 * This function evaluates the density, energy density, divergence and
 * rotation of velocities of the particle "index".
 */
static void CalcRho_i(const int index){

    int Neighbors[MaxNeighborSize];
    int Nlist;
    Nlist = GetNeighbors(SPH[index].Pos,2.e0*SPH[index].Kernel,Neighbors);

    if(ThisRun.KernelEvaluationType == 1){
        SPH[index].Nlist = Nlist;
    }
    double Vsig = 0.e0;
    double ci = sqrt(ThisRun.GGm1*SPH[index].u);
    for(int j=0;j<Nlist;j++){
        int nbindex = Neighbors[j];
        double xij[3],vij[3];

        xij[0] = PeriodicDistance(SPH[index].Pos[0],SPH[nbindex].Pos[0],0);
        xij[1] = PeriodicDistance(SPH[index].Pos[1],SPH[nbindex].Pos[1],1);
        xij[2] = PeriodicDistance(SPH[index].Pos[2],SPH[nbindex].Pos[2],2);
        vij[0] = SPH[index].Vel[0] - SPH[nbindex].Vel[0];
        vij[1] = SPH[index].Vel[1] - SPH[nbindex].Vel[1];
        vij[2] = SPH[index].Vel[2] - SPH[nbindex].Vel[2];

        double vx = DOT_PRODUCT(xij,vij);
        double r2 = NORM2(xij);
        double r = sqrt(r2);
        double w;
        w = SPHKernel(r,SPH[index].Kernel);

        SPH[index].Rho += SPH[nbindex].Mass*w;
        SPH[index].NumberDensity += w;
        double dw;
        dw = dSPHKernel(r,SPH[index].Kernel);

        SPH[index].GradN += r2*dw;
        double wij = vx/r;
        if(vx<0.e0){
            Vsig = fmax(Vsig,ci+sqrt(ThisRun.GGm1*SPH[nbindex].u)-ThisRun.ViscSignalVelocityBeta*wij);
        } else {
            Vsig = fmax(Vsig,ci+sqrt(ThisRun.GGm1*SPH[nbindex].u));
        }

        double Weightj;
        if(ThisRun.UseDISPH == 0){ // SSPH
            Weightj = SPH[nbindex].Mass;
        } else { // DISPH
            Weightj = SPH[nbindex].Mass*SPH[nbindex].u;
            SPH[index].q += Weightj*w;
        } 
        double Weightjdwj = Weightj*dw;

        SPH[index].DivV -= Weightjdwj*vx;
        SPH[index].RotV[0] -= Weightjdwj*(vij[2]*xij[1]-vij[1]*xij[2]);
        SPH[index].RotV[1] -= Weightjdwj*(vij[0]*xij[2]-vij[2]*xij[0]);
        SPH[index].RotV[2] -= Weightjdwj*(vij[1]*xij[0]-vij[0]*xij[1]);

        if(ThisRun.UseGradh > 0)
            SPH[index].Gradh += Weightjdwj*r2;
    }

    double InvValue;
    if(ThisRun.UseDISPH == 0){ // SSPH
        InvValue = 1.0/SPH[index].Rho;
    } else { // DISPH
        InvValue = 1.0/SPH[index].q;
    } 
    SPH[index].DivV *= InvValue;
    SPH[index].RotV[0] *= InvValue;
    SPH[index].RotV[1] *= InvValue;
    SPH[index].RotV[2] *= InvValue;


#define Epsilon_bal (1.e-5)

    double Ffact = Epsilon_bal*sqrt(ThisRun.GGm1*SPH[index].u)/SPH[index].Kernel;

    if(ThisRun.Dimension == 1){
        SPH[index].F = 1.0;
    } else if(ThisRun.Dimension == 2){
        SPH[index].F = fabs(SPH[index].DivV/(SPH[index].DivV+fabs(SPH[index].RotV[2])+Ffact));
    } else if(ThisRun.Dimension == 3){
        SPH[index].F = fabs(SPH[index].DivV/(SPH[index].DivV+NORM(SPH[index].RotV)+Ffact));
    }

    if(SPH[index].F>1.0)
        SPH[index].F = 1.e0;

    if(ThisRun.UseGradh > 0){
        if(ThisRun.UseDISPH == 0){ // SSPH
            SPH[index].Gradh = -ThisRun.Dimension*SPH[index].Rho/SPH[index].Gradh;
        }else{ // DISPH
            SPH[index].Gradh = -ThisRun.Dimension*SPH[index].q/SPH[index].Gradh;
        } 
        SPH[index].GradN = -ThisRun.Dimension*SPH[index].NumberDensity/SPH[index].GradN;
    }

    SPH[index].Vsig = Vsig;


    if(ThisRun.UseVariableAlpha == 1){
        //double InvTau = (Vsig)/(SPH[index].Kernel);
        double InvTau = Vsig/SPH[index].Kernel;
        double Source = 5*fmax(-SPH[index].DivV,0.e0);
        double dAdt = (ThisRun.ViscAlphaMax-SPH[index].Alpha)*Source
                     -(SPH[index].Alpha-ThisRun.ViscAlphaMin)*InvTau;
                          
        SPH[index].Alpha += dAdt*ThisRun.dt;
        SPH[index].Alpha = fmin(SPH[index].Alpha,ThisRun.ViscAlphaMax);
        SPH[index].Alpha = fmax(SPH[index].Alpha,ThisRun.ViscAlphaMin);
    }
    return ;
}


static void CalcMLSDivRot_i(const int index){

    int Neighbors[MaxNeighborSize];
    int Nlist;
    Nlist = GetNeighbors(SPH[index].Pos,2.e0*SPH[index].Kernel,Neighbors);

    if(ThisRun.KernelEvaluationType == 1){
        SPH[index].Nlist = Nlist;
    }
    for(int j=0;j<Nlist;j++){
        int nbindex = Neighbors[j];
        double xij[3];

        xij[0] = PeriodicDistance(SPH[index].Pos[0],SPH[nbindex].Pos[0],0);
        xij[1] = PeriodicDistance(SPH[index].Pos[1],SPH[nbindex].Pos[1],1);
        xij[2] = PeriodicDistance(SPH[index].Pos[2],SPH[nbindex].Pos[2],2);

        double r2 = NORM2(xij);
        double r = sqrt(r2);
        double w;
        w = SPHKernel(r,SPH[index].Kernel);

        double Volumej = ThisRun.UseDISPH==1?
            SPH[nbindex].Mass*SPH[nbindex].u/SPH[nbindex].q:
            SPH[nbindex].Mass/SPH[nbindex].Rho;

        CalcMoment(SPH[index].B,xij,w,Volumej);
    }
    CalcMomentInv(SPH[index].B);

    CalcMLS(index,Nlist,Neighbors);
    
    double Ffact = Epsilon_bal*sqrt(ThisRun.GGm1*SPH[index].u)/SPH[index].Kernel;

    if(ThisRun.Dimension == 1){
        SPH[index].F = 1.0;
    } else if(ThisRun.Dimension == 2){
        SPH[index].F = fabs(SPH[index].DivV/(SPH[index].DivV+fabs(SPH[index].RotV[2])+Ffact));
    } else if(ThisRun.Dimension == 3){
        SPH[index].F = fabs(SPH[index].DivV/(SPH[index].DivV+NORM(SPH[index].RotV)+Ffact));
    }

    if(SPH[index].F>1.0)
        SPH[index].F = 1.e0;

    return ;
}

/*!
 * This function evaluates the densities, energy densities, divergence and
 * rotation of velocities.
 */
static void CalcRho(void){

    if(ThisRun.ShowLog){
        fprintf(stderr,"SPH rho calculation start... ");
        fflush(stderr);
    }

#pragma omp parallel for schedule(dynamic, 1)
    for(int i=0;i<ThisRun.NParticles;i++){
        CalcRho_i(i);
    }

    if(ThisRun.DerivativeOperatorType==1){
#pragma omp parallel for schedule(dynamic, 1)
        for(int i=0;i<ThisRun.NParticles;i++){
            CalcMLSDivRot_i(i);
        }
    }

    if(ThisRun.ShowLog){
        fprintf(stderr,"finish.\n");
        fflush(stderr);
    }

    return ;
}


/*!
 * This function evaluates the amount of viscosity.  See Monaghan (1997).
 */
static double ReturnVisc(const int index, const int nbindex, const double vx, const double r){

    double csij = sqrt(ThisRun.GGm1*SPH[index].u) + sqrt(ThisRun.GGm1*SPH[nbindex].u);
    if(vx<0.e0){
        double wij = vx/r;
        double vsig = csij-ThisRun.ViscSignalVelocityBeta*wij;
        SPH[index].Vsig = fmax(vsig,SPH[index].Vsig);
        double Alpha = ThisRun.ViscAlpha;
        if(ThisRun.UseVariableAlpha == 1){
            Alpha = 0.5*(SPH[index].Alpha+SPH[nbindex].Alpha);
        }

        double visc = -0.5*Alpha*vsig*wij/(0.5*(SPH[index].Rho+SPH[nbindex].Rho));
        if(ThisRun.ViscBalsara == 1){
            if(ThisRun.Dimension > 1)
                visc *= 0.5*(SPH[index].F+SPH[nbindex].F);
        } else  if(ThisRun.ViscBalsara == 2){
            if(ThisRun.Dimension > 1)
                visc *= 2.0*SPH[index].F*SPH[nbindex].F/(SPH[index].F+SPH[nbindex].F);
        }
        return visc;
    } else {
        SPH[index].Vsig = fmax(csij,SPH[index].Vsig);
        return 0.e0;
    }
}

/*!
 * This function evaluates the amount of the von Neumann-Richtmyer-Landshoff
 * type viscosity.
 */
static double ReturnvNRVisc_i(const int index){

    double csi = sqrt(ThisRun.GGm1*SPH[index].u);
    double Alpha = ThisRun.ViscAlpha;
    if(ThisRun.UseVariableAlpha == 1){
        Alpha = SPH[index].Alpha;
    }
    double qAVi = (SPH[index].DivV < 0.0) ? (-Alpha*SPH[index].Rho*csi*SPH[index].Kernel*SPH[index].DivV
            + 2.0*Alpha*SPH[index].Rho*SQ(SPH[index].Kernel*SPH[index].DivV)) : 0.0;

    if((ThisRun.ViscBalsara == 1)&&(ThisRun.Dimension>1)){
        qAVi *= SPH[index].F;
    }
    return qAVi;
}


/*!
 * Evaluate the pressure force and time derivative of the specific internal
 * energy of the particle "index".
 */
static void CalcHydroAcc_i(const int index){

    int Neighbors[MaxNeighborSize];
    int Nlist = GetNeighborsPairs(SPH[index].Pos,2.e0*SPH[index].Kernel,Neighbors);
    for(int j=0;j<Nlist;j++){
        int nbindex = Neighbors[j];
        double xij[3],vij[3];
        xij[0] = PeriodicDistance(SPH[index].Pos[0],SPH[nbindex].Pos[0],0);
        xij[1] = PeriodicDistance(SPH[index].Pos[1],SPH[nbindex].Pos[1],1);
        xij[2] = PeriodicDistance(SPH[index].Pos[2],SPH[nbindex].Pos[2],2);
        vij[0] = SPH[index].Vel[0] - SPH[nbindex].Vel[0];
        vij[1] = SPH[index].Vel[1] - SPH[nbindex].Vel[1];
        vij[2] = SPH[index].Vel[2] - SPH[nbindex].Vel[2];

        double vx = xij[0]*vij[0]+xij[1]*vij[1]+xij[2]*vij[2];
        double r = NORM(xij);
        double dwi = dSPHKernel(r,SPH[index].Kernel);
        double dwj = dSPHKernel(r,SPH[nbindex].Kernel);

        double dw = 0.5*(dwi+dwj);

        double Weighti,Weightj;
        double SmoothedValuei,SmoothedValuej;
        double p_over_fund2_i,p_over_fund2_j;
        if(ThisRun.UseDISPH == 1){
            Weighti = SPH[index].Mass*SPH[index].u;
            Weightj = SPH[nbindex].Mass*SPH[nbindex].u;
            SmoothedValuei = SPH[index].q;
            SmoothedValuej = SPH[nbindex].q;
            p_over_fund2_i = ThisRun.Gm1/SPH[index].q;
            p_over_fund2_j = ThisRun.Gm1/SPH[nbindex].q;
        } else {
            Weighti = SPH[index].Mass;
            Weightj = SPH[nbindex].Mass;
            SmoothedValuei = SPH[index].Rho;
            SmoothedValuej = SPH[nbindex].Rho;
            p_over_fund2_i = ThisRun.Gm1*SPH[index].u/SPH[index].Rho;
            p_over_fund2_j = ThisRun.Gm1*SPH[nbindex].u/SPH[nbindex].Rho;
        }

        double fij,fji;
        if(ThisRun.UseGradN == 1){
            fij = 1.0-(1.0/(Weightj*SPH[index].NumberDensity))*SmoothedValuei*(1.0/SPH[index].Gradh-1.0)*SPH[index].GradN;
            fji = 1.0-(1.0/(Weighti*SPH[nbindex].NumberDensity))*SmoothedValuej*(1.0/SPH[nbindex].Gradh-1.0)*SPH[nbindex].GradN;
        } else if(ThisRun.UseGradh == 1){
            fij = SPH[index].Gradh;
            fji = SPH[nbindex].Gradh;
        } else {
            fij = fji = 1.0;
            dwi = dwj = dw;
        }


        double tmpj = 0.0; double tmpj_energy = 0.e0;
        if(ThisRun.ViscType == 0){
            double visc = 0.e0;
            visc = dw*ReturnVisc(index,nbindex,vx,r);
            double tmpj_visc = SPH[index].Mass*SPH[nbindex].Mass*visc;
            tmpj = Weighti*Weightj*(fij*p_over_fund2_i*dwi+fji*p_over_fund2_j*dwj)+tmpj_visc;
            tmpj_energy = Weighti*Weightj*fij*p_over_fund2_i*vx*dwi+0.5*tmpj_visc*vx;
        } else if(ThisRun.ViscType == 1){
            p_over_fund2_i += ReturnvNRVisc_i(index)/SQ(SmoothedValuei);
            p_over_fund2_j += ReturnvNRVisc_i(nbindex)/SQ(SmoothedValuej);
            tmpj = Weighti*Weightj*(fij*p_over_fund2_i*dwi+fji*p_over_fund2_j*dwj);
            tmpj_energy = Weighti*Weightj*fij*p_over_fund2_i*vx*dwi;
        }

        SPH[index].du += tmpj_energy;
        SPH[index].Acc[0] -= tmpj*xij[0];
        SPH[index].Acc[1] -= tmpj*xij[1];
        SPH[index].Acc[2] -= tmpj*xij[2];
    }

    double InvMass = 1.0/SPH[index].Mass;
    SPH[index].du *= InvMass;
    SPH[index].Acc[0] *= InvMass;
    SPH[index].Acc[1] *= InvMass;
    SPH[index].Acc[2] *= InvMass;

    return ;
}

/*!
 * This function evaluates the pressure force and the time derivative of the
 * specific internal energy, \f$du/dt\f$.
 */
static void CalcHydroAcc(void){

    if(ThisRun.ShowLog){
        fprintf(stderr,"SPH pressure grad calculation start... ");
        fflush(stderr);
    }

#pragma omp parallel for schedule(dynamic, 1)
    for(int i=0;i<ThisRun.NParticles;i++){
        CalcHydroAcc_i(i);
    }

    // Extra force, boundary condition, etc.
    AddExtraForce();

    if(ThisRun.ShowLog){
        fprintf(stderr,"finish.\n");
        fflush(stderr);
    }
    return ;
}


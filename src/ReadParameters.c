#include "config.h"
#include "toml.h"

/*! \file ReadParameters.c
 * \brief Functions for reading a parameter file. 
 */

/*!
 * This function updates parameteres based on dependencies. 
 */
static void UpdateParameters(void){
    
    if(ThisRun.UseGradN == 1){
        ThisRun.UseGradh = 1;
    }

    return ;
}

static void read_tomlfile_Mode(const toml_table_t* config){

    toml_table_t* tab = toml_table_in(config, "Mode");
    if (!tab) {
        fprintf(stderr,"missing [Mode]\n");
        return;
    }
    fprintf(stderr,"[Mode]\n");

    toml_datum_t d = toml_int_in(tab, "RunType");
    if(d.ok){
        fprintf(stderr,"\t RunType = %ld\n",d.u.i);
        ThisRun.RunType = d.u.i;
    }

    d = toml_int_in(tab, "NParticles");
    if(d.ok){
        fprintf(stderr,"\t NParticles = %ld\n",d.u.i);
        ThisRun.NParticles = d.u.i;
    }

    d = toml_int_in(tab, "ShowLog");
    if(d.ok){
        fprintf(stderr,"\t ShowLog = %ld\n",d.u.i);
        ThisRun.ShowLog = d.u.i;
    }
    return ;
}

static void read_tomlfile_IO(const toml_table_t* config){

    toml_table_t* tab = toml_table_in(config, "IO");
    if (!tab) {
        fprintf(stderr,"missing [IO]\n");
        return;
    }
    fprintf(stderr,"[IO]\n");

    toml_datum_t d = toml_string_in(tab, "OutDir");
    if(d.ok){
        fprintf(stderr,"\t OutDir = %s\n",d.u.s);
        strncpy(ThisRun.OutDir,d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_string_in(tab, "ICFile");
    if(d.ok){
        fprintf(stderr,"\t ICFile = %s\n",d.u.s);
        strncpy(ThisRun.ICFile,d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_string_in(tab, "RunName");
    if(d.ok){
        fprintf(stderr,"\t RunName = %s\n",d.u.s);
        strncpy(ThisRun.RunName,d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_int_in(tab, "OutputFileNumber");
    if(d.ok){
        fprintf(stderr,"\t OutputFileNumber = %ld\n",d.u.i);
        ThisRun.OutputFileNumber = d.u.i;
    }

    d = toml_int_in(tab, "WriteEveryStep");
    if(d.ok){
        fprintf(stderr,"\t WriteEveryStep = %ld\n",d.u.i);
        ThisRun.WriteEveryStep = d.u.i;
    }
    return ;
}


static void read_tomlfile_Boundary(const toml_table_t* config){

    toml_table_t* tab = toml_table_in(config, "Boundary");
    if (!tab) {
        fprintf(stderr,"missing [Boundary]\n");
        return;
    }
    fprintf(stderr,"[Boundary]\n");

    toml_datum_t d = toml_int_in(tab, "Dimension");
    if(d.ok){
        fprintf(stderr,"\t Dimension = %ld\n",d.u.i);
        ThisRun.Dimension = d.u.i;
    }

    d = toml_int_in(tab, "PeriodicBoundary");
    if(d.ok){
        fprintf(stderr,"\t PeriodicBoundary = %ld\n",d.u.i);
        ThisRun.PeriodicBoundary = d.u.i;
    }

    d = toml_double_in(tab, "LBox0");
    if(d.ok){
        fprintf(stderr,"\t LBox0 = %g\n",d.u.d);
        ThisRun.LBox[0] = d.u.d;
    }

    d = toml_double_in(tab, "LBox1");
    if(d.ok){
        fprintf(stderr,"\t LBox1 = %g\n",d.u.d);
        ThisRun.LBox[1] = d.u.d;
    }

    d = toml_double_in(tab, "LBox2");
    if(d.ok){
        fprintf(stderr,"\t LBox2 = %g\n",d.u.d);
        ThisRun.LBox[2] = d.u.d;
    }

    return ;
}

static void read_tomlfile_Time(const toml_table_t* config){

    toml_table_t* tab = toml_table_in(config, "Time");
    if (!tab) {
        fprintf(stderr,"missing [Time]\n");
        return;
    }
    fprintf(stderr,"[Time]\n");

    toml_datum_t d = toml_double_in(tab, "TEnd");
    if(d.ok){
        fprintf(stderr,"\t TEnd = %g\n",d.u.d);
        ThisRun.TEnd = d.u.d;
    }

    d = toml_double_in(tab, "CFL");
    if(d.ok){
        fprintf(stderr,"\t CFL = %g\n",d.u.d);
        ThisRun.CFL = d.u.d;
    }

    return ;
}

static void read_tomlfile_Hydro(const toml_table_t* config){

    toml_table_t* tab = toml_table_in(config, "Hydro");
    if (!tab) {
        fprintf(stderr,"missing [Hydro]\n");
        return;
    }
    fprintf(stderr,"[Hydro]\n");

    toml_datum_t d = toml_int_in(tab, "UseDISPH");
    if(d.ok){
        fprintf(stderr,"\t UseDISPH = %ld\n",d.u.i);
        ThisRun.UseDISPH = d.u.i;
    }

    d = toml_int_in(tab, "SelectKernelType");
    if(d.ok){
        fprintf(stderr,"\t SelectKernelType = %ld\n",d.u.i);
        ThisRun.SelectKernelType = d.u.i;
    }

    d = toml_int_in(tab, "KernelEvaluationType");
    if(d.ok){
        fprintf(stderr,"\t KernelEvaluationType = %ld\n",d.u.i);
        ThisRun.KernelEvaluationType = d.u.i;
    }

    d = toml_double_in(tab, "KernelEta");
    if(d.ok){
        fprintf(stderr,"\t KernelEta = %g\n",d.u.d);
        ThisRun.KernelEta = d.u.d;
    }

    d = toml_int_in(tab, "UseGradh");
    if(d.ok){
        fprintf(stderr,"\t UseGradh = %ld\n",d.u.i);
        ThisRun.UseGradh = d.u.i;
    }

    d = toml_int_in(tab, "UseGradN");
    if(d.ok){
        fprintf(stderr,"\t UseGradN = %ld\n",d.u.i);
        ThisRun.UseGradN = d.u.i;
    }

    d = toml_int_in(tab, "DerivativeOperatorType");
    if(d.ok){
        fprintf(stderr,"\t DerivativeOperatorType = %ld\n",d.u.i);
        ThisRun.DerivativeOperatorType = d.u.i;
    }

    d = toml_double_in(tab, "Gamma");
    if(d.ok){
        fprintf(stderr,"\t Gamma = %g\n",d.u.d);
        ThisRun.Gamma = d.u.d;
    }

    d = toml_int_in(tab, "Ns");
    if(d.ok){
        fprintf(stderr,"\t Ns = %ld\n",d.u.i);
        ThisRun.Ns = d.u.i;
    }

    d = toml_int_in(tab, "Nspm");
    if(d.ok){
        fprintf(stderr,"\t Nspm = %ld\n",d.u.i);
        ThisRun.Nspm = d.u.i;
    }

    d = toml_int_in(tab, "ViscType");
    if(d.ok){
        fprintf(stderr,"\t ViscType = %ld\n",d.u.i);
        ThisRun.ViscType = d.u.i;
    }

    d = toml_double_in(tab, "ViscAlpha");
    if(d.ok){
        fprintf(stderr,"\t ViscAlpha = %g\n",d.u.d);
        ThisRun.ViscAlpha = d.u.d;
    }

    d = toml_int_in(tab, "UseVariableAlpha");
    if(d.ok){
        fprintf(stderr,"\t UseVariableAlpha = %ld\n",d.u.i);
        ThisRun.UseVariableAlpha = d.u.i;
    }

    d = toml_double_in(tab, "ViscAlphaMin");
    if(d.ok){
        fprintf(stderr,"\t ViscAlphaMin = %g\n",d.u.d);
        ThisRun.ViscAlphaMin = d.u.d;
    }

    d = toml_double_in(tab, "ViscAlphaMax");
    if(d.ok){
        fprintf(stderr,"\t ViscAlphaMax = %g\n",d.u.d);
        ThisRun.ViscAlphaMax = d.u.d;
    }

    d = toml_int_in(tab, "ViscBalsara");
    if(d.ok){
        fprintf(stderr,"\t ViscBalsara = %ld\n",d.u.i);
        ThisRun.ViscBalsara = d.u.i;
    }

    d = toml_double_in(tab, "ViscSignalVelocityBeta");
    if(d.ok){
        fprintf(stderr,"\t ViscSignalVelocityBeta = %g\n",d.u.d);
        ThisRun.ViscSignalVelocityBeta = d.u.d;
    }

    return ;
}

static void read_tomlfile_Gravity(const toml_table_t* config){

    toml_table_t* tab = toml_table_in(config, "Gravity");
    if (!tab) {
        fprintf(stderr,"missing [Gravity]\n");
        return;
    }
    fprintf(stderr,"[Gravity]\n");

    toml_datum_t d = toml_int_in(tab, "SolveSelfGravity");
    if(d.ok){
        fprintf(stderr,"\t SolveSelfGravity = %ld\n",d.u.i);
        ThisRun.SolveSelfGravity = d.u.i;
    }

    d = toml_int_in(tab, "UseSymmetrizedPlummer");
    if(d.ok){
        fprintf(stderr,"\t UseSymmetrizedPlummer = %ld\n",d.u.i);
        ThisRun.UseSymmetrizedPlummer = d.u.i;
    }

    d = toml_double_in(tab, "OpeningAngle");
    if(d.ok){
        fprintf(stderr,"\t OpeningAngle = %g\n",d.u.d);
        ThisRun.OpeningAngle = d.u.d;
    }

    d = toml_int_in(tab, "GroupSize");
    if(d.ok){
        fprintf(stderr,"\t GroupSize = %ld\n",d.u.i);
        ThisRun.GroupSize = d.u.i;
    }

    d = toml_double_in(tab, "GravitationalConstant");
    if(d.ok){
        fprintf(stderr,"\t GravitationalConstant = %g\n",d.u.d);
        ThisRun.GravitationalConstant = d.u.d;
    }
    return ;
}

static void read_tomlfile_Thread(const toml_table_t* config){

    toml_table_t* tab = toml_table_in(config, "Thread");
    if (!tab) {
        fprintf(stderr,"missing [Thread]\n");
        return;
    }
    fprintf(stderr,"[Thread]\n");

    toml_datum_t d = toml_int_in(tab, "ThreadNumber");
    if(d.ok){
        fprintf(stderr,"\t ThreadNumber = %ld\n",d.u.i);
        ThisRun.ThreadNumber = d.u.i;
    }

    d = toml_int_in(tab, "DynamicChunk");
    if(d.ok){
        fprintf(stderr,"\t DynamicChunk = %ld\n",d.u.i);
        ThisRun.DynamicChunk = d.u.i;
    }
    return ;
}


static void read_tomlfile(char fname[]){

    FILE *fp;
    FileOpen(fp,fname,"r");
    char errbuf[MaxCharactersInLine];
    toml_table_t* config = toml_parse_file(fp, errbuf, sizeof(errbuf));
    fclose(fp);

    if(!config){
        fprintf(stderr,"Cannot parse - %s",errbuf);
        exit(1);
    }

    toml_datum_t d = toml_string_in(config,"Title");
    if(d.ok){
        fprintf(stderr,"This is %s.\n",d.u.s);
        free(d.u.s);
    }else{
        fprintf(stderr,"missing Title\n");
    }

    read_tomlfile_Mode(config);
    read_tomlfile_IO(config);
    read_tomlfile_Boundary(config);
    read_tomlfile_Time(config);
    read_tomlfile_Hydro(config);
    read_tomlfile_Gravity(config);
    read_tomlfile_Thread(config);

    toml_free(config);

    return ;
}

/*!
 * This function reads a parameter file of which name is defined in FileName and
 * then sets parameters.
 */
void ReadParameters(char FileName[]){

    read_tomlfile(FileName);
    UpdateParameters();
    MakeDir(ThisRun.OutDir);

    return ;
}

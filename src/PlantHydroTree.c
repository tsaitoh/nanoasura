#include	"config.h"

/* \file PlantHydroTree.c
 * \brief Build a tree structure for neighbor search.
 */

#define TreeNsub (8)            //!< Number of subnodes.
#define  TreeMaxNodeLevel (20)  //!< Maximum level of the tree structure.
static double WidthFactor[TreeMaxNodeLevel]; //!< Node width factor for each tree level.
static double DiagHydro;                     //!< Distance between a tree node center and its child node center.

/*! @struct StructCachedData
 * Data cache for building the tree structure.
 */
static struct StructCachedData{
    double Pos[3]; //!< Position
    double Kernel; //!< Kernel size
} *CachedData;

static int *sublist; //<! Temporary array.

static void MakeHydroRoot(void);
static void BuildHydroTree(void);
static int NextHydroNode(const int NodeID);
static void HydroNodeDataImplant(const int CurrentNodeID);


static bool FirstFlag = true; //<! A flag which checks whether InitializeRootForHydro() is called or not.

/*!
 * This function initializes the root node of the tree structure which is used
 * in the neighbor search.
 */
static void InitializeRootForHydro(void){

    int NloadHydro = ThisRun.NParticles;
    int NumberofAllocatedNodes = ThisRun.NParticles/8;

    /* HydroRoot */
    HydroRoot.NumberofAllocatedLeaves = NloadHydro;
	HydroRoot.Leaves = malloc(sizeof(int)*NloadHydro+1);
    HydroRoot.NumberofAllocatedNodes = NumberofAllocatedNodes;
	HydroRoot.NumberofNodeCreationLimit = 2;
    HydroRoot.MaxLevel = TreeMaxNodeLevel;

    double dw = 0.5;
    for(int i=0;i<TreeMaxNodeLevel;i++)
        WidthFactor[i] = pow(dw,(double)i);
    /* HydroRoot */

    /* HydroNode */
    HydroNode = malloc(sizeof(struct StructHydroNode)*HydroRoot.NumberofAllocatedNodes);

    struct StructHydroNode HydroNodeTemp;
    memset(&HydroNodeTemp,0,sizeof(struct StructHydroNode));
    HydroNodeTemp.Next = HydroNodeTemp.Parent = HydroNodeTemp.Children = 
    HydroNodeTemp.Sister = NONE;

    for(int i=0;i<HydroRoot.NumberofAllocatedNodes;i++)
        HydroNode[i] = HydroNodeTemp;
    /* HydroNode */

    /* NBCache */
    NBCache = malloc(sizeof(struct StructNBCache)*NloadHydro+1);
    /* NBCache */

    sublist = malloc(sizeof(int)*NloadHydro);
    CachedData = malloc(sizeof(struct StructCachedData)*NloadHydro);

	return;
}

/*!
 * Copy particle positions and kernel sizes into the cache array.
 */
static void HydroTreePreprocessing(void){

    for(int i=0;i<ThisRun.NParticles;i++){
        CachedData[i].Pos[0] = SPH[i].Pos[0];
        CachedData[i].Pos[1] = SPH[i].Pos[1];
        CachedData[i].Pos[2] = SPH[i].Pos[2];
        CachedData[i].Kernel = SPH[i].Kernel;
    }

    return ;
}

/*!
 * This function generates the tree structure.
 */
void PlantHydroTree(void){

    if(ThisRun.ShowLog){
        fprintf(stderr,"Plant tree start... ");
        fflush(stderr);
    }

    if(FirstFlag){
        InitializeRootForHydro();
        FirstFlag = false;
    }
    HydroTreePreprocessing();

	MakeHydroRoot();
    BuildHydroTree();

    for(int i=1;i<HydroRoot.NumberofNodes;i++)
        HydroNode[i].Next = NextHydroNode(i);

    DiagHydro = DISTANCE(HydroNode[0].Pos,HydroNode[HydroNode[0].Children].Pos);

    HydroNodeDataImplant(0);

    if(ThisRun.ShowLog){
        fprintf(stderr,"finish.\n");
        fflush(stderr);
    }

	return;
}

/*!
 * Update DistanceMax and KernelMax.
 */
void UpdateHydroTree(void){

    if(ThisRun.ShowLog){
        fprintf(stderr,"Plant tree start... ");
        fflush(stderr);
    }

    for(int i=0;i<ThisRun.NParticles;i++){
        int index = NBCache[i].Leaf;
        NBCache[i].Kernel = SPH[index].Kernel;
    }

    DiagHydro = DISTANCE(HydroNode[0].Pos,HydroNode[HydroNode[0].Children].Pos);
    HydroNodeDataImplant(0);

    if(ThisRun.ShowLog){
        fprintf(stderr,"finish.\n");
        fflush(stderr);
    }
	return;
}

/*!
 * Make the root node.
 */
static void MakeHydroRoot(void){

	double min[3],max[3];
    int RootNodeID = 0;

    for(int k=0;k<3;k++){
        max[k] = CachedData[0].Pos[k];
        min[k] = CachedData[0].Pos[k];
    }
    for(int i=1;i<ThisRun.NParticles;i++){
        for(int k=0;k<3;k++){
            max[k] = fmax(CachedData[i].Pos[k],max[k]);
            min[k] = fmin(CachedData[i].Pos[k],min[k]);
        }
    }

	for(int k=0;k<3;k++){
        HydroRoot.PosMax[k] = max[k];
        HydroRoot.PosMin[k] = min[k];
    }

    double WidthMax = 0.e0;
	for(int k=0;k<3;k++){
        HydroNode[RootNodeID].Pos[k] = 0.5*(max[k] + min[k]);
        WidthMax = fmax(WidthMax,max[k]-min[k]);
    }
    HydroRoot.Width = WidthMax;

    HydroNode[RootNodeID].Next = NONE;
    HydroNode[RootNodeID].Parent = NONE;
    HydroNode[RootNodeID].Sister = NONE;
    HydroNode[RootNodeID].Children = NONE;

    HydroNode[RootNodeID].NumberofLeaves = ThisRun.NParticles;

    HydroNode[RootNodeID].Level = 0;
    HydroNode[RootNodeID].Leaves = 0;

    for(int i=0;i<ThisRun.NParticles;i++)
        HydroRoot.Leaves[i] = i;
    HydroRoot.NumberofLeaves = ThisRun.NParticles;

	return ;
}

/*!
 * This function finds the "next" tree node of the current tree node.  The
 * "next" node is the sister node if the tree node has the sister node.  If the
 * tree node do not have a sister node the sister node of the parent node. In
 * this case, if the sister node of the parent node does not exist, this process
 * is applied recursively to the upper level. 
 */
static int NextHydroNode(const int NodeID){

    int CurrentNodeID = NodeID;

    if(HydroNode[CurrentNodeID].Sister != NONE){
        CurrentNodeID = HydroNode[CurrentNodeID].Sister;
    } else {
        int NextNodeID = CurrentNodeID;
        while(1){
            if(HydroNode[HydroNode[NextNodeID].Parent].Sister != NONE){
                CurrentNodeID = HydroNode[HydroNode[NextNodeID].Parent].Sister;
                break;
            } else if(HydroNode[NextNodeID].Parent == 0){
                CurrentNodeID = 0;
                break;
            }
            NextNodeID = HydroNode[NextNodeID].Parent;
        }
    }
    return CurrentNodeID;
}

/*!
 * This function returns "true" if the number of particles contained in the node
 * is small enough, otherwise false.
 */
static inline bool __attribute__((always_inline)) HydroNodeSeparationCriterion(const int CurrentNodeID, const int CriticalNumber){

	if( (HydroNode[CurrentNodeID].NumberofLeaves <= CriticalNumber) || HydroNode[CurrentNodeID].Level+1>=TreeMaxNodeLevel){
        return true;
    } else {
        return false;
    }
}

/*!
 * Build the tree structure.
 */
static void BuildHydroTree(void){

    int NumberofNodes = 0; 
    int subhead[TreeNsub],subcurrent[TreeNsub],subnumber[TreeNsub]; 

    int NumberofNodeCreationLimit = HydroRoot.NumberofNodeCreationLimit;

    int CurrentMaxLevel = 0;
    int RootNodeID = 0; 
    int CurrentNodeID = RootNodeID;
    int ChildNodeID,BackwardNodeID,NextNodeID;

	while(1){

		if(HydroNodeSeparationCriterion(CurrentNodeID,NumberofNodeCreationLimit) && (CurrentNodeID != RootNodeID)){
			if(HydroNode[CurrentNodeID].Sister != NONE){
				CurrentNodeID = HydroNode[CurrentNodeID].Sister;
			}else{
				NextNodeID = CurrentNodeID;
				while(1){
                    if(HydroNode[HydroNode[NextNodeID].Parent].Sister != NONE){
                        CurrentNodeID = HydroNode[HydroNode[NextNodeID].Parent].Sister;
						break;
					} else if(HydroNode[NextNodeID].Parent == RootNodeID){ // End procedure.
                        HydroRoot.CurrentMaxLevel = CurrentMaxLevel;

                        int NumberofLeaves = HydroNode[RootNodeID].NumberofLeaves;
                        for(int k=0;k<NumberofLeaves;k++){
                            int leaf =  HydroRoot.Leaves[k];
                            NBCache[k].Pos[0] = CachedData[leaf].Pos[0];
                            NBCache[k].Pos[1] = CachedData[leaf].Pos[1];
                            NBCache[k].Pos[2] = CachedData[leaf].Pos[2];
                            NBCache[k].Kernel = CachedData[leaf].Kernel;
                            NBCache[k].Leaf = leaf;
                        }
                        HydroRoot.NumberofNodes = NumberofNodes + 1;
						return;
					}
                    NextNodeID = HydroNode[NextNodeID].Parent;
				}
			}
			continue;
		}

		for(int k=0;k<TreeNsub;k++){
			subnumber[k] = 0;
			subhead[k] = subcurrent[k] = NONE;
		}

		for(int i=0;i<HydroNode[CurrentNodeID].NumberofLeaves;i++){
            int leaf = HydroRoot.Leaves[HydroNode[CurrentNodeID].Leaves+i];
			int subindex = 0;

            for(int k=0;k<3;k++)
                if(HydroNode[CurrentNodeID].Pos[k] <= CachedData[leaf].Pos[k])
                    subindex += 1 << k;

			if(subnumber[subindex] == 0){
				subhead[subindex] = subcurrent[subindex] = leaf;
			} else {
				sublist[subcurrent[subindex]] = leaf;
				subcurrent[subindex] = leaf;
			}
			subnumber[subindex] ++;
        }


        ChildNodeID = CurrentNodeID;
		for(int i=0;i<TreeNsub;i++){
			if(subnumber[i] != 0){
				BackwardNodeID = ChildNodeID; 
                // make node
                NumberofNodes ++;
                ChildNodeID = NumberofNodes;
                if(NumberofNodes >= HydroRoot.NumberofAllocatedNodes){
                    int NumberofAllocatedNodes = (int)(1.2*NumberofNodes);
                    HydroNode = realloc(HydroNode,sizeof(struct StructHydroNode)*NumberofAllocatedNodes);
                    HydroRoot.NumberofAllocatedNodes = NumberofAllocatedNodes;
                }

                HydroNode[ChildNodeID].Next = NONE;
                HydroNode[ChildNodeID].Parent = NONE;
                HydroNode[ChildNodeID].Sister = NONE;
                HydroNode[ChildNodeID].Children = NONE;
                HydroNode[ChildNodeID].Parent = CurrentNodeID;

                if(BackwardNodeID == CurrentNodeID){
                    HydroNode[CurrentNodeID].Children = ChildNodeID;
					NextNodeID = ChildNodeID;
                    HydroNode[ChildNodeID].Leaves = HydroNode[CurrentNodeID].Leaves;
                    CurrentMaxLevel = MAX(CurrentMaxLevel,HydroNode[CurrentNodeID].Level+1);
                } else {
                    HydroNode[BackwardNodeID].Sister = ChildNodeID;
                    HydroNode[ChildNodeID].Leaves = HydroNode[BackwardNodeID].Leaves + HydroNode[BackwardNodeID].NumberofLeaves;
                }

                HydroRoot.Leaves[HydroNode[ChildNodeID].Leaves] = subhead[i];
                for(int k=1;k<subnumber[i];k++){
                    HydroRoot.Leaves[HydroNode[ChildNodeID].Leaves+k] = 
                        sublist[HydroRoot.Leaves[HydroNode[ChildNodeID].Leaves+k-1]];
                }
                HydroNode[ChildNodeID].NumberofLeaves = subnumber[i];
                HydroNode[ChildNodeID].Level = HydroNode[CurrentNodeID].Level+1;

				for(int k=0;k<3;k++)
                    HydroNode[ChildNodeID].Pos[k] = HydroNode[CurrentNodeID].Pos[k] +
						+ BitSign((i>>k)&1)*0.25*HydroRoot.Width*WidthFactor[HydroNode[CurrentNodeID].Level];

            }
		}
        CurrentNodeID = NextNodeID;
	}
}


/* !
 * This function implants data into tree nodes.
 */
static void HydroNodeDataImplant(const int CurrentNodeID){

    double KernelMax = 0.e0;
    double DistanceMax = 0.e0;

    if(HydroNode[CurrentNodeID].Children == NONE){
        int Number_of_leaf = HydroNode[CurrentNodeID].NumberofLeaves;
        int header = HydroNode[CurrentNodeID].Leaves;
        for(int k=0;k<Number_of_leaf;k++){
            int leaf = header+k;
            double Distance = DISTANCE(HydroNode[CurrentNodeID].Pos,NBCache[leaf].Pos);
            DistanceMax = fmax(Distance,DistanceMax);
            KernelMax = fmax(KernelMax,2.e0*NBCache[leaf].Kernel+Distance);
        }
        HydroNode[CurrentNodeID].KernelMax = KernelMax;
        HydroNode[CurrentNodeID].DistanceMax = DistanceMax;
    } else {
        int ChildNodeID = HydroNode[CurrentNodeID].Children;
        do{
            HydroNodeDataImplant(ChildNodeID);

            KernelMax = fmax(KernelMax,HydroNode[ChildNodeID].KernelMax);
            DistanceMax = fmax(DistanceMax,HydroNode[ChildNodeID].DistanceMax);
            ChildNodeID = HydroNode[ChildNodeID].Sister;
        } while(ChildNodeID != NONE);
        double Width = DiagHydro*WidthFactor[HydroNode[CurrentNodeID].Level];
        HydroNode[CurrentNodeID].KernelMax = KernelMax + Width;
        HydroNode[CurrentNodeID].DistanceMax = DistanceMax + Width;
    }

    return;
}

#include "config.h"

/*! \file TimeStep.c
 * \brief Functions regarding the evaluation of the timestep.
 */

/*!
 * This function returns the timestep of a particle "index" which satisfies the
 * CFL condition.
 */
static double CourantTimeStep(const int index){
    return ThisRun.CFL*SPH[index].Kernel/SPH[index].Vsig;
}

/*!
 * This function returns the acceleration-based timestep of a particle "index".
 */
static double AccTimeStep(const int index){
    return ThisRun.CFL*sqrt(SPH[index].Kernel/NORM(SPH[index].Acc));
}


/*!
 * This function returns the minimum timestep of particles.
 */
static double CalcTimeStep(void){

    double dt_next = CourantTimeStep(0); 
    for(int i=1;i<ThisRun.NParticles;i++){
        dt_next = fmin(dt_next,fmin(CourantTimeStep(i),AccTimeStep(i)));
    }
    return dt_next; 
}

/*!
 * This function evaluates the timestep. The length of the timestep is adjusted
 * automatically, if necessary.
 */
void UpdateTimeStep(void){

    ThisRun.dt = CalcTimeStep();

    // If the timestep is larger than the output timestep...
    if(ThisRun.dt > ThisRun.dt_output){
        ThisRun.dt = ThisRun.dt_output;
    }

    // If the current time + timestep is larger than the end time...
    if(ThisRun.TCurrent+ThisRun.dt>ThisRun.TEnd){
        ThisRun.dt = ThisRun.TEnd-ThisRun.TCurrent;
    }

    if(ThisRun.ShowLog){
        fprintf(stderr,"dt %g\n",ThisRun.dt);
        fflush(stderr);
    }

    return ;
}

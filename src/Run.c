#include "config.h"
#include "TimeStep.h"
#include "Integral.h"
#include "Hydro.h"
#include "CalcGravity.h"
#include "NeighborSearch.h"

/*! \file Run.c
 * \brief Main loop for the time integration.
 */

/*!
 * This function displays the current time and timestep on the standard error.
 */
static void ShowCurrentTimeStep(void){
#ifdef TASK_KELVINHELMHOLTZ_TEST
    fprintf(stderr,"t = %g, dt = %g\n",ThisRun.TCurrent/ThisRun.TNorm,ThisRun.dt/ThisRun.TNorm);
#else
    fprintf(stderr,"t = %g, dt = %g\n",ThisRun.TCurrent,ThisRun.dt);
#endif

    return ;
}

/*!
 * This is the main function of the time integration of particles.  The
 * leap-frog method is adopted.
 */
void Run(void){

    ThisRun.Tstart = GetElapsedTime();

    UpdateHydro();
    UpdateGravity();
    Output(0.0);

    ThisRun.NStep = 0;
    ThisRun.TCurrent = 0.e0;
    do{
        ShowCurrentTimeStep();

        UpdateTimeStep();
        Kick1(0.5*ThisRun.dt);
        Drift(ThisRun.dt);
        UpdateHydro();
        UpdateGravity();
        Kick2(0.5*ThisRun.dt);

        Output(ThisRun.TCurrent);
        Log(ThisRun.TCurrent);
        ThisRun.TCurrent += ThisRun.dt;
        ThisRun.NStep ++;
    } while(ThisRun.TCurrent<ThisRun.TEnd);

    Output(ThisRun.TCurrent);

    ThisRun.Tfin = GetElapsedTime();

    return ;
}


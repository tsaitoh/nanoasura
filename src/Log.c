#include "config.h"

/*! \file Log.c
 * \brief Output log file.
 */

static int OutputTimes = 0; //!< Counter for the output file.
static bool FirstCallLog = true; //!< A flag which checks the first call of Log().

/*!
 * This function writes the header part of the output file.
 */
static void WriteHeader(const double t, FILE *fp){

    if(ThisRun.UseDISPH == 1){
        fprintf(fp,"1\n");
    } else {
        fprintf(fp,"0\n");
    }
    fprintf(fp,"%d\n",ThisRun.NParticles);
    fprintf(fp,"%g\n",t);
    fprintf(fp,"%g\n",ThisRun.TNorm);
    fprintf(fp,"%g\n",ThisRun.Gamma);

    return ;
}

/*!
 * This function writes all particle data in a file.
 */
void Output(const double t){

    if((ThisRun.WriteEveryStep == 0)&&(OutputTimes*ThisRun.dt_output>t)){
        return ;
    }

    FILE *fp;
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/%s.%04d",ThisRun.OutDir,ThisRun.RunName,OutputTimes);

    FileOpen(fp,fname,"w");
    WriteHeader(t,fp);

    for(int i=0;i<ThisRun.NParticles;i++){
        if(ThisRun.UseDISPH==1){
            double Pressure = ThisRun.Gm1*SPH[i].q;
            fprintf(fp,"%g %g %g %g %g %g %g %g %g %g %g %g %d %d\n",
                SPH[i].Pos[0],SPH[i].Pos[1],SPH[i].Pos[2],
                SPH[i].Vel[0],SPH[i].Vel[1],SPH[i].Vel[2],SPH[i].Mass,
                    SPH[i].Kernel,SPH[i].Rho,SPH[i].u,
                        Pressure,SPH[i].q,SPH[i].Nlist,SPH[i].Tag);
        } else {
            fprintf(fp,"%g %g %g %g %g %g %g %g %g %g %g %g %d %d\n",
                SPH[i].Pos[0],SPH[i].Pos[1],SPH[i].Pos[2],
                SPH[i].Vel[0],SPH[i].Vel[1],SPH[i].Vel[2],SPH[i].Mass,
                    SPH[i].Kernel,SPH[i].Rho,SPH[i].u,
                        ThisRun.Gm1*SPH[i].Rho*SPH[i].u,SPH[i].Rho*SPH[i].u,
                            SPH[i].Nlist,SPH[i].Tag);
        }
    }

    fclose(fp);
    fflush(fp);
    OutputTimes ++;

    return ;
}

/*!
 * This function writes logs.
 */
void Log(const double t){

    FILE *fp;
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/Energy.dat",ThisRun.OutDir);

    if(FirstCallLog == true){
        FileOpen(fp,fname,"w");
        FirstCallLog = false;
    } else {
        FileOpen(fp,fname,"a");
    }
    double Ek = 0.e0, // Kinetic energy
           Ep = 0.e0, // Potential energy
           Et = 0.e0; // Thermal energy

    for(int i=0;i<ThisRun.NParticles;i++){
        Ek += 0.5*SPH[i].Mass*NORM2(SPH[i].Vel);
        Ep += 0.5*SPH[i].Mass*SPH[i].Pot;
        Et += SPH[i].Mass*SPH[i].u;
    }
    fprintf(fp,"%g %g %g %g %g\n",t,Ek,Ep,Et,Ek+Ep+Et);

    fclose(fp);

    return ;
}

#include "config.h"
#include "./setup/SetupGalaxyModel.h"


/*!
 * Calculate and add extra force on particles and impose the boundary condition.
 */
void AddExtraForce(void){

    // For RT run.
    if(ThisRun.RunType == 3){
        for(int i=0;i<ThisRun.NParticles;i++){
            SPH[i].Acc[1] -= 0.5;

            if(SPH[i].Tag == 0){
                SPH[i].Vel[0] = SPH[i].Vel[1] = SPH[i].Vel[2] = 0.e0;
                SPH[i].Acc[0] = SPH[i].Acc[1] = SPH[i].Acc[2] = 0.e0;
                SPH[i].du = 0.e0;

            } else if((SPH[i].Pos[1] < 0.1)||(SPH[i].Pos[1] > 0.9)){
                if((SPH[i].Pos[1] < 0.1)&&(SPH[i].Vel[1]<0.0)){
                    SPH[i].Vel[1] *= -1.0;
                    SPH[i].Velh[1] *= -1.0;
                } else if((SPH[i].Pos[1] > 0.9)&&(SPH[i].Vel[1]>0.0)){
                    SPH[i].Vel[1] *= -1.0;
                    SPH[i].Velh[1] *= -1.0;
                }
            }
        }
    } else if (ThisRun.RunType == 5){
#define GM (1)
        for(int i=0;i<ThisRun.NParticles;i++){
            double r = NORM(SPH[i].Pos);
            if(r < 0.25){
                r = sqrt(SQ(r) + SQ(0.25));
                SPH[i].Acc[0] += -(GM/CUBE(r))*SPH[i].Pos[0];
                SPH[i].Acc[1] += -(GM/CUBE(r))*SPH[i].Pos[1];
            } else {
                SPH[i].Acc[0] += -(GM/CUBE(r))*SPH[i].Pos[0];
                SPH[i].Acc[1] += -(GM/CUBE(r))*SPH[i].Pos[1];
            }

        }
    } else if (ThisRun.RunType == 6){
        AccelerationByMilkyWayPotentialHaloDisk();
    }
    return ;
}


#include "config.h"

/*! \file Exit.c
 * \brief Show simulation log.
 */

/*!
 * This function shows the log of the run on the standard error when the run is
 * end.
 */
void ASURA_Exit(void){

    fprintf(stderr,"This run is finished successfully.\n");
    fprintf(stderr," Elapsed time is %g [sec] \n",ThisRun.Tfin-ThisRun.Tstart);
    fprintf(stderr," Total time step is %d\n",ThisRun.NStep);
    fprintf(stderr," Calculation time per step is %g [sec]\n",
            (ThisRun.Tfin-ThisRun.Tstart)/(double)ThisRun.NStep);
    fprintf(stderr," Calculation time per step per particle is %g [sec]\n",
            (ThisRun.Tfin-ThisRun.Tstart)/(double)ThisRun.NStep/(double)ThisRun.NParticles);

    return ;
}

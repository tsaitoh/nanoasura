#pragma once 

void InitXorshift(uint32_t seed);
double XorshiftRand(void);
double XorshiftGauss(const double mu, const double sigma);

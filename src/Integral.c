#include "config.h"

/*! \file Integral.c
 * \brief Integrate positions and velocities using the Leap-frog method.
 */


/*!
 * First kick.
 *
 * This function advances particles' velocities and spesific internal energies
 * from the timestep n to the timestep n+1/2.
 */
void Kick1(const double TimeStep){

    if(ThisRun.ShowLog){
        fprintf(stderr,"First kick start... ");
        fflush(stderr);
    }

    for(int i=0;i<ThisRun.NParticles;i++){
        SPH[i].Velh[0] = SPH[i].Vel[0] + TimeStep*SPH[i].Acc[0];
        SPH[i].Velh[1] = SPH[i].Vel[1] + TimeStep*SPH[i].Acc[1];
        SPH[i].Velh[2] = SPH[i].Vel[2] + TimeStep*SPH[i].Acc[2];
        SPH[i].uh = SPH[i].u + TimeStep*SPH[i].du;
        assert(SPH[i].uh>0.e0);
    }

    if(ThisRun.ShowLog){
        fprintf(stderr,"finish.\n");
        fflush(stderr);
    }

    return ;
}

/*!
 * Drift.
 *
 * This function advances particles' positions from the timestep n to the timestep
 * n+1 using the velocities at n+1/2.
 */
void Drift(const double TimeStep){

    if(ThisRun.ShowLog){
        fprintf(stderr,"Drift start... ");
        fflush(stderr);
    }

    for(int i=0;i<ThisRun.NParticles;i++){
        SPH[i].Pos[0] += TimeStep*SPH[i].Velh[0];
        SPH[i].Pos[1] += TimeStep*SPH[i].Velh[1];
        SPH[i].Pos[2] += TimeStep*SPH[i].Velh[2];
        SPH[i].Vel[0] += TimeStep*SPH[i].Acc[0];
        SPH[i].Vel[1] += TimeStep*SPH[i].Acc[1];
        SPH[i].Vel[2] += TimeStep*SPH[i].Acc[2];

        SPH[i].u += TimeStep*SPH[i].du;
        if(SPH[i].u < 0.e0){
            fprintf(stderr,"%d %g %g %g\n",i,SPH[i].u,SPH[i].du,
                    TimeStep*SPH[i].du);
            fprintf(stderr,"%d Rho %g, Posx %g Velx %g\n",i,SPH[i].Rho,SPH[i].Pos[0],SPH[i].Vel[0]);
            assert(SPH[i].u>0.e0);
        }
    }
    if(ThisRun.ShowLog){
        fprintf(stderr,"finish.\n");
        fflush(stderr);
    }

    PeriodicWrapping();

    return ;
}

/*!
 * Second kick.
 *
 * This function advances particles' velocities from the timestep n+1/2 to the
 * timestep n+1 using the accelerations at n+1.
 */
void Kick2(const double TimeStep){

    if(ThisRun.ShowLog){
        fprintf(stderr,"Second kick start... ");
        fflush(stderr);
    }

    for(int i=0;i<ThisRun.NParticles;i++){
        SPH[i].Vel[0] = SPH[i].Velh[0] + TimeStep*SPH[i].Acc[0];
        SPH[i].Vel[1] = SPH[i].Velh[1] + TimeStep*SPH[i].Acc[1];
        SPH[i].Vel[2] = SPH[i].Velh[2] + TimeStep*SPH[i].Acc[2];
        SPH[i].u = SPH[i].uh + TimeStep*SPH[i].du;

        if(SPH[i].u < 0.e0){
            fprintf(stderr,"%d %g %g %g\n",i,SPH[i].u,SPH[i].du,
                    TimeStep*SPH[i].du);
            fflush(NULL);
            assert(SPH[i].u>0.e0);
        }
    }

    if(ThisRun.ShowLog){
        fprintf(stderr,"finish.\n");
        fflush(stderr);
    }

    return ;
}

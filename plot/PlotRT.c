#include "config.h"
#include "PlantHydroTree.h"
#include "NeighborSearch.h"
#include "ReadData.h"
#include "PlotMisc.h"

static void PlotRT(const char fname[]){

    mplc_open(0,PlotParams.PythonPath);
    if(PlotParams.RT_UseCopy == 1){
        mplc_screen(0,PlotParams.RT_Xmin,PlotParams.RT_Ymin,4*PlotParams.RT_Xmax,PlotParams.RT_Ymax,true);
    } else {
        mplc_screen(0,PlotParams.RT_Xmin,PlotParams.RT_Ymin,PlotParams.RT_Xmax,PlotParams.RT_Ymax,true);
    }

    if(PlotParams.RT_PlotType == 0){
        double PosX[ThisRun.NParticles];
        double PosY[ThisRun.NParticles];

        for(int k=0;k<3;k++){
            int counter = 0;
            for(int i=0;i<ThisRun.NParticles;i++){
                if(SPH[i].Tag == k){
                    PosX[counter] = SPH[i].Pos[0];
                    PosY[counter] = SPH[i].Pos[1];
                    counter ++;
                }
            }

            if(PlotParams.RT_UseCopy == 1){
                if(k==0){
                    mplc_scatter(0,PosX,PosY,NULL,counter,"darkgreen","green","s=3, marker='o'",false,"");
                } else if(k==1){
                    mplc_scatter(0,PosX,PosY,NULL,counter,"darkblue","blue","s=3, marker='o'",false,"");
                } else if(k==2){
                    mplc_scatter(0,PosX,PosY,NULL,counter,"darkred","red","s=3, marker='o'",false,"");
                }
                for(int j=0;j<3;j++){
                    for(int l=0;l<counter;l++)
                        PosX[l] += 0.25;
                    if(k==0){
                        mplc_scatter(0,PosX,PosY,NULL,counter,"darkgreen","green","s=3, marker='o'",false,"");
                    } else if(k==1){
                        mplc_scatter(0,PosX,PosY,NULL,counter,"darkblue","blue","s=3, marker='o'",false,"");
                    } else if(k==2){
                        mplc_scatter(0,PosX,PosY,NULL,counter,"darkred","red","s=3, marker='o'",false,"");
                    }
                }
            } else {
                if(k==0){
                    mplc_scatter(0,PosX,PosY,NULL,counter,"darkgreen","green","s=3, marker='o'",false,"");
                } else if(k==1){
                    mplc_scatter(0,PosX,PosY,NULL,counter,"darkblue","blue","s=3, marker='o'",false,"");
                } else if(k==2){
                    mplc_scatter(0,PosX,PosY,NULL,counter,"darkred","red","s=3, marker='o'",false,"");
                }
            }
        }
    } else if (PlotParams.RT_PlotType == 1){
        double PosX[ThisRun.NParticles];
        double PosY[ThisRun.NParticles];
        double Color[ThisRun.NParticles];

        for(int i=0;i<ThisRun.NParticles;i++){
            PosX[i] = SPH[i].Pos[0];
            PosY[i] = SPH[i].Pos[1];
            Color[i] = SPH[i].Rho;
        }
        mplc_scatter(0,PosX,PosY,Color,ThisRun.NParticles,"black","","vmin=0.9,vmax=2.1,linewidth=0, s=5, marker='o'",true,"label='Density'");

        if(PlotParams.RT_UseCopy == 1){
            for(int j=0;j<3;j++){
                for(int i=0;i<ThisRun.NParticles;i++){
                    PosX[i] +=0.25;
                }
                mplc_scatter(0,PosX,PosY,Color,ThisRun.NParticles,"black","","vmin=0.9,vmax=2.1,linewidth=0, s=5, marker='o'",false,"");
            }
        }
    } else if (PlotParams.RT_PlotType == 2){
        double PosX[ThisRun.NParticles];
        double PosY[ThisRun.NParticles];
        double Color[ThisRun.NParticles];

        for(int i=0;i<ThisRun.NParticles;i++){
            PosX[i] = SPH[i].Pos[0];
            PosY[i] = SPH[i].Pos[1];
            Color[i] = SPH[i].Pressure;
        }
        mplc_scatter(0,PosX,PosY,Color,ThisRun.NParticles,"black","","vmin=1.0,vmax=2.0,linewidth=0, s=5, marker='o'",true,"label='Pressure'");
        if(PlotParams.RT_UseCopy == 1){
            for(int j=0;j<3;j++){
                for(int i=0;i<ThisRun.NParticles;i++){
                    PosX[i] +=0.25;
                }
                mplc_scatter(0,PosX,PosY,Color,ThisRun.NParticles,"black","","vmin=1.0,vmax=2.0,linewidth=0, s=5, marker='o'",false,"");
            }
        }
    } else if (PlotParams.RT_PlotType == 3){
        ThisRun.PeriodicBoundary = 1;
        ThisRun.Dimension = 2;
        ThisRun.LBox[0] = 0.25;
        ThisRun.LBox[1] = ThisRun.LBox[2] = 1.0;

        PlantHydroTree();

        double *GridData;
        if(PlotParams.RT_UseCopy == 1){
            GridData = malloc(sizeof(double)*PlotParams.RT_GridSize*PlotParams.RT_GridSize);

            double dX = 4*(PlotParams.RT_Xmax-PlotParams.RT_Xmin)/(double)PlotParams.RT_GridSize;
            double dY = (PlotParams.RT_Ymax-PlotParams.RT_Ymin)/(double)PlotParams.RT_GridSize;
            double H_init = (PlotParams.RT_Xmax-PlotParams.RT_Xmin)/(double)PlotParams.RT_GridSize;
            for(int i=0;i<PlotParams.RT_GridSize/4;i++){
                for(int k=0;k<PlotParams.RT_GridSize;k++){
                    double Pos[3]; 
                    Pos[0] = dX*(i+0.5)+PlotParams.RT_Xmin;
                    Pos[1] = dY*(k+0.5)+PlotParams.RT_Ymin;
                    Pos[2] = 0.5;
                    GridData[k*PlotParams.RT_GridSize+i] = CalcGridDensity(Pos,&H_init,ThisRun.Dimension);
                }
            }
            for(int i=PlotParams.RT_GridSize/4;i<PlotParams.RT_GridSize;i++){
                for(int k=0;k<PlotParams.RT_GridSize;k++){
                    GridData[k*PlotParams.RT_GridSize+i] = GridData[k*PlotParams.RT_GridSize+i%(PlotParams.RT_GridSize/4)];
                }
            }

            mplc_send_command(0,"extent = 0,1,0,1\n");
            mplc_imshow(0,GridData,PlotParams.RT_GridSize,"extent=extent,vmin=0.9,vmax=2.1,interpolation='nearest'",true,"label='Density'");
        } else {
            GridData = malloc(sizeof(double)*PlotParams.RT_GridSize*PlotParams.RT_GridSize);

            double dX = 4*(PlotParams.RT_Xmax-PlotParams.RT_Xmin)/(double)PlotParams.RT_GridSize;
            double dY = (PlotParams.RT_Ymax-PlotParams.RT_Ymin)/(double)PlotParams.RT_GridSize;
            double H_init = dX;
            for(int i=0;i<PlotParams.RT_GridSize;i++){
                for(int k=0;k<PlotParams.RT_GridSize;k++){
                    double Pos[3]; 
                    Pos[0] = dX*(i+0.5)+PlotParams.RT_Xmin;
                    Pos[1] = dY*(k+0.5)+PlotParams.RT_Ymin;
                    Pos[2] = 0.5;
                    GridData[k*PlotParams.RT_GridSize+i] = CalcGridDensity(Pos,&H_init,ThisRun.Dimension);
                }
            }

            mplc_send_command(0,"extent = 0,1,0,1\n");
            mplc_imshow(0,GridData,PlotParams.RT_GridSize,"extent=extent,vmin=0.9,vmax=2.1,interpolation='nearest'",true,"label='Density'");
        }
        free(GridData);
    }

    if(PlotParams.ShowTime){
        char Time[MaxCharactersInLine];
        Snprintf(Time,"t = %1.2f ",ThisRun.TCurrent);
        fprintf(stderr,"t = %1.2f\n",ThisRun.TCurrent);
    }

    mplc_xlabel(0,"X");
    mplc_ylabel(0,"Y");
    mplc_save(0,fname,"");
    mplc_close(0);

    return ;
}


void PlotRayleighTaylor(const int FileID){

    if(!ReadData(FileID)){
        return ;
    }

    char FileNameWrite[MaxCharactersInLine];
    if(PlotParams.RT_PlotType == 0){
        Snprintf(FileNameWrite,"%s_dot",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.RT_PlotType == 1){
        Snprintf(FileNameWrite,"%s_Rho",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.RT_PlotType == 2){
        Snprintf(FileNameWrite,"%s_P",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.RT_PlotType == 3){
        Snprintf(FileNameWrite,"%s_Grid",PlotParams.FileName[PlotParams.PlotType]);
    }

    char fname[MaxCharactersInLine];
    if(PlotParams.WriteMode == 0){
        Snprintf(fname,"./%s/%s.%04d.pdf",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    } else if(PlotParams.WriteMode == 1){
        Snprintf(fname,"./%s/%s.%04d.eps",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    } else if(PlotParams.WriteMode == 2){
        Snprintf(fname,"./%s/%s.%04d.png",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    }

    PlotRT(fname);

    return ;
}

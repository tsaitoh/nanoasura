#include "config.h"

void ReleaseSPH(void){
    if(SPH!=NULL)
        free(SPH);
    return ;
}

void ReadHeader(FILE *fp){

    fscanf(fp,"%d",&ThisRun.UseDISPH);
    fscanf(fp,"%d",&ThisRun.NParticles);
    fscanf(fp,"%le",&ThisRun.TCurrent);
    fscanf(fp,"%le",&ThisRun.TNorm);
    fscanf(fp,"%le",&ThisRun.Gamma);
    ThisRun.Gm1 = ThisRun.Gamma-1.0;
    ThisRun.GGm1 = ThisRun.Gamma*(ThisRun.Gamma-1.0);

    fprintf(stderr,"SPHType = %d, N = %d, t = %g, Gamma = %g\n",
            ThisRun.UseDISPH,ThisRun.NParticles,ThisRun.TCurrent,ThisRun.Gamma);

    return ;
}

bool ReadData(const int FileID){

    FILE *fp;
    char fname[MaxCharactersInLine];
    Snprintf(fname,"%s/%s.%04d",PlotParams.DataDir[PlotParams.PlotType],PlotParams.FileName[PlotParams.PlotType],FileID);
    //fprintf(stderr,"%s\n",fname);

    if(!CheckFile(fname)){
        return false;
    }

    fprintf(stderr,"Start File Read: %s/%s.%03d\n",PlotParams.DataDir[PlotParams.PlotType],PlotParams.FileName[PlotParams.PlotType],FileID);

    ThisRun.NParticles = 0;
    FileOpen(fp,fname,"r");
    ReadHeader(fp);

    SPH = realloc(SPH,sizeof(struct StructSPH)*ThisRun.NParticles);

    for(int i=0;i<ThisRun.NParticles;i++){
        fscanf(fp,"%le %le %le %le %le %le %le %le %le %le %le %le %d %d",
                &(SPH[i].Pos[0]),&(SPH[i].Pos[1]),&(SPH[i].Pos[2]),
                &(SPH[i].Vel[0]),&(SPH[i].Vel[1]),&(SPH[i].Vel[2]),&(SPH[i].Mass),
                &(SPH[i].Kernel),&(SPH[i].Rho),&(SPH[i].u),&(SPH[i].Pressure),
                &(SPH[i].q),
                &(SPH[i].Nlist),&(SPH[i].Tag));
        if(ThisRun.UseDISPH == 1){
            SPH[i].Rho = SPH[i].q/SPH[i].u;
        }
    }
    fclose(fp);

    fprintf(stderr,"Finish File Read: %s/%s.%03d\n",PlotParams.DataDir[PlotParams.PlotType],PlotParams.FileName[PlotParams.PlotType],FileID);

    return true;
}


#include "config.h"
#include "PlantHydroTree.h"
#include "NeighborSearch.h"
#include "ReadData.h"
#include "PlotShocktube.h"
#include "PlotHydroStatic.h"
#include "PlotKH.h"
#include "PlotRT.h"
#include "PlotSedov.h"
#include "PlotKeplar.h"
#include "PlotGalaxy.h"
#include "PlotEvrard.h"
#include "PlotMisc.h"

void Plot(void){

    for(int i=PlotParams.FileStart;i<=PlotParams.FileEnd;i++){
        if(PlotParams.PlotType == 0){
            PlotShocktube(i);
        } else if(PlotParams.PlotType == 1){
            PlotHydroStatic(i);
        } else if(PlotParams.PlotType == 2){
            PlotKelvinHelmholtz(i);
        } else if(PlotParams.PlotType == 3){
            PlotRayleighTaylor(i);
        } else if(PlotParams.PlotType == 4){
            PlotSedov(i);
        } else if(PlotParams.PlotType == 5){
            PlotKeplar(i);
        } else if(PlotParams.PlotType == 6){
            PlotGalaxy(i);
        } else if(PlotParams.PlotType == 7){
            PlotEvrard(i);
        }
    }


    ReleaseSPH();

    return ;
}

#include "config.h"
#include "toml.h"

static void read_tomlfile_Environment(const toml_table_t* config){

    toml_table_t* tab = toml_table_in(config, "Environment");
    if (!tab) {
        fprintf(stderr,"missing [Environment]\n");
        return;
    }
    fprintf(stderr,"[Environment]\n");

    toml_datum_t d = toml_string_in(tab, "PythonPath");
    if(d.ok){
        fprintf(stderr,"\t PythonPath = %s\n",d.u.s);
        strncpy(PlotParams.PythonPath,d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    return ;
}

static void read_tomlfile_Mode(const toml_table_t* config){

    toml_table_t* tab = toml_table_in(config, "Mode");
    if (!tab) {
        fprintf(stderr,"missing [Mode]\n");
        return;
    }
    fprintf(stderr,"[Mode]\n");

    toml_datum_t d = toml_int_in(tab, "PlotType");
    if(d.ok){
        fprintf(stderr,"\t PlotType = %ld\n",d.u.i);
        PlotParams.PlotType = d.u.i;
    }

    d = toml_int_in(tab, "WriteMode");
    if(d.ok){
        fprintf(stderr,"\t WriteMode = %ld\n",d.u.i);
        PlotParams.WriteMode = d.u.i;
    }

    d = toml_int_in(tab, "ShowTime");
    if(d.ok){
        fprintf(stderr,"\t ShowTime = %ld\n",d.u.i);
        PlotParams.ShowTime = d.u.i;
    }
    return ;
}

static void read_tomlfile_Targetfiles(const toml_table_t* config){

    toml_table_t* tab = toml_table_in(config, "TargetFiles");
    if (!tab) {
        fprintf(stderr,"missing [TargetFiles]\n");
        return;
    }
    fprintf(stderr,"[TargetFiles]\n");

    toml_datum_t d = toml_int_in(tab, "FileStart");
    if(d.ok){
        fprintf(stderr,"\t FileStart = %ld\n",d.u.i);
        PlotParams.FileStart = d.u.i;
    }

    d = toml_int_in(tab, "FileEnd");
    if(d.ok){
        fprintf(stderr,"\t FileEnd = %ld\n",d.u.i);
        PlotParams.FileEnd = d.u.i;
    }

    return ;
}

static void read_tomlfile_Shocktube(const toml_table_t* config){

    toml_table_t* tab = toml_table_in(config, "Shocktube");
    if (!tab) {
        fprintf(stderr,"missing [Shocktube]\n");
        return;
    }
    fprintf(stderr,"[Shocktube]\n");

    toml_datum_t d = toml_string_in(tab, "OutDir");
    if(d.ok){
        fprintf(stderr,"\t OutDir = %s\n",d.u.s);
        strncpy(PlotParams.OutDir[0],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_string_in(tab, "FileName");
    if(d.ok){
        fprintf(stderr,"\t FileName = %s\n",d.u.s);
        strncpy(PlotParams.FileName[0],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_string_in(tab, "DataDir");
    if(d.ok){
        fprintf(stderr,"\t DataDir = %s\n",d.u.s);
        strncpy(PlotParams.DataDir[0],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_int_in(tab, "PlotType");
    if(d.ok){
        fprintf(stderr,"\t PlotType = %ld\n",d.u.i);
        PlotParams.ST_PlotType = d.u.i;
    }

    d = toml_int_in(tab, "PlotSolution");
    if(d.ok){
        fprintf(stderr,"\t PlotSolution = %ld\n",d.u.i);
        PlotParams.ST_PlotSolution = d.u.i;
    }

    d = toml_string_in(tab, "SolutionFileName");
    if(d.ok){
        fprintf(stderr,"\t SolutionFileName = %s\n",d.u.s);
        strncpy(PlotParams.ST_SolutionFileName,d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_double_in(tab, "Xmin");
    if(d.ok){
        fprintf(stderr,"\t Xmin = %le\n",d.u.d);
        PlotParams.ST_Xmin = d.u.d;
    }

    d = toml_double_in(tab, "Xmax");
    if(d.ok){
        fprintf(stderr,"\t Xmax = %le\n",d.u.d);
        PlotParams.ST_Xmax = d.u.d;
    }

    d = toml_double_in(tab, "Ymin");
    if(d.ok){
        fprintf(stderr,"\t Ymin = %le\n",d.u.d);
        PlotParams.ST_Ymin = d.u.d;
    }

    d = toml_double_in(tab, "Ymax");
    if(d.ok){
        fprintf(stderr,"\t Ymax = %le\n",d.u.d);
        PlotParams.ST_Ymax = d.u.d;
    }

    d = toml_double_in(tab, "Rhomin");
    if(d.ok){
        fprintf(stderr,"\t Rhomin = %le\n",d.u.d);
        PlotParams.ST_Rhomin = d.u.d;
    }

    d = toml_double_in(tab, "Rhomax");
    if(d.ok){
        fprintf(stderr,"\t Rhomax = %le\n",d.u.d);
        PlotParams.ST_Rhomax = d.u.d;
    }

    d = toml_double_in(tab, "Pmin");
    if(d.ok){
        fprintf(stderr,"\t Pmin = %le\n",d.u.d);
        PlotParams.ST_Pmin = d.u.d;
    }

    d = toml_double_in(tab, "Pmax");
    if(d.ok){
        fprintf(stderr,"\t Pmax = %le\n",d.u.d);
        PlotParams.ST_Pmax = d.u.d;
    }

    d = toml_double_in(tab, "Umin");
    if(d.ok){
        fprintf(stderr,"\t Umin = %le\n",d.u.d);
        PlotParams.ST_Umin = d.u.d;
    }

    d = toml_double_in(tab, "Umax");
    if(d.ok){
        fprintf(stderr,"\t Umax = %le\n",d.u.d);
        PlotParams.ST_Umax = d.u.d;
    }

    d = toml_double_in(tab, "Vmin");
    if(d.ok){
        fprintf(stderr,"\t Vmin = %le\n",d.u.d);
        PlotParams.ST_Vmin = d.u.d;
    }

    d = toml_double_in(tab, "Vmax");
    if(d.ok){
        fprintf(stderr,"\t Vmax = %le\n",d.u.d);
        PlotParams.ST_Vmax = d.u.d;
    }

    return ;
}

static void read_tomlfile_Hydrostatic(const toml_table_t* config){

    toml_table_t* tab = toml_table_in(config, "Hydrostatic");
    if (!tab) {
        fprintf(stderr,"missing [Hydrostatic]\n");
        return;
    }
    fprintf(stderr,"[Hydrostatic]\n");

    toml_datum_t d = toml_string_in(tab, "OutDir");
    if(d.ok){
        fprintf(stderr,"\t OutDir = %s\n",d.u.s);
        strncpy(PlotParams.OutDir[1],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_string_in(tab, "FileName");
    if(d.ok){
        fprintf(stderr,"\t FileName = %s\n",d.u.s);
        strncpy(PlotParams.FileName[1],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_string_in(tab, "DataDir");
    if(d.ok){
        fprintf(stderr,"\t DataDir = %s\n",d.u.s);
        strncpy(PlotParams.DataDir[1],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_int_in(tab, "PlotType");
    if(d.ok){
        fprintf(stderr,"\t PlotType = %ld\n",d.u.i);
        PlotParams.HS_PlotType = d.u.i;
    }

    d = toml_double_in(tab, "Xmin");
    if(d.ok){
        fprintf(stderr,"\t Xmin = %le\n",d.u.d);
        PlotParams.HS_Xmin = d.u.d;
    }

    d = toml_double_in(tab, "Xmax");
    if(d.ok){
        fprintf(stderr,"\t Xmax = %le\n",d.u.d);
        PlotParams.HS_Xmax = d.u.d;
    }

    d = toml_double_in(tab, "Ymin");
    if(d.ok){
        fprintf(stderr,"\t Ymin = %le\n",d.u.d);
        PlotParams.HS_Ymin = d.u.d;
    }

    d = toml_double_in(tab, "Ymax");
    if(d.ok){
        fprintf(stderr,"\t Ymax = %le\n",d.u.d);
        PlotParams.HS_Ymax = d.u.d;
    }

    d = toml_int_in(tab, "GridSize");
    if(d.ok){
        fprintf(stderr,"\t GridSize = %ld\n",d.u.i);
        PlotParams.HS_GridSize = d.u.i;
    }

    return ;
}

static void read_tomlfile_KelvinHelmholtz(const toml_table_t* config){

    toml_table_t* tab = toml_table_in(config, "KelvinHelmholtz");
    if (!tab) {
        fprintf(stderr,"missing [KelvinHelmholtz]\n");
        return;
    }
    fprintf(stderr,"[KelvinHelmholtz]\n");

    toml_datum_t d = toml_string_in(tab, "OutDir");
    if(d.ok){
        fprintf(stderr,"\t OutDir = %s\n",d.u.s);
        strncpy(PlotParams.OutDir[2],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_string_in(tab, "FileName");
    if(d.ok){
        fprintf(stderr,"\t FileName = %s\n",d.u.s);
        strncpy(PlotParams.FileName[2],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_string_in(tab, "DataDir");
    if(d.ok){
        fprintf(stderr,"\t DataDir = %s\n",d.u.s);
        strncpy(PlotParams.DataDir[2],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_int_in(tab, "PlotType");
    if(d.ok){
        fprintf(stderr,"\t PlotType = %ld\n",d.u.i);
        PlotParams.KH_PlotType = d.u.i;
    }

    d = toml_double_in(tab, "Xmin");
    if(d.ok){
        fprintf(stderr,"\t Xmin = %le\n",d.u.d);
        PlotParams.KH_Xmin = d.u.d;
    }

    d = toml_double_in(tab, "Xmax");
    if(d.ok){
        fprintf(stderr,"\t Xmax = %le\n",d.u.d);
        PlotParams.KH_Xmax = d.u.d;
    }

    d = toml_double_in(tab, "Ymin");
    if(d.ok){
        fprintf(stderr,"\t Ymin = %le\n",d.u.d);
        PlotParams.KH_Ymin = d.u.d;
    }

    d = toml_double_in(tab, "Ymax");
    if(d.ok){
        fprintf(stderr,"\t Ymax = %le\n",d.u.d);
        PlotParams.KH_Ymax = d.u.d;
    }

    d = toml_int_in(tab, "GridSize");
    if(d.ok){
        fprintf(stderr,"\t GridSize = %ld\n",d.u.i);
        PlotParams.KH_GridSize = d.u.i;
    }

    return ;
}

static void read_tomlfile_RayleighTaylor(const toml_table_t* config){

    toml_table_t* tab = toml_table_in(config, "RayleighTaylor");
    if (!tab) {
        fprintf(stderr,"missing [RayleighTaylor]\n");
        return;
    }
    fprintf(stderr,"[RayleighTaylor]\n");

    toml_datum_t d = toml_string_in(tab, "OutDir");
    if(d.ok){
        fprintf(stderr,"\t OutDir = %s\n",d.u.s);
        strncpy(PlotParams.OutDir[3],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_string_in(tab, "FileName");
    if(d.ok){
        fprintf(stderr,"\t FileName = %s\n",d.u.s);
        strncpy(PlotParams.FileName[3],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_string_in(tab, "DataDir");
    if(d.ok){
        fprintf(stderr,"\t DataDir = %s\n",d.u.s);
        strncpy(PlotParams.DataDir[3],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_int_in(tab, "PlotType");
    if(d.ok){
        fprintf(stderr,"\t PlotType = %ld\n",d.u.i);
        PlotParams.RT_PlotType = d.u.i;
    }

    d = toml_int_in(tab, "UseCopy");
    if(d.ok){
        fprintf(stderr,"\t UseCopy = %ld\n",d.u.i);
        PlotParams.RT_UseCopy = d.u.i;
    }

    d = toml_double_in(tab, "Xmin");
    if(d.ok){
        fprintf(stderr,"\t Xmin = %le\n",d.u.d);
        PlotParams.RT_Xmin = d.u.d;
    }

    d = toml_double_in(tab, "Xmax");
    if(d.ok){
        fprintf(stderr,"\t Xmax = %le\n",d.u.d);
        PlotParams.RT_Xmax = d.u.d;
    }

    d = toml_double_in(tab, "Ymin");
    if(d.ok){
        fprintf(stderr,"\t Ymin = %le\n",d.u.d);
        PlotParams.RT_Ymin = d.u.d;
    }

    d = toml_double_in(tab, "Ymax");
    if(d.ok){
        fprintf(stderr,"\t Ymax = %le\n",d.u.d);
        PlotParams.RT_Ymax = d.u.d;
    }

    d = toml_int_in(tab, "GridSize");
    if(d.ok){
        fprintf(stderr,"\t GridSize = %ld\n",d.u.i);
        PlotParams.RT_GridSize = d.u.i;
    }

    return ;
}

static void read_tomlfile_Sedov(const toml_table_t* config){

    toml_table_t* tab = toml_table_in(config, "Sedov");
    if (!tab) {
        fprintf(stderr,"missing [Sedov]\n");
        return;
    }
    fprintf(stderr,"[Sedov]\n");

    toml_datum_t d = toml_string_in(tab, "OutDir");
    if(d.ok){
        fprintf(stderr,"\t OutDir = %s\n",d.u.s);
        strncpy(PlotParams.OutDir[4],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_string_in(tab, "FileName");
    if(d.ok){
        fprintf(stderr,"\t FileName = %s\n",d.u.s);
        strncpy(PlotParams.FileName[4],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_string_in(tab, "DataDir");
    if(d.ok){
        fprintf(stderr,"\t DataDir = %s\n",d.u.s);
        strncpy(PlotParams.DataDir[4],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_int_in(tab, "PlotType");
    if(d.ok){
        fprintf(stderr,"\t PlotType = %ld\n",d.u.i);
        PlotParams.SD_PlotType = d.u.i;
    }

    d = toml_int_in(tab, "PlotSolution");
    if(d.ok){
        fprintf(stderr,"\t PlotSolution = %ld\n",d.u.i);
        PlotParams.SD_PlotSolution = d.u.i;
    }

    d = toml_string_in(tab, "SolutionFileName");
    if(d.ok){
        fprintf(stderr,"\t SolutionFileName = %s\n",d.u.s);
        strncpy(PlotParams.SD_SolutionFileName,d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_double_in(tab, "Xmin");
    if(d.ok){
        fprintf(stderr,"\t Xmin = %le\n",d.u.d);
        PlotParams.SD_Xmin = d.u.d;
    }

    d = toml_double_in(tab, "Xmax");
    if(d.ok){
        fprintf(stderr,"\t Xmax = %le\n",d.u.d);
        PlotParams.SD_Xmax = d.u.d;
    }

    d = toml_double_in(tab, "Ymin");
    if(d.ok){
        fprintf(stderr,"\t Ymin = %le\n",d.u.d);
        PlotParams.SD_Ymin = d.u.d;
    }

    d = toml_double_in(tab, "Ymax");
    if(d.ok){
        fprintf(stderr,"\t Ymax = %le\n",d.u.d);
        PlotParams.SD_Ymax = d.u.d;
    }

    d = toml_double_in(tab, "Slice");
    if(d.ok){
        fprintf(stderr,"\t Slice = %le\n",d.u.d);
        PlotParams.SD_Slice = d.u.d;
    }

    d = toml_double_in(tab, "Rmin");
    if(d.ok){
        fprintf(stderr,"\t Rmin = %le\n",d.u.d);
        PlotParams.SD_Rmin = d.u.d;
    }

    d = toml_double_in(tab, "Rmax");
    if(d.ok){
        fprintf(stderr,"\t Rmax = %le\n",d.u.d);
        PlotParams.SD_Rmax = d.u.d;
    }

    d = toml_double_in(tab, "Rhomin");
    if(d.ok){
        fprintf(stderr,"\t Rhomin = %le\n",d.u.d);
        PlotParams.SD_Rhomin = d.u.d;
    }

    d = toml_double_in(tab, "Rhomax");
    if(d.ok){
        fprintf(stderr,"\t Rhomax = %le\n",d.u.d);
        PlotParams.SD_Rhomax = d.u.d;
    }

    d = toml_double_in(tab, "Pmin");
    if(d.ok){
        fprintf(stderr,"\t Pmin = %le\n",d.u.d);
        PlotParams.SD_Pmin = d.u.d;
    }

    d = toml_double_in(tab, "Pmax");
    if(d.ok){
        fprintf(stderr,"\t Pmax = %le\n",d.u.d);
        PlotParams.SD_Pmax = d.u.d;
    }

    d = toml_int_in(tab, "GridSize");
    if(d.ok){
        fprintf(stderr,"\t GridSize = %ld\n",d.u.i);
        PlotParams.SD_GridSize = d.u.i;
    }

    return ;
}

static void read_tomlfile_Keplar(const toml_table_t* config){

    toml_table_t* tab = toml_table_in(config, "Keplar");
    if (!tab) {
        fprintf(stderr,"missing [Keplar]\n");
        return;
    }
    fprintf(stderr,"[Keplar]\n");

    toml_datum_t d = toml_string_in(tab, "OutDir");
    if(d.ok){
        fprintf(stderr,"\t OutDir = %s\n",d.u.s);
        strncpy(PlotParams.OutDir[5],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_string_in(tab, "FileName");
    if(d.ok){
        fprintf(stderr,"\t FileName = %s\n",d.u.s);
        strncpy(PlotParams.FileName[5],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_string_in(tab, "DataDir");
    if(d.ok){
        fprintf(stderr,"\t DataDir = %s\n",d.u.s);
        strncpy(PlotParams.DataDir[5],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_int_in(tab, "PlotType");
    if(d.ok){
        fprintf(stderr,"\t PlotType = %ld\n",d.u.i);
        PlotParams.KP_PlotType = d.u.i;
    }

    d = toml_double_in(tab, "Xmin");
    if(d.ok){
        fprintf(stderr,"\t Xmin = %le\n",d.u.d);
        PlotParams.KP_Xmin = d.u.d;
    }

    d = toml_double_in(tab, "Xmax");
    if(d.ok){
        fprintf(stderr,"\t Xmax = %le\n",d.u.d);
        PlotParams.KP_Xmax = d.u.d;
    }

    d = toml_double_in(tab, "Ymin");
    if(d.ok){
        fprintf(stderr,"\t Ymin = %le\n",d.u.d);
        PlotParams.KP_Ymin = d.u.d;
    }

    d = toml_double_in(tab, "Ymax");
    if(d.ok){
        fprintf(stderr,"\t Ymax = %le\n",d.u.d);
        PlotParams.KP_Ymax = d.u.d;
    }

    d = toml_int_in(tab, "GridSize");
    if(d.ok){
        fprintf(stderr,"\t GridSize = %ld\n",d.u.i);
        PlotParams.KP_GridSize = d.u.i;
    }

    return ;
}

static void read_tomlfile_Galaxy(const toml_table_t* config){

    toml_table_t* tab = toml_table_in(config, "Galaxy");
    if (!tab) {
        fprintf(stderr,"missing [Galaxy]\n");
        return;
    }
    fprintf(stderr,"[Galaxy]\n");

    toml_datum_t d = toml_string_in(tab, "OutDir");
    if(d.ok){
        fprintf(stderr,"\t OutDir = %s\n",d.u.s);
        strncpy(PlotParams.OutDir[6],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_string_in(tab, "FileName");
    if(d.ok){
        fprintf(stderr,"\t FileName = %s\n",d.u.s);
        strncpy(PlotParams.FileName[6],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_string_in(tab, "DataDir");
    if(d.ok){
        fprintf(stderr,"\t DataDir = %s\n",d.u.s);
        strncpy(PlotParams.DataDir[6],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_int_in(tab, "PlotType");
    if(d.ok){
        fprintf(stderr,"\t PlotType = %ld\n",d.u.i);
        PlotParams.GL_PlotType = d.u.i;
    }

    d = toml_double_in(tab, "Xmin");
    if(d.ok){
        fprintf(stderr,"\t Xmin = %le\n",d.u.d);
        PlotParams.GL_Xmin = d.u.d;
    }

    d = toml_double_in(tab, "Xmax");
    if(d.ok){
        fprintf(stderr,"\t Xmax = %le\n",d.u.d);
        PlotParams.GL_Xmax = d.u.d;
    }

    d = toml_double_in(tab, "Ymin");
    if(d.ok){
        fprintf(stderr,"\t Ymin = %le\n",d.u.d);
        PlotParams.GL_Ymin = d.u.d;
    }

    d = toml_double_in(tab, "Ymax");
    if(d.ok){
        fprintf(stderr,"\t Ymax = %le\n",d.u.d);
        PlotParams.GL_Ymax = d.u.d;
    }

    d = toml_double_in(tab, "Phimin");
    if(d.ok){
        fprintf(stderr,"\t Phimin = %le\n",d.u.d);
        PlotParams.GL_Phimin = d.u.d;
    }

    d = toml_double_in(tab, "Phimax");
    if(d.ok){
        fprintf(stderr,"\t Phimax = %le\n",d.u.d);
        PlotParams.GL_Phimax = d.u.d;
    }

    d = toml_double_in(tab, "Vmin");
    if(d.ok){
        fprintf(stderr,"\t Vmin = %le\n",d.u.d);
        PlotParams.GL_Vmin = d.u.d;
    }

    d = toml_double_in(tab, "Vmax");
    if(d.ok){
        fprintf(stderr,"\t Vmax = %le\n",d.u.d);
        PlotParams.GL_Vmax = d.u.d;
    }

    d = toml_int_in(tab, "GridSize");
    if(d.ok){
        fprintf(stderr,"\t GridSize = %ld\n",d.u.i);
        PlotParams.GL_GridSize = d.u.i;
    }

    d = toml_double_in(tab, "Rmin");
    if(d.ok){
        fprintf(stderr,"\t Rmin = %le\n",d.u.d);
        PlotParams.GL_Rmin = d.u.d;
    }

    d = toml_double_in(tab, "Rmax");
    if(d.ok){
        fprintf(stderr,"\t Rmax = %le\n",d.u.d);
        PlotParams.GL_Rmax = d.u.d;
    }

    d = toml_double_in(tab, "RCmin");
    if(d.ok){
        fprintf(stderr,"\t RCmin = %le\n",d.u.d);
        PlotParams.GL_RCmin = d.u.d;
    }

    d = toml_double_in(tab, "RCmax");
    if(d.ok){
        fprintf(stderr,"\t RCmax = %le\n",d.u.d);
        PlotParams.GL_RCmax = d.u.d;
    }

    d = toml_double_in(tab, "RSmin");
    if(d.ok){
        fprintf(stderr,"\t RSmin = %le\n",d.u.d);
        PlotParams.GL_RSmin = d.u.d;
    }

    d = toml_double_in(tab, "RSmax");
    if(d.ok){
        fprintf(stderr,"\t RSmax = %le\n",d.u.d);
        PlotParams.GL_RSmax = d.u.d;
    }

    return ;
}

static void read_tomlfile_Evrard(const toml_table_t* config){

    toml_table_t* tab = toml_table_in(config, "Evrard");
    if (!tab) {
        fprintf(stderr,"missing [Evrard]\n");
        return;
    }
    fprintf(stderr,"[Evrard]\n");

    toml_datum_t d = toml_string_in(tab, "OutDir");
    if(d.ok){
        fprintf(stderr,"\t OutDir = %s\n",d.u.s);
        strncpy(PlotParams.OutDir[7],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_string_in(tab, "FileName");
    if(d.ok){
        fprintf(stderr,"\t FileName = %s\n",d.u.s);
        strncpy(PlotParams.FileName[7],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_string_in(tab, "DataDir");
    if(d.ok){
        fprintf(stderr,"\t DataDir = %s\n",d.u.s);
        strncpy(PlotParams.DataDir[7],d.u.s,MaxCharactersInLine);
        free(d.u.s);
    }

    d = toml_int_in(tab, "PlotType");
    if(d.ok){
        fprintf(stderr,"\t PlotType = %ld\n",d.u.i);
        PlotParams.EV_PlotType = d.u.i;
    }

    d = toml_double_in(tab, "Xmin");
    if(d.ok){
        fprintf(stderr,"\t Xmin = %le\n",d.u.d);
        PlotParams.EV_Xmin = d.u.d;
    }

    d = toml_double_in(tab, "Xmax");
    if(d.ok){
        fprintf(stderr,"\t Xmax = %le\n",d.u.d);
        PlotParams.EV_Xmax = d.u.d;
    }

    d = toml_double_in(tab, "Ymin");
    if(d.ok){
        fprintf(stderr,"\t Ymin = %le\n",d.u.d);
        PlotParams.EV_Ymin = d.u.d;
    }

    d = toml_double_in(tab, "Ymax");
    if(d.ok){
        fprintf(stderr,"\t Ymax = %le\n",d.u.d);
        PlotParams.EV_Ymax = d.u.d;
    }

    d = toml_double_in(tab, "Rmin");
    if(d.ok){
        fprintf(stderr,"\t Rmin = %le\n",d.u.d);
        PlotParams.EV_Rmin = d.u.d;
    }

    d = toml_double_in(tab, "Rmax");
    if(d.ok){
        fprintf(stderr,"\t Rmax = %le\n",d.u.d);
        PlotParams.EV_Rmax = d.u.d;
    }

    d = toml_double_in(tab, "Rhomin");
    if(d.ok){
        fprintf(stderr,"\t Rhomin = %le\n",d.u.d);
        PlotParams.EV_Rhomin = d.u.d;
    }

    d = toml_double_in(tab, "Rhomax");
    if(d.ok){
        fprintf(stderr,"\t Rhomax = %le\n",d.u.d);
        PlotParams.EV_Rhomax = d.u.d;
    }

    d = toml_double_in(tab, "Pmin");
    if(d.ok){
        fprintf(stderr,"\t Pmin = %le\n",d.u.d);
        PlotParams.EV_Pmin = d.u.d;
    }

    d = toml_double_in(tab, "Pmax");
    if(d.ok){
        fprintf(stderr,"\t Pmax = %le\n",d.u.d);
        PlotParams.EV_Pmax = d.u.d;
    }

    d = toml_double_in(tab, "Vmin");
    if(d.ok){
        fprintf(stderr,"\t Vmin = %le\n",d.u.d);
        PlotParams.EV_Vmin = d.u.d;
    }

    d = toml_double_in(tab, "Vmax");
    if(d.ok){
        fprintf(stderr,"\t Vmax = %le\n",d.u.d);
        PlotParams.EV_Vmax = d.u.d;
    }

    d = toml_double_in(tab, "Timemin");
    if(d.ok){
        fprintf(stderr,"\t Timemin = %le\n",d.u.d);
        PlotParams.EV_Timemin = d.u.d;
    }

    d = toml_double_in(tab, "Timemax");
    if(d.ok){
        fprintf(stderr,"\t Timemax = %le\n",d.u.d);
        PlotParams.EV_Timemax = d.u.d;
    }

    d = toml_double_in(tab, "Energymin");
    if(d.ok){
        fprintf(stderr,"\t Energymin = %le\n",d.u.d);
        PlotParams.EV_Energymin = d.u.d;
    }

    d = toml_double_in(tab, "Energymax");
    if(d.ok){
        fprintf(stderr,"\t Energymax = %le\n",d.u.d);
        PlotParams.EV_Energymax = d.u.d;
    }

    d = toml_int_in(tab, "GridSize");
    if(d.ok){
        fprintf(stderr,"\t GridSize = %ld\n",d.u.i);
        PlotParams.EV_GridSize = d.u.i;
    }

    return ;
}

static void read_tomlfile(char fname[]){

    FILE *fp;
    FileOpen(fp,fname,"r");
    char errbuf[MaxCharactersInLine];
    toml_table_t* config = toml_parse_file(fp, errbuf, sizeof(errbuf));
    fclose(fp);

    if(!config){
        fprintf(stderr,"Cannot parse - %s",errbuf);
        exit(1);
    }

    toml_datum_t d = toml_string_in(config,"Title");
    if(d.ok){
        fprintf(stderr,"This is %s.\n",d.u.s);
        free(d.u.s);
    }else{
        fprintf(stderr,"missing Title\n");
    }

    read_tomlfile_Environment(config);
    read_tomlfile_Mode(config);
    read_tomlfile_Targetfiles(config);
    read_tomlfile_Shocktube(config);
    read_tomlfile_Hydrostatic(config);
    read_tomlfile_KelvinHelmholtz(config);
    read_tomlfile_RayleighTaylor(config);
    read_tomlfile_Sedov(config);
    read_tomlfile_Keplar(config);
    read_tomlfile_Galaxy(config);
    read_tomlfile_Evrard(config);

    toml_free(config);

    return ;
}


/*!
 * This function reads a parameter file of which name is defined in FileName and
 * then sets parameters.
 */
void ReadParameters(char FileName[]){

    read_tomlfile(FileName);
    MakeDir(PlotParams.OutDir[PlotParams.PlotType]);

    return ;
}

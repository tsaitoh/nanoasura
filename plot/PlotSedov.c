#include "config.h"
#include "PlantHydroTree.h"
#include "NeighborSearch.h"
#include "ReadData.h"
#include "PlotMisc.h"

bool IntroduceSedovSolution = false;
int SedovSolutionN;
double SedovSolutionXi;
double SedovSolutionRs;
double SedovSolutionD;
double *SedovSolutionR; 
double *SedovSolutionRho; 
double *SedovSolutionV; 
double *SedovSolutionP; 

static void ReadSedovSolution(void){

    if(IntroduceSedovSolution == true)
        return ;

    FILE *fp;
    FileOpen(fp,PlotParams.SD_SolutionFileName,"r");

    fscanf(fp,"%le",&SedovSolutionXi);
    fscanf(fp,"%le",&SedovSolutionRs);
    fscanf(fp,"%le",&SedovSolutionD);

    int counter = 0;
    while(fscanf(fp,"%*e %*e %*e %*e") != EOF){
        counter ++;
    }
    SedovSolutionN = counter;

    fclose(fp);
    SedovSolutionR = malloc(sizeof(double)*SedovSolutionN);
    SedovSolutionRho = malloc(sizeof(double)*SedovSolutionN);
    SedovSolutionV = malloc(sizeof(double)*SedovSolutionN);
    SedovSolutionP = malloc(sizeof(double)*SedovSolutionN);

    FileOpen(fp,PlotParams.SD_SolutionFileName,"r");
    fscanf(fp,"%*e");
    fscanf(fp,"%*e");
    fscanf(fp,"%*e");
    counter = 0;
    while(fscanf(fp,"%le %le %le %le",
                SedovSolutionR+counter,SedovSolutionV+counter,
                SedovSolutionRho+counter,SedovSolutionP+counter
                ) != EOF){
        SedovSolutionR[counter] *= SedovSolutionRs;
        SedovSolutionRho[counter] *= 4;
        SedovSolutionP[counter] 
            *= (2.0/(5.0/3.0+1.0)*1*SQ(SedovSolutionD));

        //fprintf(stderr,"%g %g\n",SedovSolutionR[counter],SedovSolutionRho[counter]);
        counter ++;
    }
    fclose(fp);
    
    IntroduceSedovSolution = true;

    return ;
}

static void PlotSDSlice(const char fname[]){

    mplc_open(0,PlotParams.PythonPath);
    mplc_screen(0,PlotParams.SD_Xmin,PlotParams.SD_Ymin,PlotParams.SD_Xmax,PlotParams.SD_Ymax,true);

    double PosX[ThisRun.NParticles];
    double PosY[ThisRun.NParticles];

    int counter = 0;
    for(int i=0;i<ThisRun.NParticles;i++){
        if(fabs(SPH[i].Pos[2]-0.5)<PlotParams.SD_Slice){
            PosX[counter] = SPH[i].Pos[0];
            PosY[counter] = SPH[i].Pos[1];
            counter ++;
        }
    }
    mplc_scatter(0,PosX,PosY,NULL,counter,"black","black","s=5, marker='o'",false,"");
    mplc_xlabel(0,"X");
    mplc_ylabel(0,"Y");

    if(PlotParams.ShowTime){
        char Time[MaxCharactersInLine];
        Snprintf(Time,"t = %1.2f ",ThisRun.TCurrent);
        fprintf(stderr,"t = %1.2f\n",ThisRun.TCurrent);
    }

    mplc_save(0,fname,"");
    mplc_close(0);

    return ;
}


static void SedovGetCenter(double PosCenter[]){

    int u_peak = 0; 
    int FixedCenterSedov = 2;

    if(FixedCenterSedov == 0){
        for(int i=1;i<ThisRun.NParticles;i++){
            if(SPH[u_peak].u < SPH[i].u)
                u_peak = i;
        }
        PosCenter[0] = SPH[u_peak].Pos[0];
        PosCenter[1] = SPH[u_peak].Pos[1];
        PosCenter[2] = SPH[u_peak].Pos[2];
    } else if(FixedCenterSedov == 1){
        PosCenter[0] = 0.5;
        PosCenter[1] = 0.5;
        PosCenter[2] = 0.5;
    } else if(FixedCenterSedov == 2){
        double u_sum = 0.e0;
        double Pos[3] = {0.e0};
        for(int i=0;i<ThisRun.NParticles;i++){
            Pos[0] += SPH[i].u*SPH[i].Pos[0];
            Pos[1] += SPH[i].u*SPH[i].Pos[1];
            Pos[2] += SPH[i].u*SPH[i].Pos[2];
            u_sum += SPH[i].u;
        }
        PosCenter[0] = Pos[0]/u_sum;
        PosCenter[1] = Pos[1]/u_sum;
        PosCenter[2] = Pos[2]/u_sum;


    } else if(FixedCenterSedov == 3){
#define SedovRhoThreshold (2.0)
        double Mass = 0.e0;
        PosCenter[0] = PosCenter[1] = PosCenter[2] = 0.e0;
        for(int i=0;i<ThisRun.NParticles;i++){
            if(SPH[i].Rho > SedovRhoThreshold){
                PosCenter[0] += SPH[i].Rho*SPH[i].Pos[0];
                PosCenter[1] += SPH[i].Rho*SPH[i].Pos[1];
                PosCenter[2] += SPH[i].Rho*SPH[i].Pos[2];
                Mass += SPH[i].Rho;
            }
        }
        if(Mass > 0.e0){
            PosCenter[0] /= Mass;
            PosCenter[1] /= Mass;
            PosCenter[2] /= Mass;
        } else {
            PosCenter[0] = PosCenter[1] = PosCenter[2] = 0.5;
        }
        fprintf(stderr,"%g %g %g\n",PosCenter[0],PosCenter[1],PosCenter[2]);
        //exit(1);

#undef SedovRhoThreshold
    }
    return ;
}

static void PlotSedovRadialDensity(const char fname[]){

    ThisRun.PeriodicBoundary = 1;
    ThisRun.Dimension = 3;
    ThisRun.LBox[0] = ThisRun.LBox[1] = ThisRun.LBox[2] = 1.0;
    mplc_open(0,PlotParams.PythonPath);
    mplc_screen(0,PlotParams.SD_Rmin,PlotParams.SD_Rhomin,PlotParams.SD_Rmax,PlotParams.SD_Rhomax,false);

    double PosCenter[3]={0.e0,0.e0,0.e0};
    SedovGetCenter(PosCenter);

    double *Radius;
    double *Rho;
    Radius = malloc(sizeof(double)*ThisRun.NParticles);
    Rho = malloc(sizeof(double)*ThisRun.NParticles);
    for(int i=0;i<ThisRun.NParticles;i++){
        Radius[i] = DISTANCE(SPH[i].Pos,PosCenter);
        Rho[i] = SPH[i].Rho;
    }

    mplc_scatter(0,Radius,Rho,NULL,ThisRun.NParticles,"black","black","s=5, marker='o'",false,"");
    mplc_xlabel(0,"X");
    mplc_ylabel(0,"Y");

    free(Radius);
    free(Rho);

    if(PlotParams.SD_PlotSolution == 1){
        mplc_plot(0,SedovSolutionR,SedovSolutionRho,SedovSolutionN,"red","solid","alpha=0.5");

        double x[2] = {SedovSolutionR[SedovSolutionN-1],SedovSolutionR[SedovSolutionN-1]};
        double y[2] = {SedovSolutionRho[SedovSolutionN-1],1};
        mplc_plot(0,x,y,2,"red","solid","alpha=0.5");
        double _x[2] = {SedovSolutionR[SedovSolutionN-1],1.0};
        double _y[2] = {1,1};
        mplc_plot(0,_x,_y,2,"red","solid","alpha=0.5");
    }

    if(PlotParams.ShowTime){
        char Time[MaxCharactersInLine];
        Snprintf(Time,"t = %1.5f",ThisRun.TCurrent);
        fprintf(stderr,"t = %1.5f\n",ThisRun.TCurrent);
    }

    mplc_xlabel(0,"Radius");
    mplc_ylabel(0,"Density");
    mplc_save(0,fname,"");
    mplc_close(0);

    return ;
}

static void PlotSedovRadialPressure(const char fname[]){

    ThisRun.PeriodicBoundary = 1;
    ThisRun.Dimension = 3;
    ThisRun.LBox[0] = ThisRun.LBox[1] = ThisRun.LBox[2] = 1.0;
    mplc_open(0,PlotParams.PythonPath);
    mplc_screen(0,PlotParams.SD_Rmin,PlotParams.SD_Pmin,PlotParams.SD_Rmax,PlotParams.SD_Pmax,false);

    double PosCenter[3]={0.e0,0.e0,0.e0};
    SedovGetCenter(PosCenter);

    double *Radius;
    double *P;
    Radius = malloc(sizeof(double)*ThisRun.NParticles);
    P = malloc(sizeof(double)*ThisRun.NParticles);
    for(int i=0;i<ThisRun.NParticles;i++){
        Radius[i] = DISTANCE(SPH[i].Pos,PosCenter);
        P[i] = SPH[i].Pressure;
    }

    mplc_scatter(0,Radius,P,NULL,ThisRun.NParticles,"black","black","s=5, marker='o'",false,"");
    mplc_xlabel(0,"X");
    mplc_ylabel(0,"Y");


    free(Radius);
    free(P);

    if(PlotParams.SD_PlotSolution == 1){
        mplc_plot(0,SedovSolutionR,SedovSolutionP,SedovSolutionN,"red","solid","alpha=0.5");
        double x[2] = {SedovSolutionR[SedovSolutionN-1],SedovSolutionR[SedovSolutionN-1]};
        double y[2] = {SedovSolutionP[SedovSolutionN-1],0};
        mplc_plot(0,x,y,2,"red","solid","alpha=0.5");
        double _x[2] = {SedovSolutionR[SedovSolutionN-1],1.0};
        double _y[2] = {0.0,0.0};
        mplc_plot(0,_x,_y,2,"red","solid","alpha=0.5");
    }

    if(PlotParams.ShowTime){
        char Time[MaxCharactersInLine];
        Snprintf(Time,"t = %1.5f",ThisRun.TCurrent);
        fprintf(stderr,"t = %1.5f\n",ThisRun.TCurrent);
    }

    mplc_xlabel(0,"Radius");
    mplc_ylabel(0,"Pressure");
    mplc_save(0,fname,"");
    mplc_close(0);

    return ;
}

static void PlotSedovGrid(const char fname[]){

    ThisRun.PeriodicBoundary = 1;
    ThisRun.Dimension = 3;
    ThisRun.LBox[0] = ThisRun.LBox[1] = ThisRun.LBox[2] = 1.0;
    mplc_open(0,PlotParams.PythonPath);
    mplc_screen(0,PlotParams.SD_Xmin,PlotParams.SD_Ymin,PlotParams.SD_Xmax,PlotParams.SD_Ymax,true);

    PlantHydroTree();

    double *GridData;
    GridData = malloc(sizeof(double)*SQ(PlotParams.SD_GridSize));

    double dX = (PlotParams.SD_Xmax-PlotParams.SD_Xmin)/(double)PlotParams.SD_GridSize;
    double dY = (PlotParams.SD_Ymax-PlotParams.SD_Ymin)/(double)PlotParams.SD_GridSize;
    double H_init = dX/(double)PlotParams.SD_GridSize;
    for(int i=0;i<PlotParams.SD_GridSize;i++){
        for(int k=0;k<PlotParams.SD_GridSize;k++){
            double Pos[3]; 
            Pos[0] = dX*(i+0.5)+PlotParams.SD_Xmin;
            Pos[1] = dY*(k+0.5)+PlotParams.SD_Ymin;
            Pos[2] = 0.5;
            GridData[i+PlotParams.SD_GridSize*k] = CalcGridDensity(Pos,&H_init,3);
        }
    }

    mplc_send_command(0,"extent = 0,1,0,1\n");
    mplc_imshow(0,GridData,PlotParams.SD_GridSize,"extent=extent,vmin=0.9,vmax=4.1,interpolation='nearest'",true,"label='Density'");
    free(GridData);

    mplc_xlabel(0,"X");
    mplc_ylabel(0,"Y");
    mplc_save(0,fname,"");
    mplc_close(0);

    return ;
}

void PlotSedov(const int FileID){

    if(!ReadData(FileID)){
        return ;
    }

    char FileNameWrite[MaxCharactersInLine];
    if(PlotParams.SD_PlotType == 0){
        Snprintf(FileNameWrite,"%s_dot",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.SD_PlotType == 1){
        Snprintf(FileNameWrite,"%s_Rho",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.SD_PlotType == 2){
        Snprintf(FileNameWrite,"%s_P",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.SD_PlotType == 3){
        Snprintf(FileNameWrite,"%s_Grid",PlotParams.FileName[PlotParams.PlotType]);
    }

    char fname[MaxCharactersInLine];
    if(PlotParams.WriteMode == 0){
        Snprintf(fname,"./%s/%s.%04d.pdf",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    } else if(PlotParams.WriteMode == 1){
        Snprintf(fname,"./%s/%s.%04d.eps",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    } else if(PlotParams.WriteMode == 2){
        Snprintf(fname,"./%s/%s.%04d.png",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    }

    ReadSedovSolution();

    if(PlotParams.SD_PlotType == 0){
        PlotSDSlice(fname);
    } else if (PlotParams.SD_PlotType == 1){
        PlotSedovRadialDensity(fname);
    } else if (PlotParams.SD_PlotType == 2){
        PlotSedovRadialPressure(fname);
    } else if (PlotParams.SD_PlotType == 3){
        PlotSedovGrid(fname);
    }

    return ;
}

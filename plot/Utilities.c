#include "config.h"
#include <sys/stat.h>

/*!
 * This function returns "true" if the given file exists. Otherewise, it returns
 * "false".
 */
bool CheckFile(const char FileName[]){
    FILE *fp;
    fp = fopen(FileName,"r");
    if(fp == NULL){
        return false;
    }
    fclose(fp);
    return true;
}

/*!
 * This function returns "true" if the given directory exists. Otherewise, it returns
 * "false".
 */
bool CheckDir(const char DirName[]){
    FILE *fp;
    fp = fopen(DirName,"r");
    if(fp == NULL){
        return false;
    }
    fclose(fp);
    return true;
}

/*!
 * This function generate a direcory of the given path. If this function fails
 * to generate the directory, it calls exit(EXIT_FAILURER).
 */
void MakeDir(const char DirName[]){

    if(CheckDir(DirName) == false){
        int checkflag = mkdir(DirName,0755);
        if(checkflag < 0){
            fprintf(stderr,"Directory [%s] creation error.\n",DirName);
            exit(EXIT_FAILURE);
        } else {
            fprintf(stderr,"Directory [%s] is created.\n",DirName);
        }
    } 

    return ;
}

/*!
 * This function returns "true" when the each string which is obtained from *src
 * splited by *delimiter has *word. 
 */
bool FindWordFromString(char *src, char *word, char *delimiter){

    char src_copy[MaxCharactersInLine];
    strcpy(src_copy,src);
    char *tok;
    tok = strtok(src_copy,delimiter);
    while(tok != NULL){
        //dbg("%s\n",tok);
        if(strcmp(tok,word) == 0)
            return true;
        tok = strtok(NULL,delimiter);
    }
    return false;
}

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/* matplotlib-c.c */
void mplc_set_debug_flag(const bool flag);
void mplc_send_command(const int device_id, const char *s);
void mplc_set_layout(const int device_id, const char *c);
bool mplc_open(const int device_id, const char *python_name);
bool mplc_close(const int device_id);
void mplc_subplots(const int device_id, const char *c);
void mplc_screen(const int device_id, const double x1, const double y1, const double x2, const double y2, const bool EqualAspect);
void mplc_line(const int device_id, double x1, double y1, double x2, double y2, const char *color, const char *opt);
void mplc_plot(const int device_id, const double x[], const double y[], const int number, const char *color, const char *linestyle, const char *opt);
void mplc_step(const int device_id, const double x[], const double y[], const int number, const char *color, const char *linestyle, const char *where, const char *opt);
void mplc_fill_between(const int device_id, const double x[], const double y1[], const double y2[], const int number, const char *opt);
void mplc_point(const int device_id, double x, double y, const char *color, const char *opt);
void mplc_points(const int device_id, const double x[], const double y[], const int number, const char *color, const char *opt);
void mplc_scatter(const int device_id, const double x[], const double y[], const double c[], const int number, const char *color, const char *facecolor, const char *opt, const bool colorbar, const char *opt_colorbar);
void mplc_rect(const int device_id, double x1, double y1, double x2, double y2, const char *edgecolor, const char *facecolor, const char *opt);
void mplc_ellipse(const int device_id, double cx, double cy, double rx, double ry, const char *edgecolor, const char *facecolor, const char *opt);
void mplc_circle(const int device_id, double cx, double cy, double r, const char *edgecolor, const char *facecolor, const char *opt);
void mplc_polygon(const int device_id, const double *x, const double *y, const int number, const char *edgecolor, const char *facecolor, const char *opt);
void mplc_bar(const int device_id, const double x[], const double y[], const int number, const double width, const char *edgecolor, const char *facecolor, const char *opt);
void mplc_stackplot(const int device_id, const int number, const int groups, const double x[], const double y[][number],  const char *opt);
void mplc_contour(const int device_id, const double x[], const double y[], const double z[], const int number, const char *opt, const bool colorbar, const char *opt_colorbar);
void mplc_contourf(const int device_id, const double x[], const double y[], const double z[], const int number, const char *opt, const bool colorbar, const char *opt_colorbar);
void mplc_hexbin(const int device_id, const double x[], const double y[], const int number, const int gridsize, const char *opt, const bool colorbar, const char *opt_colorbar);
void mplc_imshow(const int device_id, const double x[], const int gridsize, const char *opt, const bool colorbar, const char *opt_colorbar);
void mplc_pcolormesh(const int device_id, const int xbin_size, const double xbin[], const int ybin_size, const double ybin[], const double z[], const char *opt, const bool colorbar, const char *opt_colorbar);
void mplc_quiverplot(const int device_id, const int nrank, const double x[], const double y[], const double u[], const double v[], const char *opt);
void mplc_streamplot(const int device_id, const int nrank, const double x[], const double y[], const double u[], const double v[], const double color[], const char *opt, const bool colorbar, const char *opt_colorbar);
void mplc_hist(const int device_id, const int size, const double x[], const int bins, const char *opt);
void mplc_hist2d(const int device_id, const int size, const double x[], const double y[], const int bin_x, const int bin_y, const char *opt, const bool colorbar, const char *opt_colorbar);
void mplc_show(const int device_id, const char *opt);
void mplc_save(const int device_id, const char *filename, const char *opt);
void mplc_clear(const int device_id);
void mplc_title(const int device_id, const char *c);
void mplc_xlabel(const int device_id, const char *c);
void mplc_ylabel(const int device_id, const char *c);
void mplc_colorbar(const int device_id);
void mplc_grid(const int device_id, const bool flag);
void mplc_legend(const int device_id, const char *c);
void mplc_text(const int device_id, const double x, const double y, const char *c, const char *opt);

void mplc_open_multipages(const int device_id, const char *python_name, const char *filename);
void mplc_close_multipages(const int device_id);
void mplc_open_onepage(const int device_id);
void mplc_close_onepage(const int device_id);

#ifdef __cplusplus
}
#endif

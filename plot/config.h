#pragma once 

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>
#include <tgmath.h>
#include <assert.h>
#include "Macros.h"
#include "Constants.h"
#include "Utilities.h"
#include "Vars.h"
#include "PeriodicWrapping.h"
#include "matplotlib-c.h"
#ifdef USE_OPENMP
#include "omp.h"
#endif


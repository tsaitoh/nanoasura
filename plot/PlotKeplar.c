#include "config.h"
#include "PlantHydroTree.h"
#include "NeighborSearch.h"
#include "ReadData.h"
#include "PlotMisc.h"

static int KP_MarkerIndex = 0;
static void SetKPMarkerIndex(void){

    static bool FirstCall_SetKPMarkerIndex = true;
    if(FirstCall_SetKPMarkerIndex == true){
        FirstCall_SetKPMarkerIndex = false;
    } else {
        return ;
    }

    if(!ReadData(0)){
        return ;
    }

    double TargetPos[] = {1.e0,0.e0,0.e0};

    double r = DISTANCE2(TargetPos,SPH[0].Pos);
    for(int i=1;i<ThisRun.NParticles;i++){
        double r_tmp = DISTANCE2(TargetPos,SPH[i].Pos);
        if(r>r_tmp){
            r = r_tmp;
            KP_MarkerIndex = i;
        }
    }

    return ;
}

static void PlotKPPoints(const char fname[]){

    mplc_open(0,PlotParams.PythonPath);
    mplc_screen(0,PlotParams.KP_Xmin,PlotParams.KP_Ymin,PlotParams.KP_Xmax,PlotParams.KP_Ymax,true);

    double *PosX,*PosY;
    PosX = malloc(sizeof(double)*ThisRun.NParticles);
    PosY = malloc(sizeof(double)*ThisRun.NParticles);

    for(int i=0;i<ThisRun.NParticles;i++){
        PosX[i] = SPH[i].Pos[0];
        PosY[i] = SPH[i].Pos[1];
    }
    mplc_scatter(0,PosX,PosY,NULL,ThisRun.NParticles,"black","grey","s=2, marker='o'",false,"");

    if(PlotParams.ShowTime){
        char Time[MaxCharactersInLine];
        Snprintf(Time,"t = %1.2f",ThisRun.TCurrent);
        fprintf(stderr,"t = %1.2f\n",ThisRun.TCurrent);
    }

    mplc_xlabel(0,"X");
    mplc_ylabel(0,"Y");
    mplc_save(0,fname,"");
    mplc_close(0);

    return ;
}

static void PlotKPGrid(const char fname[]){

    ThisRun.PeriodicBoundary = 0;
    ThisRun.Dimension = 2;

    mplc_open(0,PlotParams.PythonPath);
    mplc_screen(0,PlotParams.KP_Xmin,PlotParams.KP_Ymin,PlotParams.KP_Xmax,PlotParams.KP_Ymax,true);

    PlantHydroTree();

    double *GridData;
    GridData = malloc(sizeof(double)*SQ(PlotParams.KP_GridSize));

    double dX = (PlotParams.KP_Xmax-PlotParams.KP_Xmin)/(double)PlotParams.KP_GridSize;
    double dY = (PlotParams.KP_Ymax-PlotParams.KP_Ymin)/(double)PlotParams.KP_GridSize;
    double H_init = 10*dX/(double)PlotParams.KP_GridSize;
    for(int i=0;i<PlotParams.KP_GridSize;i++){
        for(int k=0;k<PlotParams.KP_GridSize;k++){
            double Pos[3]; 
            Pos[0] = dX*(i+0.5)+PlotParams.KP_Xmin;
            Pos[1] = dY*(k+0.5)+PlotParams.KP_Ymin;
            Pos[2] = 0.5;
            GridData[i+PlotParams.KP_GridSize*k] = CalcGridDensity(Pos,&H_init,2);
        }
    }

    char opt[MaxCharactersInLine];
    Snprintf(opt,"extent = %g,%g,%g,%g\n",
            PlotParams.KP_Xmin,PlotParams.KP_Xmax,PlotParams.KP_Ymin,PlotParams.KP_Ymax);
    mplc_send_command(0,opt);
    mplc_send_command(0,"from matplotlib.colors import LogNorm\n");
    mplc_imshow(0,GridData,PlotParams.KP_GridSize,"extent=extent,norm=LogNorm(vmin=1e-9,vmax=1e-7)",true,"label='log Density'");

    free(GridData);

    mplc_xlabel(0,"X");
    mplc_ylabel(0,"Y");

    mplc_save(0,fname,"");
    mplc_close(0);

    return ;
}

void PlotKeplar(const int FileID){

    SetKPMarkerIndex();

    if(!ReadData(FileID)){
        return ;
    }

    char FileNameWrite[MaxCharactersInLine];
    if(PlotParams.KP_PlotType == 0){
        Snprintf(FileNameWrite,"%s_dot",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.KP_PlotType == 1){
        Snprintf(FileNameWrite,"%s_Grid",PlotParams.FileName[PlotParams.PlotType]);
    }

    char fname[MaxCharactersInLine];
    if(PlotParams.WriteMode == 0){
        Snprintf(fname,"./%s/%s.%04d.pdf",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    } else if(PlotParams.WriteMode == 1){
        Snprintf(fname,"./%s/%s.%04d.eps",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    } else if(PlotParams.WriteMode == 2){
        Snprintf(fname,"./%s/%s.%04d.png",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    }

    if(PlotParams.KP_PlotType == 0){
        PlotKPPoints(fname);
    } else if(PlotParams.KP_PlotType == 1){
        PlotKPGrid(fname);
    }

    return ;
}

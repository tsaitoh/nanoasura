#include "config.h"
#include "PlantHydroTree.h"
#include "NeighborSearch.h"
#include "ReadData.h"
#include "PlotMisc.h"

static void PlotKH(const char fname[]){

    mplc_open(0,PlotParams.PythonPath);
    mplc_screen(0,PlotParams.KH_Xmin,PlotParams.KH_Ymin,PlotParams.KH_Xmax,PlotParams.KH_Ymax,true);

    if(PlotParams.KH_PlotType == 0){
        double PosX[ThisRun.NParticles];
        double PosY[ThisRun.NParticles];

        // Plot Tag0
        int counter = 0;
        for(int i=0;i<ThisRun.NParticles;i++){
            if(SPH[i].Tag == 0){
                PosX[counter] = SPH[i].Pos[0];
                PosY[counter] = SPH[i].Pos[1];
                counter ++;
            }
        }
        mplc_scatter(0,PosX,PosY,NULL,counter,"darkred","red","s=5, marker='o'",false,"");

        // Plot Tag1
        counter = 0;
        for(int i=0;i<ThisRun.NParticles;i++){
            if(SPH[i].Tag == 1){
                PosX[counter] = SPH[i].Pos[0];
                PosY[counter] = SPH[i].Pos[1];
                counter ++;
            }
        }
        mplc_scatter(0,PosX,PosY,NULL,counter,"darkblue","blue","s=5, marker='o'",false,"");

    } else if (PlotParams.KH_PlotType == 1){
        double PosX[ThisRun.NParticles];
        double PosY[ThisRun.NParticles];
        double Color[ThisRun.NParticles];
        for(int i=0;i<ThisRun.NParticles;i++){
            PosX[i] = SPH[i].Pos[0];
            PosY[i] = SPH[i].Pos[1];
            Color[i] = SPH[i].Rho;
        }
        mplc_scatter(0,PosX,PosY,Color,ThisRun.NParticles,"black","","linewidth=0, s=20, marker='o',vmin=0.9,vmax=2.1",true,"label='Density'");
    } else if (PlotParams.KH_PlotType == 2){
        double PosX[ThisRun.NParticles];
        double PosY[ThisRun.NParticles];
        double Color[ThisRun.NParticles];
        for(int i=0;i<ThisRun.NParticles;i++){
            PosX[i] = SPH[i].Pos[0];
            PosY[i] = SPH[i].Pos[1];
            Color[i] = SPH[i].Pressure;
        }
        mplc_scatter(0,PosX,PosY,Color,ThisRun.NParticles,"black","","linewidth=0, s=20, marker='o', vmin=2.0, vmax=3.0",true,"label='Density'");
    } else if (PlotParams.KH_PlotType == 3){
        ThisRun.PeriodicBoundary = 1;
        ThisRun.Dimension = 2;
        ThisRun.LBox[0] = ThisRun.LBox[1] = ThisRun.LBox[2] = 1.0;

        PlantHydroTree();

        double *PosX;
        double *PosY;
        double *GridData;
        PosX = malloc(sizeof(double)*PlotParams.KH_GridSize);
        PosY = malloc(sizeof(double)*PlotParams.KH_GridSize);
        GridData = malloc(sizeof(double)*PlotParams.KH_GridSize*PlotParams.KH_GridSize);

        double dX = (PlotParams.KH_Xmax-PlotParams.KH_Xmin)/(double)PlotParams.KH_GridSize;
        double dY = (PlotParams.KH_Ymax-PlotParams.KH_Ymin)/(double)PlotParams.KH_GridSize;

        for(int i=0;i<PlotParams.KH_GridSize;i++){
            PosX[i] = dX*(i+0.5)+PlotParams.KH_Xmin;
            PosY[i] = dY*(i+0.5)+PlotParams.KH_Ymin;
        }

        double H_init = (PlotParams.KH_Xmax-PlotParams.KH_Xmin)/(double)PlotParams.KH_GridSize;
        for(int i=0;i<PlotParams.KH_GridSize;i++){
            for(int k=0;k<PlotParams.KH_GridSize;k++){
                double Pos[3]; 
                Pos[0] = dX*(i+0.5)+PlotParams.KH_Xmin;
                Pos[1] = dY*(k+0.5)+PlotParams.KH_Ymin;
                Pos[2] = 0.5;
                GridData[k*PlotParams.KH_GridSize+i] = CalcGridDensity(Pos,&H_init,ThisRun.Dimension);
            }
        }

        char opt[MaxCharactersInLine];
        Snprintf(opt,"extent = %g,%g,%g,%g\n",
                PlotParams.KH_Xmin,PlotParams.KH_Xmax,PlotParams.KH_Ymin,PlotParams.KH_Ymax);
        mplc_send_command(0,opt);
        mplc_imshow(0,GridData,PlotParams.KH_GridSize,"extent=extent,vmin=0.9,vmax=2.1,interpolation='nearest'",true,"label='Density'");

        free(PosX);
        free(PosY);
        free(GridData);

    }

    if(PlotParams.ShowTime){
        char Time[MaxCharactersInLine];
        Snprintf(Time,"t = %1.2f \\gt\\dKH\\u",ThisRun.TCurrent/ThisRun.TNorm);
        fprintf(stderr,"t = %1.2f \n",ThisRun.TCurrent/ThisRun.TNorm);
    }

    mplc_xlabel(0,"X");
    mplc_ylabel(0,"Y");
    mplc_save(0,fname,"");
    mplc_close(0);

    return ;
}

void PlotKelvinHelmholtz(const int FileID){

    if(!ReadData(FileID)){
        return ;
    }

    char FileNameWrite[MaxCharactersInLine];
    if(PlotParams.KH_PlotType == 0){
        Snprintf(FileNameWrite,"%s_dot",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.KH_PlotType == 1){
        Snprintf(FileNameWrite,"%s_Rho",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.KH_PlotType == 2){
        Snprintf(FileNameWrite,"%s_P",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.KH_PlotType == 3){
        Snprintf(FileNameWrite,"%s_Grid",PlotParams.FileName[PlotParams.PlotType]);
    }


    char fname[MaxCharactersInLine];
    if(PlotParams.WriteMode == 0){
        Snprintf(fname,"./%s/%s.%04d.pdf",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    } else if(PlotParams.WriteMode == 1){
        Snprintf(fname,"./%s/%s.%04d.eps",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    } else if(PlotParams.WriteMode == 2){
        Snprintf(fname,"./%s/%s.%04d.png",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    }

    PlotKH(fname);

    return ;
}

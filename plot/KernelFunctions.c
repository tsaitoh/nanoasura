#include "config.h"

double (*SPHKernel)(const double, const double);
double (*dSPHKernel)(const double, const double);

/*!
 * This is the cubic spline kernel function.
 */
static double SPHKernelB2Spline(const double r, const double Kernel){

    static const double coef1d = 2.0/3.0;
    static const double coef2d = 10.0/(7.0*M_PI);
    static const double coef3d = M_1_PI;

    double InvKernel = 1.0/Kernel;
    double u = r*InvKernel;

    double coef;
    if(ThisRun.Dimension == 1){
        coef = coef1d*InvKernel;
    } else if(ThisRun.Dimension == 2){
        coef = coef2d*SQ(InvKernel);
    } else if(ThisRun.Dimension == 3){
        coef = coef3d*CUBE(InvKernel);
    } else {
        coef = 0.e0;
    } 

    if(u<1.e0){
        return (coef*(1.e0-1.5*SQ(u)+0.75*CUBE(u)));
    } else if(u<2.e0){
        return (coef*(0.25*CUBE(2.e0-u)));
    } else {
        return 0.e0;
    }
}

/*!
 * This fucntion returns the spatial derivation of the spline kernel.  The
 * returned value is 
 *    \sigma(\nu)    d W_ij(u)
 *    -----------  x -------- .
 *    h^{\nu+2}*u       du
 */
static double dSPHKernelB2Spline(const double r, const double Kernel){

    if(!(r>0.e0))
        return (0.e0);

    static const double coef1d = 2.0/3.0;
    static const double coef2d = 10.0/(7.0*M_PI);
    static const double coef3d = M_1_PI;

    double InvKernel = 1.0/Kernel;
    double u = r*InvKernel;
 
    double coef;
    if(ThisRun.Dimension == 1){
        coef = coef1d*CUBE(InvKernel);
    }else if(ThisRun.Dimension == 2){
        coef = coef2d*SQ(SQ(InvKernel));
    }else if(ThisRun.Dimension == 3){
        coef = coef3d*CUBE(InvKernel)*SQ(InvKernel);
    } else {
        coef = 0.e0;
    }

    if(u<1.e0){
        return (-coef*(3.e0-2.25*u));
    } else if(u<2.e0){
        return (-coef*(0.75*SQ(2.e0-u))/u);
    } else {
        return (0.e0);
    }
}

/*!
 * This fucntion returns the spatial derivation of the spline kernel.  The
 * returned value is 
 *    \sigma(\nu)    d W_ij(u)
 *    -----------  x -------- .
 *    h^{\nu+2}*u       du
 * To suppress the pairing instablity, a cuspy profile is adopted at u < 2/3.
 */
static double dSPHKernelB2SplineTC92(const double r, const double Kernel){

    if(!(r>0.e0))
        return (0.e0);
 
    static const double coef1d = 2.0/3.0;
    static const double coef2d = 10.0/(7.0*M_PI);
    static const double coef3d = M_1_PI;

    double InvKernel = 1.0/Kernel;
    double u = r*InvKernel;

    double coef;
    if(ThisRun.Dimension == 1){
        coef = coef1d*CUBE(InvKernel);
    }else if(ThisRun.Dimension == 2){
        coef = coef2d*SQ(SQ(InvKernel));
    }else if(ThisRun.Dimension == 3){
        coef = coef3d*CUBE(InvKernel)*SQ(InvKernel);
    } else {
        coef = 0.e0;
    }

    if(3.0*u<2.0){
        return (-coef/u);
    } else if(u<1.e0){
        return (-coef*(3.e0-2.25*u));
    } else if(u<2.e0){
        return (-coef*(0.75*SQ(2.e0-u))/u);
    } else {
        return (0.e0);
    }
}

#define WENDLANDC2_1D_COEF (1.620185)
#define WENDLANDC2_2D_COEF (1.897367)
#define WENDLANDC2_3D_COEF (1.936492)

static double SPHKernelWendlandC2(const double r, const double Kernel){

    if(ThisRun.Dimension == 1){
        static const double coef1d = 5.0/4.0;
        static const double F_H = WENDLANDC2_1D_COEF;
        double InvH = 1.0/(F_H*Kernel);
        double u = r*InvH;
        double coef = coef1d*InvH;
        return coef*fmax(0.0,CUBE(1.0-u))*(1.0+3.0*u);
    }else if(ThisRun.Dimension == 2){
        static const double coef2d = 7.0*M_1_PI;
        static const double F_H = WENDLANDC2_2D_COEF;
        double InvH = 1.0/(F_H*Kernel);
        double u = r*InvH;
        double coef = coef2d*SQ(InvH);
        return coef*fmax(0.0,SQ(SQ(1.0-u)))*(1.0+4.0*u);
    }else if(ThisRun.Dimension == 3){
        static const double coef3d = 10.5*M_1_PI;
        static const double F_H = WENDLANDC2_3D_COEF;
        double InvH = 1.0/(F_H*Kernel);
        double u = r*InvH;
        double coef = coef3d*CUBE(InvH);
        return coef*fmax(0.0,SQ(SQ(1.0-u)))*(1.0+4.0*u);
    } else {
        return 0.e0;
    }
}

static double dSPHKernelWendlandC2(const double r, const double Kernel){

    if(ThisRun.Dimension == 1){
        static const double coef1d = 15.0; // 12*5/4
        static const double F_H = WENDLANDC2_1D_COEF;
        double InvH = 1.0/(F_H*Kernel);
        double u = r*InvH;
        double coef = coef1d*CUBE(InvH);
        return -coef*fmax(0.0,SQ(1.0-u));
    }else if(ThisRun.Dimension == 2){
        static const double coef2d = 140.0*M_1_PI; // 20*7/pi
        static const double F_H = WENDLANDC2_2D_COEF;
        double InvH = 1.0/(F_H*Kernel);
        double u = r*InvH;
        double coef = coef2d*SQ(SQ(InvH));
        return -coef*fmax(0.0,CUBE(1.0-u));
    }else if(ThisRun.Dimension == 3){
        static const double coef3d = 210.0*M_1_PI; // 20*10.5/pi
        static const double F_H = WENDLANDC2_3D_COEF;
        double InvH = 1.0/(F_H*Kernel);
        double u = r*InvH;
        double coef = coef3d*SQ(InvH)*CUBE(InvH);
        return -coef*fmax(0.0,CUBE(1.0-u));
    } else {
        return 0.e0;
    }
}

#define WENDLANDC4_1D_COEF (1.936492)
#define WENDLANDC4_2D_COEF (2.171239)
#define WENDLANDC4_3D_COEF (2.207940)

static double SPHKernelWendlandC4(const double r, const double Kernel){

    if(ThisRun.Dimension == 1){
        static const double coef1d = 1.5;
        static const double F_H = WENDLANDC4_1D_COEF;
        double InvH = 1.0/(F_H*Kernel);
        double u = r*InvH;
        double coef = coef1d*InvH;
        return coef*fmax(0.0,CUBE(1.0-u)*SQ(1.0-u))*(1.0+5.0*u+8.0*SQ(u));
    }else if(ThisRun.Dimension == 2){
        static const double coef2d = 9.0*M_1_PI;
        static const double F_H = WENDLANDC4_2D_COEF;
        double InvH = 1.0/(F_H*Kernel);
        double u = r*InvH;
        double coef = coef2d*SQ(InvH);
        return coef*fmax(0.0,SQ(CUBE(1.0-u)))*(1.0+6.0*u+11.6666666666667*SQ(u)); //35/3 = 11.6666666666667
    }else if(ThisRun.Dimension == 3){
        static const double coef3d = 15.46875*M_1_PI; // 495.0/32.0 = 15.46875
        static const double F_H = WENDLANDC4_3D_COEF;
        double InvH = 1.0/(F_H*Kernel);
        double u = r*InvH;
        double coef = coef3d*CUBE(InvH);
        return coef*fmax(0.0,SQ(CUBE(1.0-u)))*(1.0+6.0*u+11.6666666666667*SQ(u)); //35/3 = 11.6666666666667
    } else {
        return 0.e0;
    }
}

static double dSPHKernelWendlandC4(const double r, const double Kernel){

    if(ThisRun.Dimension == 1){
        static const double coef1d = 21.0; // 14*3/2
        static const double F_H = WENDLANDC4_1D_COEF;
        double InvH = 1.0/(F_H*Kernel);
        double u = r*InvH;
        double coef = coef1d*CUBE(InvH);
        return -coef*fmax(0.0,SQ(SQ(1.0-u)))*(1.0+4.0*u);
    }else if(ThisRun.Dimension == 2){
        static const double coef2d = 168.0*M_1_PI; // (56/3)*9/pi
        static const double F_H = WENDLANDC4_2D_COEF;
        double InvH = 1.0/(F_H*Kernel);
        double u = r*InvH;
        double coef = coef2d*SQ(SQ(InvH));
        return -coef*fmax(0.0,SQ(1.0-u)*CUBE(1.0-u))*(1.0+5.0*u);
    }else if(ThisRun.Dimension == 3){
        static const double coef3d = 866.25*M_1_PI; // 56*495.0/32.0/pi
        static const double F_H = WENDLANDC4_3D_COEF;
        double InvH = 1.0/(F_H*Kernel);
        double u = r*InvH;
        double coef = coef3d*CUBE(InvH)*SQ(InvH);
        return -coef*fmax(0.0,SQ(1.0-u)*CUBE(1.0-u))*(1.0+5.0*u);
    } else {
        return 0.e0;
    }
}

#define WENDLANDC6_1D_COEF (2.207940)
#define WENDLANDC6_2D_COEF (2.415230)
#define WENDLANDC6_3D_COEF (2.449490)

static double SPHKernelWendlandC6(const double r, const double Kernel){

    if(ThisRun.Dimension == 1){
        static const double coef1d = 55.0/32.0;
        static const double F_H = WENDLANDC6_1D_COEF;
        double InvH = 1.0/(F_H*Kernel);
        double u = r*InvH;
        double coef = coef1d*InvH;
        return coef*fmax(0.0,CUBE(SQ(1.0-u))*SQ(1.0-u))*(1.0+7.0*u+19.0*SQ(u)+21*CUBE(u));
    }else if(ThisRun.Dimension == 2){
        static const double coef2d = (78.0/7.0)*M_1_PI;
        static const double F_H = WENDLANDC6_2D_COEF;
        double InvH = 1.0/(F_H*Kernel);
        double u = r*InvH;
        double coef = coef2d*SQ(InvH);
        return coef*fmax(0.0,SQ(SQ(SQ(1.0-u))))*(1.0+8.0*u+25.0*SQ(u)+32*CUBE(u)); 
    }else if(ThisRun.Dimension == 3){
        static const double coef3d = (1365.0/64.0)*M_1_PI; // 495.0/32.0 = 15.46875
        static const double F_H = WENDLANDC6_3D_COEF;
        double InvH = 1.0/(F_H*Kernel);
        double u = r*InvH;
        double coef = coef3d*CUBE(InvH);
        return coef*fmax(0.0,SQ(SQ(SQ(1.0-u))))*(1.0+8.0*u+25.0*SQ(u)+32*CUBE(u)); 
    } else {
        return 0.e0;
    }
}

static double dSPHKernelWendlandC6(const double r, const double Kernel){

    if(ThisRun.Dimension == 1){
        static const double coef1d = 55.0/32.0; 
        static const double F_H = WENDLANDC6_1D_COEF;
        double InvH = 1.0/(F_H*Kernel);
        double u = r*InvH;
        double coef = coef1d*CUBE(InvH);
        return -coef*fmax(0.0,SQ(CUBE(1.0-u)))*(18.0+110.0*u+208*SQ(u));
    }else if(ThisRun.Dimension == 2){
        static const double coef2d = 22*(78.0/7.0)*M_1_PI;
        static const double F_H = WENDLANDC6_2D_COEF;
        double InvH = 1.0/(F_H*Kernel);
        double u = r*InvH;
        double coef = coef2d*SQ(SQ(InvH));
        return -coef*fmax(0.0,SQ(SQ(1.0-u))*CUBE(1.0-u))*(1.0+7.0*u+16*SQ(u));
    }else if(ThisRun.Dimension == 3){
        static const double coef3d = 22*(1365.0/64.0)*M_1_PI; 
        static const double F_H = WENDLANDC6_3D_COEF;
        double InvH = 1.0/(F_H*Kernel);
        double u = r*InvH;
        double coef = coef3d*CUBE(InvH)*SQ(InvH);
        return -coef*fmax(0.0,SQ(SQ(1.0-u))*CUBE(1.0-u))*(1.0+7.0*u+16*SQ(u));
    } else {
        return 0.e0;
    }
}

/*!
 * Select kernel function following the ThisRun.SelectKernelType option.
 */
void InitKernelFunction(void){

    if(ThisRun.SelectKernelType == 0){
        SPHKernel = SPHKernelB2Spline;
        dSPHKernel = dSPHKernelB2Spline;
    } else if(ThisRun.SelectKernelType == 1){
        SPHKernel = SPHKernelB2Spline;
        dSPHKernel = dSPHKernelB2SplineTC92;
    } else if(ThisRun.SelectKernelType == 2){ // Wendland C2 Kernel
        SPHKernel = SPHKernelWendlandC2;
        dSPHKernel = dSPHKernelWendlandC2;
    } else if(ThisRun.SelectKernelType == 3){ // Wendland C4 Kernel
        SPHKernel = SPHKernelWendlandC4;
        dSPHKernel = dSPHKernelWendlandC4;
    } else if(ThisRun.SelectKernelType == 4){ // Wendland C6 Kernel
        SPHKernel = SPHKernelWendlandC6;
        dSPHKernel = dSPHKernelWendlandC6;
    }
    

    return ;
}

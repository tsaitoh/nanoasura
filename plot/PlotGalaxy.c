#include "config.h"
#include "PlantHydroTree.h"
#include "NeighborSearch.h"
#include "ReadData.h"
#include "PlotMisc.h"

static void PlotGLPoints(const char fname[]){

    mplc_open(0,PlotParams.PythonPath);
    mplc_screen(0,PlotParams.GL_Xmin,PlotParams.GL_Ymin,PlotParams.GL_Xmax,PlotParams.GL_Ymax,true);

    double *PosX,*PosY;
    PosX = malloc(sizeof(double)*ThisRun.NParticles);
    PosY = malloc(sizeof(double)*ThisRun.NParticles);

    for(int i=0;i<ThisRun.NParticles;i++){
        PosX[i] = SPH[i].Pos[0];
        PosY[i] = SPH[i].Pos[1];
    }
    mplc_scatter(0,PosX,PosY,NULL,ThisRun.NParticles,"darkred","red","linewidth=0, s=2, marker='o'",false,"");

    if(PlotParams.ShowTime){
        char Time[MaxCharactersInLine];
        Snprintf(Time,"t = %1.2f",ThisRun.TCurrent);
        fprintf(stderr,"t = %1.2f\n",ThisRun.TCurrent);
    }

    mplc_xlabel(0,"X [kpc]");
    mplc_ylabel(0,"Y [kpc]");
    mplc_save(0,fname,"");
    mplc_close(0);

    return ;
}

static void PlotGLGrid(const char fname[]){

    ThisRun.Dimension = 2;

    mplc_open(0,PlotParams.PythonPath);
    mplc_screen(0,PlotParams.GL_Xmin,PlotParams.GL_Ymin,PlotParams.GL_Xmax,PlotParams.GL_Ymax,true);

    PlantHydroTree();

    double *GridData;
    GridData = malloc(sizeof(double)*PlotParams.GL_GridSize*PlotParams.GL_GridSize);

    double dX = (PlotParams.GL_Xmax-PlotParams.GL_Xmin)/(double)PlotParams.GL_GridSize;
    double dY = (PlotParams.GL_Ymax-PlotParams.GL_Ymin)/(double)PlotParams.GL_GridSize;
    double H_init = (PlotParams.GL_Xmax-PlotParams.GL_Xmin)/(double)PlotParams.GL_GridSize;
    for(int i=0;i<PlotParams.GL_GridSize;i++){
        for(int k=0;k<PlotParams.GL_GridSize;k++){
            double Pos[3]; 
            Pos[0] = dX*(i+0.5)+PlotParams.GL_Xmin;
            Pos[1] = dY*(k+0.5)+PlotParams.GL_Ymin;
            Pos[2] = 0.5;
            GridData[i+PlotParams.GL_GridSize*k] = CalcGridDensity(Pos,&H_init,2);
        }
    }

    char opt[MaxCharactersInLine];
    Snprintf(opt,"extent = %g,%g,%g,%g\n",
            PlotParams.GL_Xmin,PlotParams.GL_Xmax,PlotParams.GL_Ymin,PlotParams.GL_Ymax);
    mplc_send_command(0,opt);
    mplc_imshow(0,GridData,PlotParams.GL_GridSize,"extent=extent,interpolation='nearest',vmin=1.e-6,vmax=2.5e-5",true,"label='Density'");

    free(GridData);

    mplc_xlabel(0,"X [kpc]");
    mplc_ylabel(0,"Y [kpc]");

    mplc_save(0,fname,"");
    mplc_close(0);

    return ;
}

static double dRing = 0.5;
static double SolarPosition[] = {0,-8,0};

static void PlotGLPointsPV(const char fname[]){

    mplc_open(0,PlotParams.PythonPath);
    mplc_screen(0,PlotParams.GL_Phimin,PlotParams.GL_Vmin,PlotParams.GL_Phimax,PlotParams.GL_Vmax,true);

    double *PosX,*PosY;
    PosX = malloc(sizeof(double)*ThisRun.NParticles);
    PosY = malloc(sizeof(double)*ThisRun.NParticles);

    double Rsolar = sqrt(SQ(SolarPosition[0])+SQ(SolarPosition[1]));

    // Compute LOS
    int counter = 0;
    double SolarVel[2] = {0.e0,0.e0};
    for(int i=0;i<ThisRun.NParticles;i++){
        double R = sqrt(SQ(SPH[i].Pos[0])+SQ(SPH[i].Pos[1]));

        if(R < Rsolar-dRing) continue;
        if(R > Rsolar+dRing) continue;

        double Vr = (SPH[i].Vel[0]*SPH[i].Pos[0]+SPH[i].Vel[1]*SPH[i].Pos[1])/R;
        double Vtheta = sqrt(SQ(SPH[i].Vel[0])+SQ(SPH[i].Vel[1])-SQ(Vr));

        SolarVel[0] += Vtheta;
        counter ++;
    }
    SolarVel[0] /= (double)counter;
    SolarVel[1] /= (double)counter;

    double vfact = 1.0/((1.e+5/3.086e+21)/(1.0/3.15576e+15));
    for(int i=0;i<ThisRun.NParticles;i++){
        double Pos[] = {SPH[i].Pos[0]-SolarPosition[0],SPH[i].Pos[1]-SolarPosition[1],0};
        double Vel[] = {SPH[i].Vel[0]-SolarVel[0],SPH[i].Vel[1]-SolarVel[1],0};

        double phi = (180.0/M_PI)*(atan2(Pos[1],Pos[0]))-90;
        if(phi > +180) phi -= 360;
        if(phi < -180) phi += 360;
        PosX[i] = phi;
        PosY[i] = vfact*(Vel[0]*Pos[0]+Vel[1]*Pos[1])/NORM(Pos);
    }


    mplc_scatter(0,PosX,PosY,NULL,ThisRun.NParticles,"darkred","red","s=5, marker='o'",false,"");

    if(PlotParams.ShowTime){
        char Time[MaxCharactersInLine];
        Snprintf(Time,"t = %1.2f",ThisRun.TCurrent);
        fprintf(stderr,"t = %1.2f\n",ThisRun.TCurrent);
    }

    mplc_xlabel(0,"Angle [deg]");
    mplc_ylabel(0,"Velocity [km/s]");
    mplc_save(0,fname,"");
    mplc_close(0);

    return ;
}

static void PlotGLGridPV(const char fname[]){

    PlantHydroTree();

    mplc_open(0,PlotParams.PythonPath);
    mplc_screen(0,PlotParams.GL_Phimin,PlotParams.GL_Vmin,PlotParams.GL_Phimax,PlotParams.GL_Vmax,false);


    double *GridData;
    GridData = malloc(sizeof(double)*PlotParams.GL_GridSize*PlotParams.GL_GridSize);
    for(int i=0;i<PlotParams.GL_GridSize*PlotParams.GL_GridSize;i++)
        GridData[i] = 0.e0;

    double dPhi = (PlotParams.GL_Phimax-PlotParams.GL_Phimin)/(double)PlotParams.GL_GridSize;
    double dV = (PlotParams.GL_Vmax-PlotParams.GL_Vmin)/(double)PlotParams.GL_GridSize;

    // Compute LOS
    int counter = 0;
    double Rsolar = sqrt(SQ(SolarPosition[0])+SQ(SolarPosition[1]));
    double SolarVel[2] = {0.e0,0.e0};
    for(int i=0;i<ThisRun.NParticles;i++){
        double R = sqrt(SQ(SPH[i].Pos[0])+SQ(SPH[i].Pos[1]));

        if(R < Rsolar-dRing) continue;
        if(R > Rsolar+dRing) continue;

        double Vr = (SPH[i].Vel[0]*SPH[i].Pos[0]+SPH[i].Vel[1]*SPH[i].Pos[1])/R;
        double Vtheta = sqrt(SQ(SPH[i].Vel[0])+SQ(SPH[i].Vel[1])-SQ(Vr));

        SolarVel[0] += Vtheta;
        counter ++;
    }
    SolarVel[0] /= (double)counter;
    SolarVel[1] /= (double)counter;

    double vfact = 1.0/((1.e+5/3.086e+21)/(1.0/3.15576e+15));

    for(int i=0;i<ThisRun.NParticles;i++){
        double Pos[] = {SPH[i].Pos[0]-SolarPosition[0],SPH[i].Pos[1]-SolarPosition[1],0};
        double Vel[] = {SPH[i].Vel[0]-SolarVel[0],SPH[i].Vel[1]-SolarVel[1],0};
        double phi = (180.0/M_PI)*(atan2(Pos[1],Pos[0]))-90;
        if(phi > +180) phi -= 360;
        if(phi < -180) phi += 360;
        int IndexX = (phi-PlotParams.GL_Phimin)/dPhi;
        double Vlos = vfact*(Vel[0]*Pos[0]+Vel[1]*Pos[1])/NORM(Pos);
        int IndexV = (Vlos-PlotParams.GL_Vmin)/dV;

        if(IndexX<0) continue;
        if(IndexX>=PlotParams.GL_GridSize) continue;
        if(IndexV<0) continue;
        if(IndexV>=PlotParams.GL_GridSize) continue;

        GridData[IndexX+PlotParams.GL_GridSize*IndexV] ++;
    }
    for(int i=0;i<PlotParams.GL_GridSize*PlotParams.GL_GridSize;i++)
        GridData[i] /= (double)ThisRun.NParticles;

    char opt[MaxCharactersInLine];
    Snprintf(opt,"extent = %g,%g,%g,%g\n",
            PlotParams.GL_Phimin,PlotParams.GL_Phimax,PlotParams.GL_Vmin,PlotParams.GL_Vmax);
    mplc_send_command(0,opt);
    mplc_send_command(0,"from matplotlib.colors import LogNorm\n");
    mplc_imshow(0,GridData,PlotParams.GL_GridSize,"extent=extent,interpolation='nearest',norm=LogNorm(vmin=1e-5,vmax=1e-2),aspect=0.7",true,"label='log Number Density'");

    free(GridData);

    if(PlotParams.ShowTime){
        char Time[MaxCharactersInLine];
        Snprintf(Time,"t = %1.2f",ThisRun.TCurrent);
        fprintf(stderr,"t = %1.2f\n",ThisRun.TCurrent);
    }

    mplc_xlabel(0,"Angle [deg]");
    mplc_ylabel(0,"Velocity [km/s]");

    mplc_save(0,fname,"");
    mplc_close(0);

    return ;
}

static void PlotGLRotationCurve(const char fname[]){

    FILE *fp;
    char fname_rc[MaxCharactersInLine];
    Snprintf(fname_rc,"%s/rc.dat",PlotParams.DataDir[6]);
    FileOpen(fp,fname_rc,"r");
    int counter = 0;
    while(fscanf(fp,"%*e %*e %*e %*e %*e %*e")!=EOF){
        counter ++;
    }
    double R[counter],Vel[counter];
    rewind(fp);
    
    counter = 0;
    while(fscanf(fp,"%le %le %*e %*e %*e %*e",R+counter,Vel+counter)!=EOF){
        counter ++;
    }
    fclose(fp);


    mplc_open(0,PlotParams.PythonPath);
    mplc_screen(0,PlotParams.GL_Rmin,PlotParams.GL_RCmin,PlotParams.GL_Rmax,PlotParams.GL_RCmax,false);
    mplc_plot(0,R,Vel,counter,"red","solid","alpha=0.5");

    mplc_xlabel(0,"Distance [kpc]");
    mplc_ylabel(0,"Velocity [km/s]");
    mplc_save(0,fname,"");
    mplc_close(0);

    exit(EXIT_SUCCESS);

    return ;
}

static void PlotGLRotationSpeed(const char fname[]){

    FILE *fp;
    char fname_rc[MaxCharactersInLine];
    Snprintf(fname_rc,"%s/rc.dat",PlotParams.DataDir[6]);
    FileOpen(fp,fname_rc,"r");
    int counter = 0;
    while(fscanf(fp,"%*e %*e %*e %*e %*e %*e")!=EOF){
        counter ++;
    }
    double R[counter],Omega[counter],OmegaP[counter],OmegaM[counter],OmegaB[counter];
    rewind(fp);
    
    counter = 0;
    while(fscanf(fp,"%le %*e %le %le %le %le",R+counter,Omega+counter,OmegaP+counter,OmegaM+counter,OmegaB+counter)!=EOF){
        counter ++;
    }
    fclose(fp);

    mplc_open(0,PlotParams.PythonPath);
    mplc_screen(0,PlotParams.GL_Rmin,PlotParams.GL_RSmin,PlotParams.GL_Rmax,PlotParams.GL_RSmax,false);

    mplc_plot(0,R,Omega,counter,"black","solid","");
    mplc_plot(0,R,OmegaP,counter,"red","solid","");
    mplc_plot(0,R,OmegaM,counter,"blue","solid","");
    mplc_plot(0,R,OmegaB,counter,"green","solid","");

    mplc_xlabel(0,"Distance [kpc]");
    mplc_ylabel(0,"Angular velocity [km/s/kpc]");
    mplc_save(0,fname,"");
    mplc_close(0);

    exit(EXIT_SUCCESS);

    return ;
}

void PlotGalaxy(const int FileID){

    if(!ReadData(FileID)){
        return ;
    }

    char FileNameWrite[MaxCharactersInLine];
    if(PlotParams.GL_PlotType == 0){
        Snprintf(FileNameWrite,"%s_dot",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.GL_PlotType == 1){
        Snprintf(FileNameWrite,"%s_map_rho",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.GL_PlotType == 2){
        Snprintf(FileNameWrite,"%s_dot_pv",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.GL_PlotType == 3){
        Snprintf(FileNameWrite,"%s_map_pv",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.GL_PlotType == 10){
        Snprintf(FileNameWrite,"%s_rotation_curve",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.GL_PlotType == 11){
        Snprintf(FileNameWrite,"%s_rotation_speed",PlotParams.FileName[PlotParams.PlotType]);
    }

    char fname[MaxCharactersInLine];
    if(PlotParams.WriteMode == 0){
        Snprintf(fname,"./%s/%s.%04d.pdf",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    } else if(PlotParams.WriteMode == 1){
        Snprintf(fname,"./%s/%s.%04d.eps",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    } else if(PlotParams.WriteMode == 2){
        Snprintf(fname,"./%s/%s.%04d.png",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    }

    if(PlotParams.GL_PlotType == 0){
        PlotGLPoints(fname);
    } else if(PlotParams.GL_PlotType == 1){
        PlotGLGrid(fname);
    } else if(PlotParams.GL_PlotType == 2){
        PlotGLPointsPV(fname);
    } else if(PlotParams.GL_PlotType == 3){
        PlotGLGridPV(fname);
    } else if(PlotParams.GL_PlotType == 10){
        PlotGLRotationCurve(fname);
    } else if(PlotParams.GL_PlotType == 11){
        PlotGLRotationSpeed(fname);
    }

    return ;
}

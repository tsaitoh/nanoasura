#include "config.h"
#include "NeighborSearch.h"
#include "PlantHydroTree.h"

/*!
 * This function returns the number of particles included in a spherical volume
 * of which coordinate center is (Pos[0],Pos[1],Pos[2]) and the radius is "2h".
 * The tree method is used.
 */
int GetNeighborNumber(double Pos[restrict], const double h){

	double hh = h*h;
	int nlist = 0;
    int RootNodeID = 0;
    int CurrentNodeID = HydroNode[RootNodeID].Children;
	while(CurrentNodeID != RootNodeID){

        double dx2 = 0.e0;
        if(ThisRun.PeriodicBoundary == 1){
            for(int k=0;k<ThisRun.Dimension;k++)
                dx2 += SQ(PeriodicDistance(Pos[k],HydroNode[CurrentNodeID].Pos[k],k));
        } else {
            dx2 = DISTANCE2(HydroNode[CurrentNodeID].Pos,Pos);
        }

        if( dx2 > SQ(h+HydroNode[CurrentNodeID].DistanceMax) ){
			CurrentNodeID = HydroNode[CurrentNodeID].Next;
		} else if(HydroNode[CurrentNodeID].Children != NONE){		
			CurrentNodeID = HydroNode[CurrentNodeID].Children;
		} else {
			int NumberofLeaves = HydroNode[CurrentNodeID].NumberofLeaves;
            int header = HydroNode[CurrentNodeID].Leaves;
            for(int k=0;k<NumberofLeaves;k++){
                int leaf = header+k;
                double distance2 = 0.e0;
                if(ThisRun.PeriodicBoundary == 1){
                    for(int l=0;l<ThisRun.Dimension;l++)
                        distance2 += SQ(PeriodicDistance(Pos[l],NBCache[leaf].Pos[l],l));
                } else {
                    distance2 = DISTANCE2(NBCache[leaf].Pos,Pos);
                }
                if(distance2 < hh){
                    nlist ++;
                }
			}
			CurrentNodeID = HydroNode[CurrentNodeID].Next;
		}
	}
	return nlist;
}

/*!
 * This function returns the number of particles and the index list included in
 * a spherical volume of which coordinate center is (Pos[0],Pos[1],Pos[2]) and
 * the radius is "2h".  The tree method is used.
 */
int GetNeighbors(double Pos[restrict], const double h, int list[restrict]){

	double hh = h*h;
	int nlist = 0;

    int RootNodeID = 0;
    int CurrentNodeID = HydroNode[RootNodeID].Children;
	while(CurrentNodeID != RootNodeID){

        double dx2 = 0.e0;
        if(ThisRun.PeriodicBoundary == 1){
            for(int k=0;k<ThisRun.Dimension;k++)
                dx2 += SQ(PeriodicDistance(Pos[k],HydroNode[CurrentNodeID].Pos[k],k));
        } else {
            dx2 = DISTANCE2(HydroNode[CurrentNodeID].Pos,Pos);
        }

        if( dx2 > SQ(h+HydroNode[CurrentNodeID].DistanceMax) ){
			CurrentNodeID = HydroNode[CurrentNodeID].Next;
		} else if(HydroNode[CurrentNodeID].Children != NONE){		
			CurrentNodeID = HydroNode[CurrentNodeID].Children;
		} else {
			int NumberofLeaves = HydroNode[CurrentNodeID].NumberofLeaves;
            int header = HydroNode[CurrentNodeID].Leaves;
            for(int k=0;k<NumberofLeaves;k++){
                int leaf = header+k;
                double distance2 = 0.e0;
                if(ThisRun.PeriodicBoundary == 1){
                    for(int l=0;l<ThisRun.Dimension;l++)
                        distance2 += SQ(PeriodicDistance(Pos[l],NBCache[leaf].Pos[l],l));
                } else {
                    distance2 = DISTANCE2(NBCache[leaf].Pos,Pos);
                }
                if(distance2 < hh){
                    list[nlist] = NBCache[leaf].Leaf;
                    nlist ++;
                }
			}
			CurrentNodeID = HydroNode[CurrentNodeID].Next;
		}
	}
	return nlist;
}

/*!
 * This function returns the number of particles and the index list
 * included (1) in a spherical volume of which coordinate center is
 * (Pos[0],Pos[1],Pos[2]) and the radius is "2h" and (2) in interaction
 * regions of other particles (spherical regions of which center is
 * (SPH[nbindex].Pos[0],SPH[nbindex].Pos[1],SPH[nbindex].Pos[2]) and the radius
 * is "2*SPH[nbindex].Kernel".  The tree method is used.
 */
int GetNeighborsPairs(double Pos[restrict], const double h, int list[restrict]){

    double hh = h*h;
    int nlist = 0;

    int RootNodeID = 0;
    int CurrentNodeID = HydroNode[RootNodeID].Children;

	while(CurrentNodeID != RootNodeID){
        double dx2 = 0.e0;
        if(ThisRun.PeriodicBoundary == 1){
            for(int k=0;k<ThisRun.Dimension;k++)
                dx2 += SQ(PeriodicDistance(Pos[k],HydroNode[CurrentNodeID].Pos[k],k));
        } else {
            dx2 = DISTANCE2(HydroNode[CurrentNodeID].Pos,Pos);
        }

        if( (dx2 > SQ(h+HydroNode[CurrentNodeID].DistanceMax))&&(dx2>SQ(HydroNode[CurrentNodeID].KernelMax)) ){
            CurrentNodeID = HydroNode[CurrentNodeID].Next;
        } else if(HydroNode[CurrentNodeID].Children != NONE){
            CurrentNodeID = HydroNode[CurrentNodeID].Children;
        } else {
			int NumberofLeaves = HydroNode[CurrentNodeID].NumberofLeaves;
            int header = HydroNode[CurrentNodeID].Leaves;
            for(int k=0;k<NumberofLeaves;k++){
                int leaf = header+k;
                double distance2 = 0.e0;
                if(ThisRun.PeriodicBoundary == 1){
                    for(int l=0;l<ThisRun.Dimension;l++)
                        distance2 += SQ(PeriodicDistance(Pos[l],NBCache[leaf].Pos[l],l));
                } else {
                    distance2 = DISTANCE2(NBCache[leaf].Pos,Pos);
                }
                if( (distance2<hh) || (distance2<4.0*SQ(NBCache[leaf].Kernel)) ){
                    list[nlist] = NBCache[leaf].Leaf;
                    nlist ++;
                }
            }
            CurrentNodeID = HydroNode[CurrentNodeID].Next;
        }
    }
    return nlist;
}


/*!
 * This function returns the number of particles and the index list included in
 * a spherical volume of which coordinate center is (Pos[0],Pos[1],Pos[2]) and
 * the radius is "2h". Direct evaluation is used. Thus, the computational cost
 * is \f$N^2\f$.
 */
int GetNeighborsDirect(double Pos[restrict], const double h, int list[restrict]){

	double hh = h*h;
	int nlist = 0;
	for(int i=0;i<ThisRun.NParticles;i++){
        double r2 = 0.e0;
        if(ThisRun.PeriodicBoundary == 1){
            for(int k=0;k<ThisRun.Dimension;k++)
                r2 += SQ(PeriodicDistance(Pos[k],SPH[i].Pos[k],k));
        } else {
		    r2 = DISTANCE2(Pos,SPH[i].Pos);
        }

		if(r2 <= hh){
			list[nlist] = i;
			nlist ++;
		}
	}

	return nlist;
}

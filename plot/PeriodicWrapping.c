#include "config.h"

/*!
 * This function returns the distance between x1 and x2.  The periodic boundary
 * condition is imposed.
 */
double PeriodicDistance(const double x1, const double x2, const int dim){
    double X = x1-x2;
    if(ThisRun.PeriodicBoundary == 1){
        if(X>0.5*ThisRun.LBox[dim]){
            X -= ThisRun.LBox[dim];
        } else if(X<-0.5*ThisRun.LBox[dim]){
            X += ThisRun.LBox[dim];
        }
    }

    return X;
}

void PeriodicWrappingi(double Pos[restrict]){

    if(Pos[0] >= ThisRun.LBox[0]) Pos[0] -= ThisRun.LBox[0];
    if(Pos[0] < 0.e0)             Pos[0] += ThisRun.LBox[0];
    if(ThisRun.Dimension == 1) return ;

    if(Pos[1] >= ThisRun.LBox[1]) Pos[1] -= ThisRun.LBox[1];
    if(Pos[1] < 0.e0)             Pos[1] += ThisRun.LBox[1];
    if(ThisRun.Dimension == 2) return ;

    if(Pos[2] >= ThisRun.LBox[2]) Pos[2] -= ThisRun.LBox[2];
    if(Pos[2] < 0.e0)             Pos[2] += ThisRun.LBox[2];
    return;
}

void PeriodicWrapping(void){

    if(ThisRun.ShowLog){
        fprintf(stderr,"Impose periodic boundary... ");
    }
    if(ThisRun.PeriodicBoundary == 1){
        for(int i=0;i<ThisRun.NParticles;i++){
            PeriodicWrappingi(SPH[i].Pos);
        }
    }

    if(ThisRun.ShowLog){
        fprintf(stderr,"finish.\n");
    }
    return;
}


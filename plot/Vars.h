#pragma once

/*! @struct StructThisRun
 * A structure which contains all of run status.
 */
extern struct StructThisRun{

    int NParticles; //<! Number of particles in the run. 
    int RunType;  //<! Select type of simulation. 0:1D shocktube, 1:2D hydrostatic test, 2:2D Kelvin-Helmholtz instability test, 3:2D Rayleigh-Taylor instability test, 4:3D point like explosion test.
    char OutDir[MaxCharactersInLine];   //<! Data output directory.
    char RunName[MaxCharactersInLine];  //<! Run name.
    // int SelectICType;

    int Dimension;        //<! Dimension.
    int PeriodicBoundary; //<! Periodic boundary condition flag. If this flag is 1, the periodic consition is imposed.
    double LBox[3];       //<! Simulation box size for the run with the periodic boundary condition.

    double dt;       //!< Timestep.
    double TCurrent; //!< Current time.
    double TEnd;     //!< End time.
    int NStep;       //!< Total steps.
    double TNorm;    
    double dt_output; //!< Output timestep.
    double CFL;       //!< Coefficient for the CFL timestep.

    int UseDISPH;              //!< This flag controls the type of SPH. If the value is 1, DISPH is adopted. If it is 0, Standard SPH is used.
    int SelectKernelType;      //!< The kernel type can be selected by this parameter. The values of 0, 1, 2, 3 corresponds to the cubic spline kernel, Wendland C2, C4, and C6 kernels, respectively.
    int KernelEvaluationType;  //!<
    double KernelEta;          //!<
    int UseGradh;              //!< 
    int UseGradN;              //!< 

    double Gamma;              //!< Specific heat ratio \f$\gamma\f$.
    double Gm1;                //!< \f$\gamma-1\f$.
    double GGm1;               //!< \f$\gamma(\gamma-1)\f$.

    int Ns;    //!< Number of neighbor partilces; \f$N_{\rm s}\f$.
    int Nspm;  //!< Tolerance of the neighbor particles; \f$N_{\rm s} \pm N_{\rm spm}\f$.
    int NBmin; //!< Minimum number of the neighbor particles; \f$N_{\rm s}-N_{\rm spm}\f$.
    int NBmax; //!< Maximum number of the neighbor particles; \f$N_{\rm s}+N_{\rm spm}\f$.

    // int ViscType;   // input param.
    // int ViscDensityType;   // input param.
    double ViscAlpha;          //!< Viscosity coefficient.
    // Handcode double ViscEta2;           //!< input param.
    int ViscBalsara;           //!< Balsara limiter flag. If this flag is 1, the run uses the Balsara limiter.
    double SignalVelocityBeta; //!< A coefficient, \f$\beta\f$, for the signal velocity. The definition of the signal velocity between particles \f$i\f$ and \f$j\f$ is \f$v_{{\rm sig},ij} = c_{\rm i} +c_{\rm j} -\beta \boldmath r \cdot \boldmath v/|r|\f$, where \f$c\f$ is the sound speed, \f$r\f$ and \f$v\f$ are the distance and relative velocity between particles \f$i\f$ and \f$j\f$, respectively.

    int VariableAlpha;                //!< Variable viscosity factor flag.
    double VariableAlphaMin;          //!< Minimum value of viscosity factor.
    double VariableAlphaMax;          //!< Maximum value of viscosity factor.
    double VariableAlphaDecayFactor;  //!< Decay term coefficient for the viscosity factor.
    double VariableAlphaSourceFactor; //!< Source term coefficient for the viscosity factor.
    // int NumberofInitAlpha;
    // char InitAlphaArray[MaxCharactersInLine]; // input param.
    // double *InitAlpha;

    int OutputFileNumber;  //!< Number of output file.
    int WriteEveryStep;    //!< If this flag is 1, the log files are written every timestep. "OutputFileNumber" is ignored.
    int ShowLog;           //!< Verbose mode flag. If this flag is 1, you can see logs during the simulation.

    double Tstart;         //!< The beginning time of this run.
    double Tfin;           //!< The finishing time of this run

    int ThreadNumber;   //!< Thread number for OpenMP parallelization.
    int DynamicChunk;   //!< Chunk size of OpenMP parallelization.
} ThisRun; 

/*! @struct StructSPH
 * A structure which contains all information of a SPH particle
 */
extern struct StructSPH{
    int Nlist;         //!< Number of neighbor particles
    double Pos[3];     //!< Position
    double Vel[3];     //!< Velocity
    double Velh[3];    //!< Velocity at the intermediate step
    double Acc[3];     //!< Acceleration
    double Pot;        //!< Potential
    double Mass;       //!< Mass
    double Eps;        //!< Gravitational softening length
    double Rho;        //!< Density evaluated by \f$\rho=\sum_j m_j W(r_{ij},h_i)\f$
    double Pressure;   //!< Pressure
    double Kernel;     //!< Kernel size
    double q;          //!< Energy Density used in the DISPH mode. The definition is \f$q=\sum_j U_j W(r_{ij},h_i)\f$
    double DivV;          //!< Divergence of velocity
    double RotV[3];       //!< Rotation of velocity
    double F;             //!< Balsara limiter
    double Gradh;         //!< Grad-h term
    double NumberDensity; //!< Number density defined by \f$n=\sum_j W(r_{ij},h_i)\f$
    double GradN;         //!< Grad-N term
    double Vsig;          //!< Signal velocity
    double u;             //!< Specific internal energy
    double uh;            //!< Specific internal energy at the intermediate step
    double du;            //!< Time derivative of the specific internal energy
    double Alpha;         //!< Viscosity factor used in the variable alpha mode
    int Tag;              //!< Tag
} *SPH;


/*! @struct StructHydroRoot
 * A structure which contains all information of the root node of the tree
 * structure
 */
extern struct StructHydroRoot{
    int NumberofLeaves;         //!< Current number of used nodes
    int NumberofAllocatedLeaves;//!< Maximum allocated memory for *Leaves
    int *Leaves;                //!< Leaves, which is the morton-ordered local particles index list
    int NumberofNodes;             //!< Whole allocated nodes for this tree
    int NumberofAllocatedNodes;    //!< Number of allocated nodes for this tree
    int NumberofNodeCreationLimit; // Maximum number of particles for the lowest node

    short CurrentMaxLevel;         // The maximum level of this tree. 
    short MaxLevel;                // Allowing maximum level of this tree. 

    double PosMax[3];       //<! Maximum corner of particle distribution. 
    double PosMin[3];       //<! Minimum corner of particle distribution. 
    double Width;           //<! Width of the current tree node
} HydroRoot;

/*! @struct StructHydroNode
 * A structure which contains all information of each node of the tree
 * structure
 */
extern struct StructHydroNode{
    int Next;
    int Parent;
    int Children;
    int Sister;

    short Level;              // Level of node in belonging tree structure. 
    short NumberofChildren;   // Number of child-node in this node.
    int Leaves;               // This member points a Pbody or Phydro in the top of the node.
    int NumberofLeaves;       // Number of particles in this node.

    double  Pos[3];           // Center of this node.
    double  KernelMax;        // The maximum length of kernels.
    double  DistanceMax;      // The maximum distance between particles and the center of this node.
} *HydroNode;

/*! @struct StructNBCache
 * A structure which contains all information of each node of the tree
 * structure
 */
extern struct StructNBCache{
    double Pos[3];            //!< Position of particle
    double Kernel;            //!< Kernel size of particle
    int  Leaf;                //!< ID to refer the original particle
} *NBCache;


/*! @struct StructPlotParams
 * A structure which contains all of run status.
 */
extern struct StructPlotParams{

    char PythonPath[MaxCharactersInLine];   //<! path to Python3.x
    char OutDir[MaxModelNumber][MaxCharactersInLine];
    char DataDir[MaxModelNumber][MaxCharactersInLine];
    char FileName[MaxModelNumber][MaxCharactersInLine];

    int WriteMode;

    int FileStart;
    int FileEnd;
    int ShowTime;

    int PlotType;

    // Plot Type 0
    int ST_PlotType;
    int ST_PlotSolution;
    char ST_SolutionFileName[MaxCharactersInLine];
    double ST_Xmin;
    double ST_Xmax;
    double ST_Ymin;
    double ST_Ymax;
    double ST_Rhomin;
    double ST_Rhomax;
    double ST_Pmin;
    double ST_Pmax;
    double ST_Umin;
    double ST_Umax;
    double ST_Vmin;
    double ST_Vmax;

    // For PlotType 1
    int HS_PlotType;
    int HS_Color[2];
    int HS_Symbol[2];
    double HS_Xmin;
    double HS_Xmax;
    double HS_Ymin;
    double HS_Ymax;
    int HS_GridSize;

    // For PlotType 2
    int KH_PlotType;
    int KH_Color[2];
    int KH_Symbol[2];
    double KH_Xmin;
    double KH_Xmax;
    double KH_Ymin;
    double KH_Ymax;
    int KH_GridSize;

    // For PlotType 3
    int RT_PlotType;
    int RT_UseCopy;
    int RT_Color[3];
    int RT_Symbol[3];
    double RT_Xmin;
    double RT_Xmax;
    double RT_Ymin;
    double RT_Ymax;
    int RT_GridSize;


    // For PlotType 4
    int SD_PlotType;
    int SD_PlotSolution;
    char SD_SolutionFileName[MaxCharactersInLine];
    double SD_Xmin;
    double SD_Xmax;
    double SD_Ymin;
    double SD_Ymax;
    double SD_Slice;
    double SD_Rmin;
    double SD_Rmax;
    double SD_Rhomin;
    double SD_Rhomax;
    double SD_Pmin;
    double SD_Pmax;
    int SD_GridSize;

    // For PlotType 5
    int KP_PlotType;
    double KP_Xmin;
    double KP_Xmax;
    double KP_Ymin;
    double KP_Ymax;
    int KP_GridSize;

    // For PlotType 6
    int GL_PlotType;
    double GL_Xmin;
    double GL_Xmax;
    double GL_Ymin;
    double GL_Ymax;
    double GL_Phimin;
    double GL_Phimax;
    double GL_Vmin;
    double GL_Vmax;
    int GL_GridSize;
    double GL_Rmin;
    double GL_Rmax;
    double GL_RCmin;
    double GL_RCmax;
    double GL_RSmin;
    double GL_RSmax;

    // For PlotType 7
    int EV_PlotType;
    double EV_Xmin;
    double EV_Xmax;
    double EV_Ymin;
    double EV_Ymax;
    double EV_Rmin;
    double EV_Rmax;
    double EV_Rhomin;
    double EV_Rhomax;
    double EV_Pmin;
    double EV_Pmax;
    double EV_Vmin;
    double EV_Vmax;
    double EV_Timemin;
    double EV_Timemax;
    double EV_Energymin;
    double EV_Energymax;
    int EV_GridSize;

} PlotParams;



#pragma once 

bool CheckFile(const char FileName[]);
bool CheckDir(const char DirName[]);
void MakeDir(const char DirName[]);
bool FindWordFromString(char *src, char *word, char *delimiter);

#include "config.h"
#include "ReadData.h"
#include "PlotMisc.h"

int STSolutionN = 0;
double *STSolutionPos;
double *STSolutionVel;
double *STSolutionRho;
double *STSolutionP;
double *STSolutionU;

static void STReadAnalyticalSolution(void){

    if(STSolutionN>0) return;

    FILE *fp;
    FileOpen(fp,PlotParams.ST_SolutionFileName,"r");
    STSolutionN = 0;
    while(fscanf(fp,"%*e %*e %*e %*e %*e") != EOF){
        STSolutionN ++;
    }
    fclose(fp);

    STSolutionPos = malloc(sizeof(double)*STSolutionN);
    STSolutionVel = malloc(sizeof(double)*STSolutionN);
    STSolutionRho = malloc(sizeof(double)*STSolutionN);
    STSolutionP   = malloc(sizeof(double)*STSolutionN);
    STSolutionU   = malloc(sizeof(double)*STSolutionN);
    
    FileOpen(fp,PlotParams.ST_SolutionFileName,"r");
    int counter = 0;
    while(fscanf(fp,"%le %le %le %le %le",
                STSolutionPos+counter,
                STSolutionVel+counter,
                STSolutionRho+counter,
                STSolutionP+counter,
                STSolutionU+counter) != EOF){
        STSolutionPos[counter] += 1.0;
        counter ++;
    }
    fclose(fp);

    return ;
}

static void PlotST(const char *fname){

    int NumebrofDataType = 4;
    double PosX[ThisRun.NParticles],Data[NumebrofDataType][ThisRun.NParticles];
    for(int i=0;i<ThisRun.NParticles;i++){
        PosX[i] = SPH[i].Pos[0];
        for(int k=0;k<NumebrofDataType;k++){
            if(k==0){
                Data[k][i] = SPH[i].Rho;
            } else if(k==1){
                //Data[k][i] = ThisRun.Gm1*SPH[i].Rho*SPH[i].u;
                Data[k][i] = SPH[i].Pressure;
            } else if(k==2){
                Data[k][i] = SPH[i].u;
            } else if(k==3){
                Data[k][i] = SPH[i].Vel[0];
            }
        }
    }

    if(PlotParams.ST_PlotType == 4){

        mplc_set_layout(0,"2,2");
        mplc_open(0,PlotParams.PythonPath);

        int counter = 0;
        for(int j=0;j<2;j++){
            for(int i=0;i<2;i++){
                if(counter == 0){
                    mplc_subplots(0,"0,0");
                    mplc_screen(0,PlotParams.ST_Xmin,PlotParams.ST_Rhomin,PlotParams.ST_Xmax,PlotParams.ST_Rhomax,true);
                } else if(counter == 1){
                    mplc_subplots(0,"1,0");
                    mplc_screen(0,PlotParams.ST_Xmin,PlotParams.ST_Pmin,PlotParams.ST_Xmax,PlotParams.ST_Pmax,true);
                } else if(counter == 2){
                    mplc_subplots(0,"0,1");
                    mplc_screen(0,PlotParams.ST_Xmin,PlotParams.ST_Umin,PlotParams.ST_Xmax,PlotParams.ST_Umax,true);
                } else if(counter == 3){
                    mplc_subplots(0,"1,1");
                    mplc_screen(0,PlotParams.ST_Xmin,PlotParams.ST_Vmin,PlotParams.ST_Xmax,PlotParams.ST_Vmax,true);
                }

                mplc_scatter(0,PosX,Data[counter],NULL,ThisRun.NParticles,"black","black","s=10, marker='o'",false,"");
                if(PlotParams.ST_PlotSolution == 1){
                    if(counter == 0){
                        mplc_plot(0,STSolutionPos,STSolutionRho,STSolutionN,"red","solid","linewidth=2, alpha=0.5");
                    } else if(counter == 1){
                        mplc_plot(0,STSolutionPos,STSolutionP,STSolutionN,"red","solid","linewidth=2, alpha=0.5");
                    } else if(counter == 2){
                        mplc_plot(0,STSolutionPos,STSolutionU,STSolutionN,"red","solid","linewidth=2, alpha=0.5");
                    } else if(counter == 3){
                        mplc_plot(0,STSolutionPos,STSolutionVel,STSolutionN,"red","solid","linewidth=2, alpha=0.5");
                    }
                }

                if(counter == 0){
                    mplc_xlabel(0,"Position");
                    mplc_ylabel(0,"Density");
                } else if(counter == 1){
                    mplc_xlabel(0,"Position");
                    mplc_ylabel(0,"Pressure");
                } else if(counter == 2){
                    mplc_xlabel(0,"Position");
                    mplc_ylabel(0,"Internal Energy");
                } else if(counter == 3){
                    mplc_xlabel(0,"Position");
                    mplc_ylabel(0,"Velocity");
                } 

                if(counter == 0){
                    if(PlotParams.ShowTime){
                        char Time[MaxCharactersInLine];
                        Snprintf(Time,"t = %1.2f ",ThisRun.TCurrent);
                        fprintf(stderr,"t = %1.2f\n",ThisRun.TCurrent);
                        float Ymax = PlotParams.ST_Rhomax;
                        mplc_text(0,0.5*(PlotParams.ST_Xmax+PlotParams.ST_Xmin),1.05*Ymax,Time,"");
                    }
                }
                counter ++;
            }
        }

        mplc_save(0,fname,"");
        mplc_close(0);

    } else {
        mplc_open(0,PlotParams.PythonPath);

        if(PlotParams.ST_PlotType == 0){
            mplc_screen(0,PlotParams.ST_Xmin,PlotParams.ST_Rhomin,PlotParams.ST_Xmax,PlotParams.ST_Rhomax,true);
        } else if(PlotParams.ST_PlotType == 1){
            mplc_screen(0,PlotParams.ST_Xmin,PlotParams.ST_Pmin,PlotParams.ST_Xmax,PlotParams.ST_Pmax,true);
        } else if(PlotParams.ST_PlotType == 2){
            mplc_screen(0,PlotParams.ST_Xmin,PlotParams.ST_Umin,PlotParams.ST_Xmax,PlotParams.ST_Umax,true);
        } else if(PlotParams.ST_PlotType == 3){
            mplc_screen(0,PlotParams.ST_Xmin,PlotParams.ST_Vmin,PlotParams.ST_Xmax,PlotParams.ST_Vmax,true);
        } else if(PlotParams.ST_PlotType == 4){
        } 
        mplc_scatter(0,PosX,Data[PlotParams.ST_PlotType],NULL,ThisRun.NParticles,"black","black","s=30, marker='o'",false,"");

        dprint(PlotParams.ST_PlotSolution);
        if(PlotParams.ST_PlotSolution == 1){
            if(PlotParams.ST_PlotType == 0){
                dprint(STSolutionN);
                mplc_plot(0,STSolutionPos,STSolutionRho,STSolutionN,"red","solid","linewidth=2, alpha=0.5");
            } else if(PlotParams.ST_PlotType == 1){
                mplc_plot(0,STSolutionPos,STSolutionP,STSolutionN,"red","solid","linewidth=2, alpha=0.5");
            } else if(PlotParams.ST_PlotType == 2){
                mplc_plot(0,STSolutionPos,STSolutionU,STSolutionN,"red","solid","linewidth=2, alpha=0.5");
            } else if(PlotParams.ST_PlotType == 3){
                mplc_plot(0,STSolutionPos,STSolutionVel,STSolutionN,"red","solid","linewidth=2, alpha=0.5");
            }
        }

        mplc_title(0,"Shock tube test");
        if(PlotParams.ST_PlotType == 0){
            mplc_xlabel(0,"Position");
            mplc_ylabel(0,"Density");
        } else if(PlotParams.ST_PlotType == 1){
            mplc_xlabel(0,"Position");
            mplc_ylabel(0,"Pressure");
        } else if(PlotParams.ST_PlotType == 2){
            mplc_xlabel(0,"Position");
            mplc_ylabel(0,"Internal Energy");
        } else if(PlotParams.ST_PlotType == 3){
            mplc_xlabel(0,"Position");
            mplc_ylabel(0,"Velocity");
        } 

        if(PlotParams.ShowTime){
            char Time[MaxCharactersInLine];
            Snprintf(Time,"t = %1.2f ",ThisRun.TCurrent);
            fprintf(stderr,"t = %1.2f\n",ThisRun.TCurrent);
            float Ymax = 0.e0; 
            if(PlotParams.ST_PlotType == 0){
                Ymax = PlotParams.ST_Rhomax;
            } else if(PlotParams.ST_PlotType == 1){
                Ymax = PlotParams.ST_Pmax;
            } else if(PlotParams.ST_PlotType == 2){
                Ymax = PlotParams.ST_Umax;
            } else if(PlotParams.ST_PlotType == 3){
                Ymax = PlotParams.ST_Vmax;
            }
            mplc_text(0,0.5*(PlotParams.ST_Xmax+PlotParams.ST_Xmin),1.05*Ymax,Time,"");
        }

        mplc_save(0,fname,"");
        mplc_close(0);
    }


    return ;
}

void PlotShocktube(const int FileID){

    if(!ReadData(FileID)){
        return ;
    }

    STReadAnalyticalSolution();

    char FileNameWrite[MaxCharactersInLine];
    if(PlotParams.ST_PlotType == 0){
        Snprintf(FileNameWrite,"%s_Rho",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.ST_PlotType == 1){
        Snprintf(FileNameWrite,"%s_P",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.ST_PlotType == 2){
        Snprintf(FileNameWrite,"%s_U",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.ST_PlotType == 3){
        Snprintf(FileNameWrite,"%s_V",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.ST_PlotType == 4){
        Snprintf(FileNameWrite,"%s_four",PlotParams.FileName[PlotParams.PlotType]);
    }

    char fname[MaxCharactersInLine];
    if(PlotParams.WriteMode == 0){
        Snprintf(fname,"./%s/%s.%04d.pdf",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    } else if(PlotParams.WriteMode == 1){
        Snprintf(fname,"./%s/%s.%04d.eps",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    } else if(PlotParams.WriteMode == 2){
        Snprintf(fname,"./%s/%s.%04d.png",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    }

    PlotST(fname);

    return ;
}


#pragma once

void OpenPGplot(char WriteFileName[], const int FileID);
void ClosePGplot(void);
double CalcGridDensity(double Pos[], double *H_init, const int dim);
void ColorBar(float Xmin, float Xmax, float Ymin, float Ymax, const double ColorMin, const double ColorMax, const int PaletteID, char Label[]);
void PlotGrid(const double Xmin, const double Xmax, const double Ymin, const double Ymax, const int NGrid, const int dim, const char opt[], const bool colorbar, const char opt_colorbar[]);

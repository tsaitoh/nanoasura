#pragma once

int GetNeighborNumber(double Pos[restrict], const double h);
int GetNeighbors(double Pos[restrict], const double h, int list[restrict]);
int GetNeighborsPairs(double Pos[restrict], const double h, int list[restrict]);
int GetNeighborsDirect(double Pos[restrict], const double h, int list[restrict]);

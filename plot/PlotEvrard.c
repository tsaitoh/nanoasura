#include "config.h"
#include "PlantHydroTree.h"
#include "NeighborSearch.h"
#include "ReadData.h"
#include "PlotMisc.h"

static void PlotEVPoints(const char fname[]){

    mplc_open(0,PlotParams.PythonPath);
    mplc_screen(0,PlotParams.EV_Xmin,PlotParams.EV_Ymin,PlotParams.EV_Xmax,PlotParams.EV_Ymax,true);

    double *PosX,*PosY;
    PosX = malloc(sizeof(double)*ThisRun.NParticles);
    PosY = malloc(sizeof(double)*ThisRun.NParticles);

    for(int i=0;i<ThisRun.NParticles;i++){
        PosX[i] = SPH[i].Pos[0];
        PosY[i] = SPH[i].Pos[1];
    }
    mplc_scatter(0,PosX,PosY,NULL,ThisRun.NParticles,"darkred","red","linewidth=0, s=2, marker='o'",false,"");

    if(PlotParams.ShowTime){
        char Time[MaxCharactersInLine];
        Snprintf(Time,"t = %1.2f",ThisRun.TCurrent);
        fprintf(stderr,"t = %1.2f\n",ThisRun.TCurrent);
        mplc_title(0,Time);
    }

    mplc_xlabel(0,"X");
    mplc_ylabel(0,"Y");
    mplc_save(0,fname,"bbox_inches='tight'");
    mplc_close(0);

    return ;
}

static void PlotEVEnergy(const char fname[]){

    // Load energy data
    FILE *fp;
    char filename[MaxCharactersInLine];
    Snprintf(filename,"%s/Energy.dat",
            PlotParams.DataDir[PlotParams.PlotType]);
    FileOpen(fp,filename,"r");
    int counter = 0;
    while(fscanf(fp,"%*e %*e %*e %*e %*e")!=EOF){
        counter ++;
    }
    double Time[counter],Ek[counter],Ep[counter],Et[counter],E[counter];
    rewind(fp);
    counter = 0;
    while(fscanf(fp,"%le %le %le %le %le",Time+counter,Ek+counter,Ep+counter,Et+counter,E+counter)!=EOF){
        counter ++;
    }
    fclose(fp);

    mplc_open(0,PlotParams.PythonPath);
    //mplc_screen(0,PlotParams.EV_Timemin,PlotParams.EV_Energymin,PlotParams.EV_Timemax,PlotParams.EV_Energymax,false);

    if(PlotParams.EV_PlotType == 10){
        mplc_plot(0,Time,Ek,counter,"black","solid","label='Kinetic'");
        mplc_plot(0,Time,Ep,counter,"red","solid","label='Potential'");
        mplc_plot(0,Time,Et,counter,"blue","solid","label='Thermal'");
        mplc_plot(0,Time,E,counter,"green","solid","label='Total'");
    } else if(PlotParams.EV_PlotType == 11){
        mplc_plot(0,Time,E,counter,"blue","solid","lw=2, label='Total'");
    }

    char command[MaxCharactersInLine];
    Snprintf(command,"plt.xlim([%.17f,%.17f])\n", PlotParams.EV_Timemin, PlotParams.EV_Timemax);
    mplc_send_command(0,command);
    if(PlotParams.EV_PlotType == 10){
        Snprintf(command,"plt.ylim([%.17f,%.17f])\n", PlotParams.EV_Energymin, PlotParams.EV_Energymax);
        mplc_send_command(0,command);
    }
    mplc_xlabel(0,"Time");
    mplc_ylabel(0,"Energy");
    mplc_legend(0,"fontsize=14");
    mplc_save(0,fname,"bbox_inches='tight'");
    mplc_close(0);

    ReleaseSPH();
    exit(EXIT_SUCCESS);
}

static void PlotEVRadialProfile(const char fname[]){

    if(PlotParams.EV_PlotType == 4){ //Rho,Pressure,Velocity
        mplc_set_layout(0,"3,3");
    }
    mplc_open(0,PlotParams.PythonPath);


    double *R,*Data,*Data2,*Data3;
    R = malloc(sizeof(double)*ThisRun.NParticles);
    Data = malloc(sizeof(double)*ThisRun.NParticles);
    Data2 = malloc(sizeof(double)*ThisRun.NParticles);
    Data3 = malloc(sizeof(double)*ThisRun.NParticles);

    for(int i=0;i<ThisRun.NParticles;i++){
        R[i] = NORM(SPH[i].Pos);
        if(PlotParams.EV_PlotType == 1){ //Rho
            Data[i] += SPH[i].Rho/(3.0/(4.0*M_PI));
        } else if(PlotParams.EV_PlotType == 2){ //Pressure
            Data[i] += SPH[i].Pressure/(3.0/(4.0*M_PI));
        } else if(PlotParams.EV_PlotType == 3){ //Velocity
            Data[i] += DOT_PRODUCT(SPH[i].Pos,SPH[i].Vel)/R[i];
        } else if(PlotParams.EV_PlotType == 4){ //Velocity
            Data[i] += SPH[i].Rho/(3.0/(4.0*M_PI));
            Data2[i] += SPH[i].Pressure/(3.0/(4.0*M_PI));
            Data3[i] += DOT_PRODUCT(SPH[i].Pos,SPH[i].Vel)/R[i];
        }
    }

    char Time[MaxCharactersInLine];
    Snprintf(Time,"t = %1.2f",ThisRun.TCurrent);
    fprintf(stderr,"t = %1.2f\n",ThisRun.TCurrent);

    char command[MaxCharactersInLine];

    if(PlotParams.EV_PlotType == 1){ //Rho
        mplc_screen(0,PlotParams.EV_Rmin,PlotParams.EV_Rhomin,PlotParams.EV_Rmax,PlotParams.EV_Rhomax,false);
        mplc_scatter(0,R,Data,NULL,ThisRun.NParticles,"darkred","red","s=20, marker='o', alpha=0.8",false,"");
        mplc_xlabel(0,"Raidus");
        if(PlotParams.ShowTime){
            mplc_title(0,Time);
        }
        mplc_ylabel(0,"Density");
        sprintf(command,"plt.xscale('log')\nplt.yscale('log')\n");
        mplc_send_command(0,command);
    } else if(PlotParams.EV_PlotType == 2){ //Pressure
        mplc_screen(0,PlotParams.EV_Rmin,PlotParams.EV_Pmin,PlotParams.EV_Rmax,PlotParams.EV_Pmax,false);
        mplc_scatter(0,R,Data,NULL,ThisRun.NParticles,"darkred","red","s=20, marker='o', alpha=0.8",false,"");
        mplc_xlabel(0,"Raidus");
        if(PlotParams.ShowTime){
            mplc_title(0,Time);
        }
        mplc_ylabel(0,"Pressure");
        sprintf(command,"plt.xscale('log')\nplt.yscale('log')\n");
        mplc_send_command(0,command);
    } else if(PlotParams.EV_PlotType == 3){ //Velocity
        mplc_screen(0,PlotParams.EV_Rmin,PlotParams.EV_Vmin,PlotParams.EV_Xmax,PlotParams.EV_Vmax,false);
        mplc_scatter(0,R,Data,NULL,ThisRun.NParticles,"darkred","red","s=20, marker='o', alpha=0.8",false,"");
        mplc_xlabel(0,"Raidus");
        if(PlotParams.ShowTime){
            mplc_title(0,Time);
        }
        mplc_ylabel(0,"Velocity");
        sprintf(command,"plt.xscale('log')\n");
        mplc_send_command(0,command);
    } else if(PlotParams.EV_PlotType == 4){
        mplc_subplots(0,"0,0");
        mplc_screen(0,PlotParams.EV_Rmin,PlotParams.EV_Rhomin,PlotParams.EV_Rmax,PlotParams.EV_Rhomax,false);
        mplc_scatter(0,R,Data,NULL,ThisRun.NParticles,"darkred","red","s=20, marker='o', alpha=0.8",false,"rasterize=True");
        mplc_xlabel(0,"Raidus");
        mplc_ylabel(0,"Density");
        sprintf(command,"plt.xscale('log')\nplt.yscale('log')\n");
        mplc_send_command(0,command);

        mplc_subplots(0,"0,1");
        mplc_screen(0,PlotParams.EV_Rmin,PlotParams.EV_Pmin,PlotParams.EV_Rmax,PlotParams.EV_Pmax,false);
        mplc_scatter(0,R,Data2,NULL,ThisRun.NParticles,"darkred","red","s=20, marker='o', alpha=0.8",false,"rasterize=True");
        mplc_xlabel(0,"Raidus");
        mplc_ylabel(0,"Pressure");
        sprintf(command,"plt.xscale('log')\nplt.yscale('log')\n");
        mplc_send_command(0,command);
        if(PlotParams.ShowTime){
            mplc_title(0,Time);
        }

        mplc_subplots(0,"0,2");
        mplc_screen(0,PlotParams.EV_Rmin,PlotParams.EV_Vmin,PlotParams.EV_Xmax,PlotParams.EV_Vmax,false);
        mplc_scatter(0,R,Data3,NULL,ThisRun.NParticles,"darkred","red","s=20, marker='o', alpha=0.8",false,"rasterize=True");
        mplc_xlabel(0,"Raidus");
        mplc_ylabel(0,"Velocity");
        sprintf(command,"plt.xscale('log')\n");
        mplc_send_command(0,command);
    }

    mplc_save(0,fname,"bbox_inches='tight'");
    mplc_close(0);

    free(R);
    free(Data);
    free(Data2);
    free(Data3);

    return ;
}

static void PlotEVRadialProfileBin(const char fname[]){

    int    Nbin[PlotParams.EV_GridSize];
    double Rbin[PlotParams.EV_GridSize];
    double DataRho[PlotParams.EV_GridSize];
    double DataP[PlotParams.EV_GridSize];
    double DataV[PlotParams.EV_GridSize];
    for(int i=0;i<PlotParams.EV_GridSize;i++){
        Nbin[i] = 0;
        Rbin[i] = 0.0;
        DataRho[i] = 0.0;
        DataP[i] = 0.0;
        DataV[i] = 0.0;
    }

    double log_dR = log10(PlotParams.EV_Rmax/PlotParams.EV_Rmin)/PlotParams.EV_GridSize;
    gprint(log10(PlotParams.EV_Rmax/PlotParams.EV_Rmin));
    gprint(log10(PlotParams.EV_Rmin));
    gprint(log10(PlotParams.EV_Rmax));
    gprint(log_dR);
    for(int i=0;i<ThisRun.NParticles;i++){
        double R = NORM(SPH[i].Pos);
        int IndexR = log10(R/PlotParams.EV_Rmin)/log_dR;
        if(IndexR<0) continue;
        if(IndexR>=PlotParams.EV_GridSize) continue;

        DataRho[IndexR] += SPH[i].Rho/(3.0/(4.0*M_PI));
        DataP[IndexR] += SPH[i].Pressure/(3.0/(4.0*M_PI));
        DataV[IndexR] += DOT_PRODUCT(SPH[i].Pos,SPH[i].Vel)/R;

        Nbin[IndexR] ++;
    }

    for(int i=0;i<PlotParams.EV_GridSize;i++){

        DataRho[i] /= Nbin[i]>0?Nbin[i]:1.0;
        DataP[i] /= Nbin[i]>0?Nbin[i]:1.0;
        DataV[i] /= Nbin[i]>0?Nbin[i]:1.0;

        DataRho[i] = DataRho[i]>0?DataRho[i]:0.001;
        DataP[i] = DataP[i]>0?DataP[i]:0.001;

        Rbin[i] = PlotParams.EV_Rmin*pow(10.0,(i+0.5)*log_dR);

        fprintf(stderr,"%d %g %g %g %g\n",Nbin[i],Rbin[i],DataRho[i],DataP[i],DataV[i]);
    }

    if(PlotParams.EV_PlotType == 8){ //Rho,Pressure,Velocity
        mplc_set_layout(0,"3,3");
    }
    mplc_open(0,PlotParams.PythonPath);

    char Time[MaxCharactersInLine];
    if(PlotParams.ShowTime){
        Snprintf(Time,"t = %1.2f",ThisRun.TCurrent);
        fprintf(stderr,"t = %1.2f\n",ThisRun.TCurrent);
    }

    if(PlotParams.EV_PlotType == 5){ //Rho
        mplc_screen(0,PlotParams.EV_Rmin,PlotParams.EV_Rhomin,PlotParams.EV_Rmax,PlotParams.EV_Rhomax,false);
        mplc_scatter(0,Rbin,DataRho,NULL,PlotParams.EV_GridSize,"darkred","red","s=30, marker='o'",false,"");
        mplc_xlabel(0,"Raidus");
        mplc_ylabel(0,"Density");
        mplc_send_command(0,"plt.xscale('log')\nplt.yscale('log')\n");
        if(PlotParams.ShowTime)
            mplc_title(0,Time);
    } else if(PlotParams.EV_PlotType == 6){ //Pressure
        mplc_screen(0,PlotParams.EV_Rmin,PlotParams.EV_Pmin,PlotParams.EV_Rmax,PlotParams.EV_Pmax,false);
        mplc_scatter(0,Rbin,DataP,NULL,PlotParams.EV_GridSize,"darkred","red","s=30, marker='o'",false,"");
        mplc_xlabel(0,"Raidus");
        mplc_ylabel(0,"Pressure");
        mplc_send_command(0,"plt.xscale('log')\nplt.yscale('log')\n");
        if(PlotParams.ShowTime)
            mplc_title(0,Time);
    } else if(PlotParams.EV_PlotType == 7){ //Velocity
        mplc_screen(0,PlotParams.EV_Rmin,PlotParams.EV_Vmin,PlotParams.EV_Xmax,PlotParams.EV_Vmax,false);
        mplc_scatter(0,Rbin,DataV,NULL,PlotParams.EV_GridSize,"darkred","red","s=30, marker='o'",false,"");
        mplc_xlabel(0,"Raidus");
        mplc_ylabel(0,"Velocity");
        mplc_send_command(0,"plt.xscale('log')\n");
        if(PlotParams.ShowTime)
            mplc_title(0,Time);
    } else if(PlotParams.EV_PlotType == 8){ //Rho,Pressure,Velocity
        mplc_subplots(0,"0,0");
        mplc_screen(0,PlotParams.EV_Rmin,PlotParams.EV_Rhomin,PlotParams.EV_Rmax,PlotParams.EV_Rhomax,false);
        mplc_scatter(0,Rbin,DataP,NULL,PlotParams.EV_GridSize,"darkred","red","s=30, marker='o'",false,"");
        mplc_xlabel(0,"Raidus");
        mplc_ylabel(0,"Density");
        mplc_send_command(0,"plt.xscale('log')\nplt.yscale('log')\n");

        mplc_subplots(0,"0,1");
        mplc_screen(0,PlotParams.EV_Rmin,PlotParams.EV_Pmin,PlotParams.EV_Rmax,PlotParams.EV_Pmax,false);
        mplc_scatter(0,Rbin,DataP,NULL,PlotParams.EV_GridSize,"darkred","red","s=30, marker='o'",false,"");
        mplc_xlabel(0,"Raidus");
        mplc_ylabel(0,"Pressure");
        mplc_send_command(0,"plt.xscale('log')\nplt.yscale('log')\n");
        if(PlotParams.ShowTime){
            mplc_title(0,Time);
        }

        mplc_subplots(0,"0,2");
        mplc_screen(0,PlotParams.EV_Rmin,PlotParams.EV_Vmin,PlotParams.EV_Xmax,PlotParams.EV_Vmax,false);
        mplc_scatter(0,Rbin,DataV,NULL,PlotParams.EV_GridSize,"darkred","red","s=30, marker='o'",false,"");
        mplc_xlabel(0,"Raidus");
        mplc_ylabel(0,"Velocity");
        mplc_send_command(0,"plt.xscale('log')\n");
    }

    mplc_save(0,fname,"bbox_inches='tight'");
    mplc_close(0);
}


void PlotEvrard(const int FileID){

    if(!ReadData(FileID)){
        return ;
    }

    char FileNameWrite[MaxCharactersInLine];
    if(PlotParams.EV_PlotType == 0){
        Snprintf(FileNameWrite,"%s_dot",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.EV_PlotType == 1){
        Snprintf(FileNameWrite,"%s_r_rho",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.EV_PlotType == 2){
        Snprintf(FileNameWrite,"%s_r_p",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.EV_PlotType == 3){
        Snprintf(FileNameWrite,"%s_r_v",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.EV_PlotType == 4){
        Snprintf(FileNameWrite,"%s_r_rhopv",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.EV_PlotType == 5){
        Snprintf(FileNameWrite,"%s_r_rho_bin",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.EV_PlotType == 6){
        Snprintf(FileNameWrite,"%s_r_p_bin",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.EV_PlotType == 7){
        Snprintf(FileNameWrite,"%s_r_v_bin",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.EV_PlotType == 8){
        Snprintf(FileNameWrite,"%s_r_rhopv_bin",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.EV_PlotType == 10){
        Snprintf(FileNameWrite,"%s_energy",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.EV_PlotType == 11){
        Snprintf(FileNameWrite,"%s_total_energy",PlotParams.FileName[PlotParams.PlotType]);
    }

    char fname[MaxCharactersInLine];
    if(PlotParams.WriteMode == 0){
        Snprintf(fname,"./%s/%s.%04d.pdf",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    } else if(PlotParams.WriteMode == 1){
        Snprintf(fname,"./%s/%s.%04d.eps",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    } else if(PlotParams.WriteMode == 2){
        Snprintf(fname,"./%s/%s.%04d.png",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    }

    if(PlotParams.EV_PlotType == 0){
        PlotEVPoints(fname);
    } else if(PlotParams.EV_PlotType == 1){
        PlotEVRadialProfile(fname);
    } else if(PlotParams.EV_PlotType == 2){
        PlotEVRadialProfile(fname);
    } else if(PlotParams.EV_PlotType == 3){
        PlotEVRadialProfile(fname);
    } else if(PlotParams.EV_PlotType == 4){
        PlotEVRadialProfile(fname);
    } else if(PlotParams.EV_PlotType == 5){
        PlotEVRadialProfileBin(fname);
    } else if(PlotParams.EV_PlotType == 6){
        PlotEVRadialProfileBin(fname);
    } else if(PlotParams.EV_PlotType == 7){
        PlotEVRadialProfileBin(fname);
    } else if(PlotParams.EV_PlotType == 8){
        PlotEVRadialProfileBin(fname);
    } else if(PlotParams.EV_PlotType == 10){
        PlotEVEnergy(fname);
    } else if(PlotParams.EV_PlotType == 11){
        PlotEVEnergy(fname);
    }

    return ;
}

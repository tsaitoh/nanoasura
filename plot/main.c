#include "config.h"
#include "ReadParameters.h"
#include "InitOpenMP.h"
#include "Plot.h"

/*!
 * This is the main function of this program.
 */
int main(int argc, char **argv){

    // Read and set run parameters.
    if(argc > 1){
        ReadParameters(argv[1]);
    } else {
        ReadParameters("./param.toml");
    }

    Plot();

    return EXIT_SUCCESS;
}

#include "config.h"

struct StructThisRun ThisRun;
struct StructSPH *SPH;
struct StructHydroRoot HydroRoot;
struct StructHydroNode *HydroNode;
struct StructNBCache *NBCache;


struct StructPlotParams PlotParams;
#if 0
//////////////////////////
char OutDir[MaxCharactersInLine];
char DataDir[MaxModelNumber][MaxCharactersInLine];
char FileName[MaxModelNumber][MaxCharactersInLine];

int WriteMode;
int PaletteID;

int FileStart;
int FileEnd;
int ShowTime;
int UseEOSRho;

int PlotType;

// Plot Type 0
int ST_PlotType;
int ST_PlotSolution;
char ST_SolutionFileName[MaxCharactersInLine];
double ST_Xmin;
double ST_Xmax;
double ST_Ymin;
double ST_Ymax;
double ST_Rhomin;
double ST_Rhomax;
double ST_Pmin;
double ST_Pmax;
double ST_Umin;
double ST_Umax;
double ST_Vmin;
double ST_Vmax;


// For PlotType 1
int HS_PlotType;
int HS_Color[2];
int HS_Symbol[2];
double HS_Xmin;
double HS_Xmax;
double HS_Ymin;
double HS_Ymax;
int HS_Palette; 
int HS_GridSize;
double HS_ColorMinDens;
double HS_ColorMaxDens;
double HS_ColorMinP;
double HS_ColorMaxP;


// For PlotType 2
int KH_PlotType;
int KH_Color[2];
int KH_Symbol[2];
double KH_Xmin;
double KH_Xmax;
double KH_Ymin;
double KH_Ymax;
int KH_Palette; 
int KH_GridSize;
double KH_ColorMinDens;
double KH_ColorMaxDens;
double KH_ColorMinP;
double KH_ColorMaxP;


// For PlotType 3
int RT_PlotType;
int RT_UseCopy;
int RT_Color[3];
int RT_Symbol[3];
double RT_Xmin;
double RT_Xmax;
double RT_Ymin;
double RT_Ymax;
int RT_Palette; 
int RT_GridSize;
double RT_ColorMinDens;
double RT_ColorMaxDens;
double RT_ColorMinP;
double RT_ColorMaxP;

// For PlotType 4
int SD_PlotType;
int SD_PlotSolution;
char SD_SolutionFileName[MaxCharactersInLine];
double SD_Xmin;
double SD_Xmax;
double SD_Ymin;
double SD_Ymax;
double SD_Slice;
double SD_Rmin;
double SD_Rmax;
double SD_Rhomin;
double SD_Rhomax;
double SD_Pmin;
double SD_Pmax;
int SD_GridSize;
double SD_Xmin_Grid;
double SD_Xmax_Grid;
double SD_Ymin_Grid;
double SD_Ymax_Grid;
int SD_Palette;


// For PlotType 5
int KP_PlotType;
double KP_Xmin;
double KP_Xmax;
double KP_Ymin;
double KP_Ymax;
int KP_Color[2];
int KP_Symbol[2];
int KP_Palette;
int KP_GridSize;
double KP_ColorMinDens;
double KP_ColorMaxDens;

// For PlotType 6
int GL_PlotType;
double GL_Xmin;
double GL_Xmax;
double GL_Ymin;
double GL_Ymax;
double GL_Phimin;
double GL_Phimax;
double GL_Vmin;
double GL_Vmax;
int GL_Color[2];
int GL_Symbol[2];
int GL_Palette;
int GL_GridSize;
double GL_ColorMinDens;
double GL_ColorMaxDens;
double GL_ColorMinPV;
double GL_ColorMaxPV;
double GL_Rmin;
double GL_Rmax;
double GL_RCmin;
double GL_RCmax;
double GL_RSmin;
double GL_RSmax;
#endif

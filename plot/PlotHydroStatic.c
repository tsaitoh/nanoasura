#include "config.h"
#include "PlantHydroTree.h"
#include "NeighborSearch.h"
#include "ReadData.h"
#include "PlotMisc.h"

static void PlotHS(char fname[]){

    mplc_open(0,PlotParams.PythonPath);
    mplc_screen(0,PlotParams.HS_Xmin,PlotParams.HS_Ymin,PlotParams.HS_Xmax,PlotParams.HS_Ymax,true);

    if(PlotParams.HS_PlotType == 0){
        double PosX[ThisRun.NParticles];
        double PosY[ThisRun.NParticles];

        // Plot Tag0
        int counter = 0;
        for(int i=0;i<ThisRun.NParticles;i++){
            if(SPH[i].Tag == 0){
                PosX[counter] = SPH[i].Pos[0];
                PosY[counter] = SPH[i].Pos[1];
                counter ++;
            }
        }

        mplc_scatter(0,PosX,PosY,NULL,counter,"darkred","red","s=20, marker='o'",false,"");

        // Plot Tag1
        counter = 0;
        for(int i=0;i<ThisRun.NParticles;i++){
            if(SPH[i].Tag == 1){
                PosX[counter] = SPH[i].Pos[0];
                PosY[counter] = SPH[i].Pos[1];
                counter ++;
            }
        }
        mplc_scatter(0,PosX,PosY,NULL,counter,"darkblue","blue","s=20, marker='o'",false,"");
    } else if (PlotParams.HS_PlotType == 1){
        double PosX[ThisRun.NParticles];
        double PosY[ThisRun.NParticles];
        double Color[ThisRun.NParticles];
        for(int i=0;i<ThisRun.NParticles;i++){
            PosX[i] = SPH[i].Pos[0];
            PosY[i] = SPH[i].Pos[1];
            Color[i] = SPH[i].Rho;
        }
        mplc_scatter(0,PosX,PosY,Color,ThisRun.NParticles,"black","","linewidth=0, s=20, marker='o', vmin=0.9, vmax=4.1",true,"label='Density'");
    } else if (PlotParams.HS_PlotType == 2){
        double PosX[ThisRun.NParticles];
        double PosY[ThisRun.NParticles];
        double Color[ThisRun.NParticles];
        for(int i=0;i<ThisRun.NParticles;i++){
            PosX[i] = SPH[i].Pos[0];
            PosY[i] = SPH[i].Pos[1];
            Color[i] = SPH[i].Pressure;
        }
        mplc_scatter(0,PosX,PosY,Color,ThisRun.NParticles,"black","","linewidth=0, s=20, marker='o', vmin=2.4, vmax=2.6",true,"label='Pressure'");
    }


    if(PlotParams.ShowTime){
        char Time[MaxCharactersInLine];
        Snprintf(Time,"t = %1.2f ",ThisRun.TCurrent);
        fprintf(stderr,"t = %1.2f\n",ThisRun.TCurrent);
        mplc_text(0,0.5,1.05,Time,"");
    }

    mplc_xlabel(0,"X");
    mplc_ylabel(0,"Y");
    mplc_save(0,fname,"");
    mplc_close(0);

    return ;
}

void PlotHydroStatic(const int FileID){

    if(!ReadData(FileID)){
        return ;
    }

    char FileNameWrite[MaxCharactersInLine];
    if(PlotParams.HS_PlotType == 0){
        Snprintf(FileNameWrite,"%s_dot",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.HS_PlotType == 1){
        Snprintf(FileNameWrite,"%s_Rho",PlotParams.FileName[PlotParams.PlotType]);
    } else if(PlotParams.HS_PlotType == 2){
        Snprintf(FileNameWrite,"%s_P",PlotParams.FileName[PlotParams.PlotType]);
    }

    char fname[MaxCharactersInLine];
    if(PlotParams.WriteMode == 0){
        Snprintf(fname,"./%s/%s.%04d.pdf",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    } else if(PlotParams.WriteMode == 1){
        Snprintf(fname,"./%s/%s.%04d.eps",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    } else if(PlotParams.WriteMode == 2){
        Snprintf(fname,"./%s/%s.%04d.png",PlotParams.OutDir[PlotParams.PlotType],FileNameWrite,FileID);
    }

    PlotHS(fname);

    return ;
}



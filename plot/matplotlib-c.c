/*
 * This is a library to draw figures by matplotlib from c programs.
 * The original version of this program writtein in C++ is provided by Prof.
 * Masahide Kashiwagi; http://verifiedby.me/adiary/0115
 * 
 * This library, matplotlib for c (mplc), is distributed under the MIT licence.
 * Please feel free to contact me if you have problems, issues, and ideas.
 * Contact address: saitoh@people.kobe-u.ac.jp
 *
 * How to use this library.
 * Type 'make' command in this directory and check there is 'libmplc.a'.
 * Or, copy matplotlib-c.[c,h] to your source codes and compile them with your
 * code.
 * Note that this is the simplified version and there is only the primitive
 * functions.
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdarg.h>

// This is the max allowable number of devices.
// If necessary, please change the number.
#define mplc_MaxDeviceNumber (16)

#define mplc_MaxCharactersInLine (256)

// This is the device list.
static FILE *mplc_device[mplc_MaxDeviceNumber] = {NULL};
static FILE *mplc_device_log[mplc_MaxDeviceNumber] = {NULL}; // This is for logging
static char mplc_device_layout[mplc_MaxDeviceNumber][mplc_MaxCharactersInLine] = {0};

static bool mplc_debug_flag = false;

void mplc_fprintf(const int device_id, const char *format, ...){
    va_list args;
    va_start(args, format);
    vfprintf(mplc_device[device_id], format, args);
    if((mplc_debug_flag == true)&&(mplc_device_log[device_id] != NULL)){
        vfprintf(mplc_device_log[device_id], format, args);
    }
    va_end(args);
    return ;
}

void mplc_fflush(const int device_id){
    fflush(mplc_device[device_id]);
    if((mplc_debug_flag == true)&&(mplc_device_log[device_id] != NULL)){
        fflush(mplc_device_log[device_id]);
    }
    return ;
}

void mplc_set_debug_flag(const bool flag){
    mplc_debug_flag = flag;
    return ;
}

void mplc_send_command(const int device_id, const char *s){
    mplc_fprintf(device_id, "%s\n", s);
    mplc_fflush(device_id);
    return ;
}

static bool first_call = true;

void mplc_splash(void){
    
    fprintf(stderr,"////////////////////////////////////////////////////\n");
    fprintf(stderr,"// Matplolib-c                                    //\n");
    fprintf(stderr,"//  A library to draw figures                     //\n");
    fprintf(stderr,"//                 with matplotlib with lang c    //\n");
    fprintf(stderr,"//                           (c) Takayuki Saitoh. //\n");
#ifdef GITVERSION //{
    fprintf(stderr,"// hash: %s //\n",GITVERSION);
#else //GITVERSION  //}//{
    fprintf(stderr,"// hash: N/A                                      //\n");
#endif //GITVERSION  //}
    fprintf(stderr,"//////////////////////////////////////////////////// \n");
    fflush(stderr);

    return ;
}

void mplc_set_layout(const int device_id, const char *c){
    strncpy(mplc_device_layout[device_id],c,mplc_MaxCharactersInLine-1);
    return ;
}

bool mplc_open(const int device_id, const char *python_name){
    if(first_call == true){
        mplc_splash();
        first_call = false;
    }
    char command[mplc_MaxCharactersInLine];
    //snprintf(command,mplc_MaxCharactersInLine-1,"%s -c 'import code; import os; import sys; sys.stdout = sys.stderr = open(os.devnull, \"w\"); code.InteractiveConsole().interact()'",python_name);
    snprintf(command,mplc_MaxCharactersInLine-1,"%s -c 'import code; import os; import sys; sys.stdout = sys.stderr = open(\"mplc.log\", \"w\"); code.InteractiveConsole().interact()'",python_name);
    mplc_device[device_id] = popen(command, "w");
    if (mplc_device[device_id] == NULL) return false;

    mplc_send_command(device_id,"import matplotlib");
    mplc_send_command(device_id,"matplotlib.use('Agg')");

    if(mplc_debug_flag){
        char mplc_log_filename[mplc_MaxCharactersInLine];
        snprintf(mplc_log_filename,mplc_MaxCharactersInLine-1,"mplc_log.%02d",device_id);
        mplc_device_log[device_id] = fopen(mplc_log_filename, "w");
        if (mplc_device_log[device_id] == NULL) return false;
    }

    mplc_send_command(device_id,"import sys");
    mplc_send_command(device_id,"if sys.version_info[0] == 2:");
    mplc_send_command(device_id,"\tprint('\\n\\t=== Caution! MPLC requires python version 3.x.x ===')\n");

    mplc_send_command(device_id,"import matplotlib.pyplot as plt");
    mplc_send_command(device_id,"import matplotlib.patches as patches");
    mplc_send_command(device_id,"import numpy as np");
    if(strlen(mplc_device_layout[device_id])>0){
        mplc_send_command(device_id,"import matplotlib.gridspec as gridspec\n");
        mplc_fprintf(device_id,"fig = plt.figure(tight_layout=True)\n");
        mplc_fprintf(device_id,"gs = gridspec.GridSpec(%s)\n",mplc_device_layout[device_id]);
    } else {
        mplc_send_command(device_id,"fig, ax = plt.subplots()");
    }
    // mplc_send_command(device_id,"plt.show(block=False)");
    return true;
}

bool mplc_close(const int device_id){
    mplc_send_command(device_id,"plt.close()");
    mplc_send_command(device_id,"quit()");
    strncpy(mplc_device_layout[device_id],"",mplc_MaxCharactersInLine-1);
    if (pclose(mplc_device[device_id]) == -1) return false;
    if((mplc_debug_flag == true)&&(mplc_device_log[device_id] != NULL)){
        if (fclose(mplc_device_log[device_id]) == -1) return false;
    }
    return true;
}

void mplc_subplots(const int device_id, const char *c){
    mplc_fprintf(device_id, "ax = fig.add_subplot(gs[%s])\n",c);
    return ;
}

void mplc_screen(const int device_id, const double x1, const double y1, const double x2, const double y2, const bool EqualAspect){
    if (EqualAspect == true) {
        mplc_fprintf(device_id, "ax.set_aspect('equal')\n");
    }
    mplc_fprintf(device_id, "plt.xlim([%.17f,%.17f])\n", x1, x2);
    mplc_fprintf(device_id, "plt.ylim([%.17f,%.17f])\n", y1, y2);
    mplc_fprintf(device_id, "plt.draw()\n");
    mplc_fflush(device_id);
    return ;
}

void mplc_line(const int device_id, double x1, double y1, double x2, double y2, const char *color, const char *opt){
    mplc_fprintf(device_id, "ax.plot([%.17f,%.17f],[%.17f,%.17f], color='%s', %s)\n", x1, x2, y1, y2, color, opt);
    mplc_fprintf(device_id, "fig.canvas.blit(ax.bbox)\n");
    mplc_fprintf(device_id, "fig.canvas.flush_events()\n");
    mplc_fflush(device_id);
    return ;
}

void mplc_plot(const int device_id, const double x[], const double y[], const int number, const char *color, const char *linestyle, const char *opt){

    mplc_fprintf(device_id, "xvalues = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "(%.17f),", x[i]);
    }
    mplc_fprintf(device_id, "]\n");
    mplc_fprintf(device_id, "yvalues = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "(%.17f),", y[i]);
    }
    mplc_fprintf(device_id, "]\n");
    if((strncmp(linestyle,"solid",mplc_MaxCharactersInLine)   == 0)||
       (strncmp(linestyle,"dotted",mplc_MaxCharactersInLine)  == 0)||
       (strncmp(linestyle,"dashed",mplc_MaxCharactersInLine)  == 0)||
       (strncmp(linestyle,"dashdot",mplc_MaxCharactersInLine) == 0)){
        mplc_fprintf(device_id, "ax.plot(xvalues,yvalues,color='%s', linestyle='%s', %s)\n", color, linestyle, opt);
    } else {
        mplc_fprintf(device_id, "ax.plot(xvalues,yvalues,color='%s', linestyle=%s, %s)\n", color, linestyle, opt);
    }
    mplc_fprintf(device_id, "fig.canvas.blit(ax.bbox)\n");
    mplc_fprintf(device_id, "fig.canvas.flush_events()\n");
    mplc_fflush(device_id);
    return ;
}

void mplc_step(const int device_id, const double x[], const double y[], const int number, const char *color, const char *linestyle, const char *where, const char *opt){

    mplc_fprintf(device_id, "xvalues = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "(%.17f),", x[i]);
    }
    mplc_fprintf(device_id, "]\n");
    mplc_fprintf(device_id, "yvalues = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "(%.17f),", y[i]);
    }
    mplc_fprintf(device_id, "]\n");
    if((strncmp(linestyle,"solid",mplc_MaxCharactersInLine)   == 0)||
       (strncmp(linestyle,"dotted",mplc_MaxCharactersInLine)  == 0)||
       (strncmp(linestyle,"dashed",mplc_MaxCharactersInLine)  == 0)||
       (strncmp(linestyle,"dashdot",mplc_MaxCharactersInLine) == 0)){
        mplc_fprintf(device_id, "ax.step(xvalues,yvalues,color='%s',linestyle='%s',where='%s',%s)\n", color, linestyle, where, opt);
    } else {
        mplc_fprintf(device_id, "ax.step(xvalues,yvalues,color='%s',linestyle=%s,where='%s',%s)\n", color, linestyle, where, opt);
    }
    mplc_fprintf(device_id, "fig.canvas.blit(ax.bbox)\n");
    mplc_fprintf(device_id, "fig.canvas.flush_events()\n");
    mplc_fflush(device_id);
    return ;
}

void mplc_fill_between(const int device_id, const double x[], const double y1[], const double y2[], const int number, const char *opt){

    mplc_fprintf(device_id, "xvalues = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "(%.17f),", x[i]);
    }
    mplc_fprintf(device_id, "]\n");
    mplc_fprintf(device_id, "y1values = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "(%.17f),", y1[i]);
    }
    mplc_fprintf(device_id, "]\n");
    mplc_fprintf(device_id, "y2values = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "(%.17f),", y2[i]);
    }
    mplc_fprintf(device_id, "]\n");


    mplc_fprintf(device_id, "ax.fill_between(xvalues,y1values,y2values,%s)\n", opt);
    mplc_fprintf(device_id, "fig.canvas.blit(ax.bbox)\n");
    mplc_fprintf(device_id, "fig.canvas.flush_events()\n");
    mplc_fflush(device_id);

    return ;
}

void mplc_point(const int device_id, double x, double y, const char *color, const char *opt){
    mplc_fprintf(device_id, "ax.scatter([%.17f],[%.17f], color='%s', %s)\n", x, y, color, opt);
    mplc_fprintf(device_id, "fig.canvas.blit(ax.bbox)\n");
    mplc_fprintf(device_id, "fig.canvas.flush_events()\n");
    mplc_fflush(device_id);
    return ;
}

void mplc_points(const int device_id, const double x[], const double y[], const int number, const char *color, const char *opt){
    for(int i=0;i<number;i++)
        mplc_fprintf(device_id, "ax.scatter([%.17f],[%.17f], color='%s', %s)\n", x[i], y[i], color, opt);
    mplc_fprintf(device_id, "fig.canvas.blit(ax.bbox)\n");
    mplc_fprintf(device_id, "fig.canvas.flush_events()\n");
    mplc_fflush(device_id);
    return ;
}

void mplc_scatter(const int device_id, const double x[], const double y[], const double c[], const int number, const char *color, const char *facecolor, const char *opt, const bool colorbar, const char *opt_colorbar){

    mplc_fprintf(device_id, "xvalues = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "(%.17f),", x[i]);
    }
    mplc_fprintf(device_id, "]\n");
    mplc_fprintf(device_id, "yvalues = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "(%.17f),", y[i]);
    }
    mplc_fprintf(device_id, "]\n");
    if(c != NULL){
        mplc_fprintf(device_id, "cvalues = [");
        for (int i=0; i<number; i++) {
            mplc_fprintf(device_id, "(%.17f),", c[i]);
        }
        mplc_fprintf(device_id, "]\n");
        if ((facecolor == NULL)||(strlen(facecolor)==0)) {
            mplc_fprintf(device_id, "sc = ax.scatter(xvalues,yvalues,c=cvalues, edgecolor='%s', %s)\n", color, opt);
        } else {
            mplc_fprintf(device_id, "sc = ax.scatter(xvalues,yvalues,c=cvalues, edgecolor='%s', facecolor='%s', %s)\n", color, facecolor,opt);
        }
    } else {
        if ((facecolor == NULL)||(strlen(facecolor)==0)) {
            mplc_fprintf(device_id, "sc = ax.scatter(xvalues,yvalues, edgecolor='%s', %s)\n", color, opt);
        } else {
            mplc_fprintf(device_id, "sc = ax.scatter(xvalues,yvalues, edgecolor='%s', facecolor='%s', %s)\n", color, facecolor,opt);
        }
    }

    if(colorbar == true){
        mplc_fprintf(device_id, "fig.colorbar(sc, %s)\n",opt_colorbar);
    }

    mplc_fprintf(device_id, "fig.canvas.blit(ax.bbox)\n");
    mplc_fprintf(device_id, "fig.canvas.flush_events()\n");
    mplc_fflush(device_id);
    return ;
}

void mplc_rect(const int device_id, double x1, double y1, double x2, double y2, const char *edgecolor, const char *facecolor, const char *opt){
    
    if ((facecolor == NULL)||(strlen(facecolor)==0)) {
        mplc_fprintf(device_id, "ax.add_patch(patches.Rectangle(xy=(%.17f,%.17f), width=%.17f, height=%.17f, fill=False, edgecolor='%s', %s))\n", x1, y1, x2-x1, y2-y1, edgecolor, opt);
    } else {
        mplc_fprintf(device_id, "ax.add_patch(patches.Rectangle(xy=(%.17f,%.17f), width=%.17f, height=%.17f, fill=True, edgecolor='%s', facecolor='%s', %s))\n", x1, y1, x2-x1, y2-y1, edgecolor, facecolor, opt);
    }
    mplc_fprintf(device_id, "fig.canvas.blit(ax.bbox)\n");
    mplc_fprintf(device_id, "fig.canvas.flush_events()\n");
    mplc_fflush(device_id);
}

void mplc_ellipse(const int device_id, double cx, double cy, double rx, double ry, const char *edgecolor, const char *facecolor, const char *opt){
    if ((facecolor == NULL)||(strlen(facecolor)==0)) {
        mplc_fprintf(device_id, "ax.add_patch(patches.Ellipse(xy=(%.17f,%.17f), width=%.17f, height=%.17f, fill=False, edgecolor='%s', %s))\n", cx, cy, rx*2, ry*2, edgecolor, opt);
    } else {
        mplc_fprintf(device_id, "ax.add_patch(patches.Ellipse(xy=(%.17f,%.17f), width=%.17f, height=%.17f, fill=True, edgecolor='%s', facecolor='%s', %s))\n", cx, cy, rx*2, ry*2, edgecolor, facecolor, opt);
    }
    mplc_fprintf(device_id, "fig.canvas.blit(ax.bbox)\n");
    mplc_fprintf(device_id, "fig.canvas.flush_events()\n");
    mplc_fflush(device_id);
    return  ;
}

void mplc_circle(const int device_id, double cx, double cy, double r, const char *edgecolor, const char *facecolor, const char *opt){
    mplc_ellipse(device_id, cx, cy, r, r, edgecolor, facecolor, opt);
}

void mplc_polygon(const int device_id, const double *x, const double *y, const int number, const char *edgecolor, const char *facecolor, const char *opt){

    mplc_fprintf(device_id, "ax.add_patch(patches.Polygon((");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "(%.17f,%.17f),", x[i], y[i]);
    }
    
    if ((facecolor == NULL)||(strlen(facecolor)==0)) {
        mplc_fprintf(device_id, "), fill=False, edgecolor='%s', %s))\n", edgecolor, opt);
    } else {
        mplc_fprintf(device_id, "), fill=True, edgecolor='%s', facecolor='%s', %s))\n", edgecolor, facecolor, opt);
    }
    mplc_fprintf(device_id, "fig.canvas.blit(ax.bbox)\n");
    mplc_fprintf(device_id, "fig.canvas.flush_events()\n");
    mplc_fflush(device_id);
    return ;
}

void mplc_bar(const int device_id, const double x[], const double y[], const int number, const double width, const char *edgecolor, const char *facecolor, const char *opt){

    mplc_fprintf(device_id, "xvalues = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "(%.17f),", x[i]);
    }
    mplc_fprintf(device_id, "]\n");
    mplc_fprintf(device_id, "yvalues = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "(%.17f),", y[i]);
    }
    mplc_fprintf(device_id, "]\n");
    if ((facecolor == NULL)||(strlen(facecolor)==0)) {
        mplc_fprintf(device_id, "ax.bar(xvalues,yvalues, facecolor='%s', width=%g, %s)\n", edgecolor, width, opt);
    } else {
        mplc_fprintf(device_id, "ax.bar(xvalues,yvalues, facecolor='%s', edgecolor='%s', width=%g, %s)\n", facecolor, edgecolor, width, opt);
    } 
    mplc_fprintf(device_id, "fig.canvas.blit(ax.bbox)\n");
    mplc_fprintf(device_id, "fig.canvas.flush_events()\n");
    mplc_fflush(device_id);
    return ;
}

void mplc_stackplot(const int device_id, const int number, const int groups, const double x[], const double y[][number],  const char *opt){

    mplc_fprintf(device_id, "xvalues = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "(%.17f),", x[i]);
    }
    mplc_fprintf(device_id, "]\n");

    for(int k=0;k<groups;k++){
        mplc_fprintf(device_id, "yvalues%02d = [",k);
        for (int i=0; i<number; i++) {
            mplc_fprintf(device_id, "(%.17f),", y[k][i]);
        }
        mplc_fprintf(device_id, "]\n");
    }

    mplc_fprintf(device_id, "y = np.vstack([");
    for(int k=0;k<groups-1;k++){
        mplc_fprintf(device_id, "yvalues%02d,",k);
    }
    mplc_fprintf(device_id, "yvalues%02d",groups-1);
    mplc_fprintf(device_id, "])\n");

    mplc_fprintf(device_id, "ax.stackplot(xvalues,y, %s)\n",opt);
    mplc_fprintf(device_id, "fig.canvas.blit(ax.bbox)\n");
    mplc_fprintf(device_id, "fig.canvas.flush_events()\n");
    mplc_fflush(device_id);
    return ;
}

void mplc_contour(const int device_id, const double x[], const double y[], const double z[], const int number, const char *opt, const bool colorbar, const char *opt_colorbar){

    mplc_fprintf(device_id, "xvalues = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "(%.17f),", x[i]);
    }
    mplc_fprintf(device_id, "]\n");
    mplc_fprintf(device_id, "yvalues = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "(%.17f),", y[i]);
    }
    mplc_fprintf(device_id, "]\n");
    mplc_fprintf(device_id, "zvalues = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "[");
        for (int j=0; j<number; j++) {
            mplc_fprintf(device_id, "(%.17f),", z[i*number+j]);
        }
        mplc_fprintf(device_id, "],\n");
    }
    mplc_fprintf(device_id, "]\n");

    mplc_fprintf(device_id, "cf = ax.contour(xvalues,yvalues,zvalues,%s)\n", opt);
    if(colorbar == true){
        mplc_fprintf(device_id, "fig.colorbar(cf, %s)\n",opt_colorbar);
    }
    mplc_fprintf(device_id, "fig.canvas.blit(ax.bbox)\n");
    mplc_fprintf(device_id, "fig.canvas.flush_events()\n");
    mplc_fflush(device_id);
    return ;
}

void mplc_contourf(const int device_id, const double x[], const double y[], const double z[], const int number, const char *opt, const bool colorbar, const char *opt_colorbar){

    mplc_fprintf(device_id, "xvalues = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "(%.17f),", x[i]);
    }
    mplc_fprintf(device_id, "]\n");
    mplc_fprintf(device_id, "yvalues = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "(%.17f),", y[i]);
    }
    mplc_fprintf(device_id, "]\n");
    mplc_fprintf(device_id, "zvalues = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "[");
        for (int j=0; j<number; j++) {
            mplc_fprintf(device_id, "(%.17f),", z[i*number+j]);
        }
        mplc_fprintf(device_id, "],\n");
    }
    mplc_fprintf(device_id, "]\n");

    mplc_fprintf(device_id, "cf = ax.contourf(xvalues,yvalues,zvalues,%s)\n", opt);
    if(colorbar == true){
        mplc_fprintf(device_id, "fig.colorbar(cf, %s)\n",opt_colorbar);
    }
    mplc_fprintf(device_id, "fig.canvas.blit(ax.bbox)\n");
    mplc_fprintf(device_id, "fig.canvas.flush_events()\n");
    mplc_fflush(device_id);
    return ;
}

void mplc_hexbin(const int device_id, const double x[], const double y[], const int number, const int gridsize, const char *opt, const bool colorbar, const char *opt_colorbar){

    mplc_fprintf(device_id, "xvalues = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "(%.17f),", x[i]);
    }
    mplc_fprintf(device_id, "]\n");
    mplc_fprintf(device_id, "yvalues = [");
    for (int i=0; i<number; i++) {
        mplc_fprintf(device_id, "(%.17f),", y[i]);
    }
    mplc_fprintf(device_id, "]\n");

    mplc_fprintf(device_id, "hb = ax.hexbin(xvalues,yvalues,gridsize=%d, %s)\n", gridsize, opt);
    if(colorbar == true){
        mplc_fprintf(device_id, "fig.colorbar(hb, %s)\n",opt_colorbar);
    }
    mplc_fprintf(device_id, "fig.canvas.blit(ax.bbox)\n");
    mplc_fprintf(device_id, "fig.canvas.flush_events()\n");
    mplc_fflush(device_id);
    return ;
}

void mplc_imshow(const int device_id, const double x[], const int gridsize, const char *opt, const bool colorbar, const char *opt_colorbar){

    mplc_fprintf(device_id, "xvalues = [");
    for (int i=0;i<gridsize;i++) {
        mplc_fprintf(device_id, "[");
        for (int j=0;j<gridsize;j++) {
            mplc_fprintf(device_id, "(%.17f),", x[i*gridsize+j]);
        }
        mplc_fprintf(device_id, "],\n");
    }
    mplc_fprintf(device_id, "]\n");

    mplc_fprintf(device_id, "im = ax.imshow(xvalues,origin='lower',%s)\n", opt);
    if(colorbar){
        mplc_fprintf(device_id, "fig.colorbar(im,ax=ax,%s)\n", opt_colorbar);
    }
    mplc_fprintf(device_id, "fig.canvas.blit(ax.bbox)\n");
    mplc_fprintf(device_id, "fig.canvas.flush_events()\n");
    mplc_fflush(device_id);
    return ;
}

void mplc_pcolormesh(const int device_id, const int xbin_size, const double xbin[], const int ybin_size, const double ybin[], const double z[], const char *opt, const bool colorbar, const char *opt_colorbar)
{

    mplc_fprintf(device_id, "xbin = [");
    for(int i=0;i<xbin_size;i++) {
        mplc_fprintf(device_id, "(%.17f),", xbin[i]);
    }
    mplc_fprintf(device_id, "]\n");

    mplc_fprintf(device_id, "ybin = [");
    for(int i=0;i<ybin_size;i++) {
        mplc_fprintf(device_id, "(%.17f),", ybin[i]);
    }
    mplc_fprintf(device_id, "]\n");

    mplc_fprintf(device_id, "values = [");
    for (int i=0;i<xbin_size;i++) {
        mplc_fprintf(device_id, "[");
        for (int j=0;j<ybin_size;j++) {
            mplc_fprintf(device_id, "(%.17f),", z[i*xbin_size+j]);
        }
        mplc_fprintf(device_id, "],\n");
    }
    mplc_fprintf(device_id, "]\n");

    mplc_fprintf(device_id, "pcm = ax.pcolormesh(xbin,ybin,values,edgecolors='face',%s)\n", opt);
    if(colorbar){
        mplc_fprintf(device_id, "fig.colorbar(pcm,ax=ax,%s)\n", opt_colorbar);
    }
    mplc_fprintf(device_id, "fig.canvas.blit(ax.bbox)\n");
    mplc_fprintf(device_id, "fig.canvas.flush_events()\n");
    mplc_fflush(device_id);
    return ;
}

void mplc_quiverplot(const int device_id, const int nrank, const double x[], const double y[], const double u[], const double v[], const char *opt){

    mplc_fprintf(device_id, "xvalues = [");
    for (int i=0;i<nrank;i++) {
        mplc_fprintf(device_id, "[");
        for (int j=0;j<nrank;j++) {
            mplc_fprintf(device_id, "(%.17f),", x[j+nrank*i]);
        }
        mplc_fprintf(device_id, "],\n");
    }
    mplc_fprintf(device_id, "]\n");

    mplc_fprintf(device_id, "yvalues = [");
    for (int i=0;i<nrank;i++) {
        mplc_fprintf(device_id, "[");
        for (int j=0;j<nrank;j++) {
            mplc_fprintf(device_id, "(%.17f),", y[j+nrank*i]);
        }
        mplc_fprintf(device_id, "],\n");
    }
    mplc_fprintf(device_id, "]\n");

    mplc_fprintf(device_id, "uvalues = [");
    for (int i=0;i<nrank;i++) {
        mplc_fprintf(device_id, "[");
        for (int j=0;j<nrank;j++) {
            mplc_fprintf(device_id, "(%.17f),", u[j+nrank*i]);
        }
        mplc_fprintf(device_id, "],\n");
    }
    mplc_fprintf(device_id, "]\n");

    mplc_fprintf(device_id, "vvalues = [");
    for (int i=0;i<nrank;i++) {
        mplc_fprintf(device_id, "[");
        for (int j=0;j<nrank;j++) {
            mplc_fprintf(device_id, "(%.17f),", v[j+nrank*i]);
        }
        mplc_fprintf(device_id, "],\n");
    }
    mplc_fprintf(device_id, "]\n");

    mplc_fprintf(device_id, "nxvalues = np.array(xvalues)\n");
    mplc_fprintf(device_id, "nyvalues = np.array(yvalues)\n");
    mplc_fprintf(device_id, "nuvalues = np.array(uvalues)\n");
    mplc_fprintf(device_id, "nvvalues = np.array(vvalues)\n");

    mplc_fprintf(device_id, "strm = ax.quiver(nxvalues,nyvalues,nuvalues,nvvalues,%s)\n", opt);

    mplc_fflush(device_id);
    return ;
}

void mplc_streamplot(const int device_id, const int nrank, const double x[], const double y[], const double u[], const double v[], const double color[], const char *opt, const bool colorbar, const char *opt_colorbar){

    mplc_fprintf(device_id, "xvalues = [");
    for (int i=0;i<nrank;i++) {
        mplc_fprintf(device_id, "[");
        for (int j=0;j<nrank;j++) {
	        mplc_fprintf(device_id, "(%.17f),", x[j+nrank*i]);
        }
        mplc_fprintf(device_id, "],\n");
    }
    mplc_fprintf(device_id, "]\n");

    mplc_fprintf(device_id, "yvalues = [");
    for (int i=0;i<nrank;i++) {
        mplc_fprintf(device_id, "[");
        for (int j=0;j<nrank;j++) {
	        mplc_fprintf(device_id, "(%.17f),", y[j+nrank*i]);
        }
        mplc_fprintf(device_id, "],\n");
    }
    mplc_fprintf(device_id, "]\n");

    mplc_fprintf(device_id, "uvalues = [");
    for (int i=0;i<nrank;i++) {
        mplc_fprintf(device_id, "[");
        for (int j=0;j<nrank;j++) {
	        mplc_fprintf(device_id, "(%.17f),", u[j+nrank*i]);
        }
        mplc_fprintf(device_id, "],\n");
    }
    mplc_fprintf(device_id, "]\n");

    mplc_fprintf(device_id, "vvalues = [");
    for (int i=0;i<nrank;i++) {
        mplc_fprintf(device_id, "[");
        for (int j=0;j<nrank;j++) {
	        mplc_fprintf(device_id, "(%.17f),", v[j+nrank*i]);
        }
        mplc_fprintf(device_id, "],\n");
    }
    mplc_fprintf(device_id, "]\n");

    mplc_fprintf(device_id, "nxvalues = np.array(xvalues)\n");
    mplc_fprintf(device_id, "nyvalues = np.array(yvalues)\n");
    mplc_fprintf(device_id, "nuvalues = np.array(uvalues)\n");
    mplc_fprintf(device_id, "nvvalues = np.array(vvalues)\n");

    if(color != NULL){
        mplc_fprintf(device_id, "speed = [");
        for (int i=0;i<nrank;i++) {
            mplc_fprintf(device_id, "[");
            for (int j=0;j<nrank;j++) {
	            mplc_fprintf(device_id, "(%.17f),", color[j+nrank*i]);
            }
            mplc_fprintf(device_id, "],\n");
        }
        mplc_fprintf(device_id, "]\n");
        mplc_fprintf(device_id, "nspeed = np.array(speed)\n");
    }


    if(color == NULL){
        mplc_fprintf(device_id, "strm = ax.streamplot(nxvalues,nyvalues,nuvalues,nvvalues,%s)\n", opt);
    } else {
        mplc_fprintf(device_id, "strm = ax.streamplot(nxvalues,nyvalues,nuvalues,nvvalues,color=nspeed,%s)\n", opt);
    }
    if(colorbar){
        mplc_fprintf(device_id, "fig.colorbar(strm.lines,%s)\n",opt_colorbar);
    }

    mplc_fflush(device_id);
    return ;
}

/*
 * This function draws 1D histgram.
 */
void mplc_hist(const int device_id, const int size, const double x[], const int bins, const char *opt){

    mplc_fprintf(device_id, "values = [");
    for (int i=0;i<size;i++) {
        mplc_fprintf(device_id, "(%.17f),", x[i]);
    }
    mplc_fprintf(device_id, "]\n");
    if(bins > 1){
        mplc_fprintf(device_id, "ht = ax.hist(values,bins=%d, %s)\n",bins,opt);
    } else {
        mplc_fprintf(device_id, "ht = ax.hist(values,%s)\n",opt);
    }

    return ;
}

/*
 * This function draws 2D histgram.
 */
void mplc_hist2d(const int device_id, const int size, const double x[], const double y[], const int bin_x, const int bin_y, const char *opt, const bool colorbar, const char *opt_colorbar){

    mplc_fprintf(device_id, "xvalues = [");
    for (int i=0;i<size;i++) {
        mplc_fprintf(device_id, "(%.17f),", x[i]);
    }
    mplc_fprintf(device_id, "]\n");
    mplc_fprintf(device_id, "yvalues = [");
    for (int i=0;i<size;i++) {
        mplc_fprintf(device_id, "(%.17f),", y[i]);
    }
    mplc_fprintf(device_id, "]\n");

    mplc_fprintf(device_id, "ht = ax.hist2d(xvalues,yvalues,bins=[%d,%d],%s)\n", bin_x, bin_y, opt);
    if(colorbar == true){
        mplc_fprintf(device_id, "fig.colorbar(ht[3], ax=ax, %s)\n",opt_colorbar);
    }

    mplc_fflush(device_id);
    return ;
}


void mplc_show(const int device_id, const char *opt) {
    mplc_fprintf(device_id, "plt.show()\n");
    mplc_fflush(device_id);
    return ;
}

void mplc_save(const int device_id, const char *filename, const char *opt){
    if((filename == NULL)||(strlen(filename) == 0)){
        mplc_show(device_id,opt);
    } else {
        mplc_fprintf(device_id, "plt.savefig('%s',%s)\n", filename,opt);
        mplc_fflush(device_id);
    }
    return ;
}

void mplc_clear(const int device_id){
    mplc_send_command(device_id,"plt.clf()");
    return ;
}

void mplc_title(const int device_id, const char *c){
    char title[mplc_MaxCharactersInLine];
    if(c[0] == '\"'){
        snprintf(title,mplc_MaxCharactersInLine-1,"plt.title(%s)",c);
    } else {
        snprintf(title,mplc_MaxCharactersInLine-1,"plt.title(\"%s\")",c);
    }
    mplc_send_command(device_id,title);
    return ;
}

void mplc_xlabel(const int device_id, const char *c){
    char xlabel[mplc_MaxCharactersInLine];
    if(c[0] == '\"'){
        snprintf(xlabel,mplc_MaxCharactersInLine-1,"plt.xlabel(%s)",c);
    } else {
        snprintf(xlabel,mplc_MaxCharactersInLine-1,"plt.xlabel(\"%s\")",c);
    }
    mplc_send_command(device_id,xlabel);
    return ;
}

void mplc_ylabel(const int device_id, const char *c){
    char ylabel[mplc_MaxCharactersInLine];
    if(c[0] == '\"'){
        snprintf(ylabel,mplc_MaxCharactersInLine-1,"plt.ylabel(%s)",c);
    } else {
        snprintf(ylabel,mplc_MaxCharactersInLine-1,"plt.ylabel(\"%s\")",c);
    }
    mplc_send_command(device_id,ylabel);
    return ;
}

void mplc_colorbar(const int device_id){
    mplc_send_command(device_id,"plt.colorbar()");
    return ;
}

void mplc_grid(const int device_id, const bool flag){
    if(flag == true)
        mplc_send_command(device_id,"plt.grid(True)");
    else
        mplc_send_command(device_id,"plt.grid(False)");
    return ;
}

void mplc_legend(const int device_id, const char *c){
    if((c == NULL)||(strlen(c) == 0)){
        mplc_send_command(device_id,"plt.legend()");
    } else {
        char legend[mplc_MaxCharactersInLine];
        if((strncmp(c,"best",        mplc_MaxCharactersInLine) == 0)||
           (strncmp(c,"upper right", mplc_MaxCharactersInLine) == 0)||
           (strncmp(c,"upper left",  mplc_MaxCharactersInLine) == 0)||
           (strncmp(c,"lower left",  mplc_MaxCharactersInLine) == 0)||
           (strncmp(c,"lower right", mplc_MaxCharactersInLine) == 0)||
           (strncmp(c,"right",       mplc_MaxCharactersInLine) == 0)||
           (strncmp(c,"center left", mplc_MaxCharactersInLine) == 0)||
           (strncmp(c,"center right",mplc_MaxCharactersInLine) == 0)||
           (strncmp(c,"lower center",mplc_MaxCharactersInLine) == 0)||
           (strncmp(c,"upper center",mplc_MaxCharactersInLine) == 0)||
           (strncmp(c,"center",      mplc_MaxCharactersInLine) == 0)){
            snprintf(legend,mplc_MaxCharactersInLine-1,"plt.legend(loc=\"%s\")",c);
        } else {
            snprintf(legend,mplc_MaxCharactersInLine-1,"plt.legend(%s)",c);
        }
        mplc_send_command(device_id,legend);
    }
    return ;
}

void mplc_text(const int device_id, const double x, const double y, const char *c, const char *opt){
    mplc_fprintf(device_id, "plt.text(%g,%g,%s,%s)\n", x,y,c,opt);
    return ;
}

void mplc_open_multipages(const int device_id, const char *python_name, const char *filename){
    mplc_open(device_id,python_name);
    mplc_send_command(device_id,"from matplotlib.backends.backend_pdf import PdfPages");
    mplc_fprintf(device_id, "pp = PdfPages('%s')\n",filename);
    return ;
}

void mplc_close_multipages(const int device_id){
    mplc_send_command(device_id,"pp.close()");
    return ;
}

void mplc_open_onepage(const int device_id){
    if(strlen(mplc_device_layout[device_id])>0){
        mplc_send_command(device_id,"import matplotlib.gridspec as gridspec\n");
        mplc_fprintf(device_id,"fig = plt.figure(tight_layout=True)\n");
        mplc_fprintf(device_id,"gs = gridspec.GridSpec(%s)\n",mplc_device_layout[device_id]);
    } else {
        mplc_send_command(device_id,"fig = plt.figure()");
        mplc_send_command(device_id,"ax = fig.add_subplot(1,1,1)");
    }
    return ;
}

void mplc_close_onepage(const int device_id){
    mplc_send_command(device_id,"plt.savefig(pp, format='pdf')");
    mplc_send_command(device_id,"fig.clf()");
    mplc_device_layout[device_id][0] = '\0';
    return ;
}



#include "config.h"
#include "PlantHydroTree.h"
#include "NeighborSearch.h"
#include "ReadData.h"

static inline double ReturnKernel(const double r, const double InvKerneli, const int dim){

    double u = r*InvKerneli;
    double coef;
    if(dim == 2){
        coef = (10.0/(7.0*PI))*SQ(InvKerneli);
    } else if(dim == 3){
        coef = (1.0/PI)*CUBE(InvKerneli);
    }

    if(u<1.e0){
        return (coef*(1.e0 - 1.5*SQ(u) + 0.75*CUBE(u)));
    } else if (u<2.e0){
        return (coef*(0.25*CUBE(2.e0-u)));
    } else {
        return 0.e0;
    }
}

double CalcGridDensity(double Pos[], double *H_init, const int dim){

    int NBmin,NBmax;

    if(dim == 2){
        NBmin = 17-2;
        NBmax = 17+2;
    } else if(dim == 3){
        NBmin = 32-2;
        NBmax = 32+2;
    } else {
        fprintf(stderr,"The input parameter, dim (=%d), is incorrect.\n",dim);
        assert((dim!=2)&&(dim!=3));
    }

    int Neighbors[MaxNeighborSize];

    bool LoopEndFlag = false;
    double KernelSize = *H_init;
    double Rvalue = 0.e0;
    double Lvalue = 0.e0;

    int Nlist = 0;
    do {
        Nlist = GetNeighbors(Pos,2.0*KernelSize,Neighbors);

        if(((NBmin)<=Nlist)&&(Nlist<=(NBmax))){
            LoopEndFlag = true;
        } else if((Rvalue>0.e0)&&(Lvalue>0.e0)){
            if((Nlist>0)&&(Rvalue-Lvalue < 1.e-2*Lvalue))
                LoopEndFlag = true;
        }

        if(Nlist<NBmin){
            Lvalue = fmax(Lvalue,KernelSize);
        } else if(Nlist>NBmax){
            if(Rvalue > 0.e0){
                Rvalue = fmin(Rvalue,KernelSize);
            }else{
                Rvalue = KernelSize;
            }
        }

        if((Lvalue>0.e0)&&(Rvalue>0.e0)){
            KernelSize = cbrt(0.5*(CUBE(Lvalue)+CUBE(Rvalue)));
        }else{
            if((iszero(Rvalue))&&(Lvalue > 0.e0)){
                KernelSize *= 1.26;
            }else if((Rvalue > 0.e0)&&(iszero(Lvalue))){
                KernelSize *= 0.74;
            }
        }
    } while (LoopEndFlag == false);

    double rho = 0.e0;
    double InvKernel = 1.e0/KernelSize;
    for(int k=0;k<Nlist;k++){
        int nbindex = Neighbors[k];
        double xij[3];
        xij[0] = PeriodicDistance(Pos[0],SPH[nbindex].Pos[0],0);
        xij[1] = PeriodicDistance(Pos[1],SPH[nbindex].Pos[1],1);
        xij[2] = PeriodicDistance(Pos[2],SPH[nbindex].Pos[2],2);
        double r = NORM(xij);
        double w = ReturnKernel(r,InvKernel,dim);
        rho += SPH[nbindex].Mass*w;
    }

    *H_init = KernelSize;

    return rho;
}

void PlotGrid(const double Xmin, const double Xmax, const double Ymin, const double Ymax, const int NGrid, const int dim, const char opt[], const bool colorbar, const char opt_colorbar[]){

    PlantHydroTree();

    double *PosX;
    double *PosY;
    double *GridData;
    PosX = malloc(sizeof(double)*NGrid);
    PosY = malloc(sizeof(double)*NGrid);
    GridData = malloc(sizeof(double)*NGrid*NGrid);

    double dX = (Xmax-Xmin)/(double)NGrid;
    double dY = (Ymax-Ymin)/(double)NGrid;

    for(int i=0;i<NGrid;i++){
        PosX[i] = dX*(i+0.5)+Xmin;
        PosY[i] = dY*(i+0.5)+Ymin;
    }

    double H_init = (Xmax-Xmin)/(double)NGrid;
    for(int i=0;i<NGrid;i++){
        for(int k=0;k<NGrid;k++){
            double Pos[3]; 
            Pos[0] = dX*(i+0.5)+Xmin;
            Pos[1] = dY*(k+0.5)+Ymin;
            Pos[2] = 0.5;
            GridData[i+NGrid*k] = CalcGridDensity(Pos,&H_init,dim);
        }
    }

    mplc_contourf(0,PosX,PosY,GridData,NGrid,opt,colorbar,opt_colorbar);

    free(PosX);
    free(PosY);
    free(GridData);

    if(PlotParams.ShowTime){
        char Time[MaxCharactersInLine];
        Snprintf(Time,"t = %1.4f",ThisRun.TCurrent);
    }

    return ;
}


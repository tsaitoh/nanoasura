#include "config.h"

/*!
 * The environment for OpenMP is checked and is set in this function.
 */
void CheckAndSetOpenMPEnv(void){

#ifdef USE_OPENMP
    fprintf(stderr,"OpenMP opearions are available.\n");
    fprintf(stderr,"Number of Threads = %d\n",omp_get_num_threads());
    fprintf(stderr,"Number of Default Threads = %d\n",omp_get_max_threads());
        omp_set_num_threads(ThisRun.ThreadNumber);
    fprintf(stderr,"Number of Current Threads = %d\n",omp_get_max_threads());
#else
    fprintf(stderr,"OpenMP operations are unavailable\n");
#endif

    return ;
}
            

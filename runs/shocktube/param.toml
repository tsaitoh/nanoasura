Title = "nano ASURA parameter file"
# All parameters described in this file are updated at runtime.

[Mode]
    RunType = 0       # 0 is the shocktube test.
    NParticles = 1000 # Number of particles used in this run.
    ShowLog = 1       # Verbose mode

[IO]
    OutDir = "./data_disph" # Data output directory.
    ICFile = ""             # Initial condition file.
    RunName = "shocktube"   # Base name of output file.
    OutputFileNumber = 100  # Number of output files.
    WriteEveryStep = 0      # When this flag is 1, the program writes particle data after every step.

[Boundary]
    Dimension = 1         # Dimension.
    PeriodicBoundary = 1  # Periodic boundary condition.
    LBox0 = 2.0           # Box size.
    LBox1 = 1.0           # Not used in this run.
    LBox2 = 1.0           # Not used in this run.

[Time]
    TEnd = 0.1            # Simulation end time.
    CFL = 0.3             # Coefficient for the CFL timestep.

[Hydro]
    UseDISPH = 1             # If 1, ASURA adopts DISPH. If 0, ASURA uses standard SPH.
    SelectKernelType = 1     # 0=cubic spline kernel, 
                             # 1=cubic spline kernel with a modification proposed by Thomas & Couchman (1992)
                             # 2=Wendland kernel C2, 3=Wendland kernel C4, 4=Wendland C6
    KernelEvaluationType = 1 # If this flag is 0, ASURA evaluates the kernel size based on the neighbor number.
                             # If 1, ASURA uses h = KernelEta*(m/rho)^{1/D}, 
                             # where m, rho, and D are mass, density and dimension, respectively.
    KernelEta = 1.2          # KenrelEta used when KernelEvaluationType == 1.
    UseGradh = 1               # Use the grad-h term.
    UseGradN = 1               # Use both grad-N and grad-h terms.
    DerivativeOperatorType = 1 # 0=the standard operators.
                               # 1=those shown in Hosono, Saitoh \& Makino (2016).
    Gamma = 1.4                # Specific heat ratio.
    Ns    = 7                  # Number of neighbor particles.
    Nspm  = 1                  # Tolerance number of the neighbor number, Ns.
    ViscType = 0             # If 0, use Monaghan 1997 type visc term. If 1, use vNR type visc term.
    ViscAlpha = 1.0          # Viscosity coefficient.
    UseVariableAlpha = 1     # If 1, this code uses the variable alpha mode.
    ViscAlphaMin = 0.1       # Min alpha for the variable alpha mode.
    ViscAlphaMax = 2.0       # Max alpha for the variable alpha mode.
    ViscBalsara = 0          # Switch for the Balsara limiter.
    ViscSignalVelocityBeta = 3.0 # A factor for the signal velocity.

[Thread]
    ThreadNumber = 8  # Number of thread for OpenMP instructions.
    DynamicChunk = 10 # Chunk size for OpenMP instructions.

# NanoASURA: A subset of ASURA. 

# Download NanoASURA 
You can clone this repository by using the following command: 
``` 
> git clone git@bitbucket.org:tsaitoh/nanoasura.git 
```

# Directory structure
The structure of this repository is as follows:
```
-+-src-+-setup
 +-runs-+-shocktube
 |      +-hydrostatic
 |      +-kh
 |      +-rt
 |      +-sedov
 |      +-keplar 
 |      +-galaxy
 |      +-evrard
 |
 +-plot
```

"src" holds the main simulation code.
The directories under "runs" have input parameter files for each model.
"Plot" is the directory for the data plot.



# Compile this code
NanoASURA is written in C, in particular, C99.  This software requires only a C
compiler that can compile C99 sources and there is no need to prepare other
libraries. 

After you have cloned/downloaded this repository, just type the "make" command in
the src directory: 
``` 
> cd src 
> make
```

# Run 

This code requires a parameter file to specify the run mode.  The default
name of the parameter file is "param.txt", but it is possible to use different files with different names.

The parameter files are prepared under "runs/?" directories. So, first, copy the
binary file to the working directory "runs/?".


Hereafter, assume that we are in "runs/shocktube".
To run the code type the following command:
``` 
> cd runs/shocktube
> ./asura.out
```
Then the code reads the parameter file and start the calculation.  The output
directory of the run is specified in the parameter file, "OutDir" in the
parameter file.


# Plot results

There is a plot tool in the "plot" directory. 
This plot tool requires "python", "numpy", and "matplotlib".
To compile the plot tool, just type the "make" command in "plot",
```
> make
```
You then obtain "plot.out".

"plot.out" also reads the local parameter file, "param.toml".
Here is an example of the first part of "param.toml".
```
Title = "nano ASURA parameter file"
# All parameters described in this file are updated at runtime.

[Environment]
    PythonPath = "python3.9"

[Mode]
    PlotType = 0
              #0=shocktube
              #1=hydrostatic
              #2=kh
              #3=rt
              #4=sedov
              #5=keplar
              #6=galaxy
    WriteMode = 2 
               # 0->pdf, 1->eps, 2->png
    ShowTime = 1

[TargetFiles]
    FileStart = 0
    FileEnd   = 3000

[Shocktube]
    OutDir = "./data_st_disph"
    FileName = "shocktube"
    DataDir = "../runs/shocktube/data_disph"
    PlotType = 4 # 0=Rho,1=Pressure,2=u,3=Velocity,4=plot four
    PlotSolution = 1
    SolutionFileName = "./solution_sod.txt"
    Xmin = 0.5
    Xmax = 1.5
    Ymin = 0.0
    Ymax = 1.2
    Rhomin = 0.0
    Rhomax = 1.2
    Pmin = 0.0
    Pmax = 1.2
    Umin = 1.5
    Umax = 2.7
    Vmin = 0.0
    Vmax = 1.0
```

First of all, You need to check ``PythonPath``, which is in the ``[Environment]`` section.
This plot tool calls python defined at ``PathonPath``. It should match your own environment.

In the ``[Mode]`` section, 
we can see the following parameters:``PlotType`` (target simulation), ``WriteMode`` (output format), and ``ShowTime`` (whether the simulation time is printed or not).

Output files are sequentially numbered. This allows you to specify which files to be plotted.
The start and end file numbers are defined in the ``[TargetFiles]`` section.

Parameters for shocktube runs are given in the ``[Shocktube]`` section.
Important parameters are ``DataDir`` which defines the simulation data directory, ``OutDir`` which defines the output directory name, and 
``PlotType`` which is used to select the plot type. As far as you are running shocktube tests with the given initial condition, 
you do not need to changee any of the other parameters.

After edit the parameter file, you type the following command:
```
> ./plot.out
```
Then, you get snapshots in the output directory. Since this case use "WriteMode 2", the plot tool writes png files in the output directory.

